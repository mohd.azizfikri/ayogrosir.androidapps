package com.soldig.ayogrosir

import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class DateTest {

    /*
    Required format for date in api : 2021-06-11 00:00:00
 */

    @Test
    fun dateTest() {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        println(simpleDateFormat.format(Date()))
    }

}