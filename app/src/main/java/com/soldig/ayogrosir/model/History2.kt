package com.soldig.ayogrosir.model

data class History2(
    val id: String,
    val date: String,
    val total_item: String,
    val total_price: String,
    val status: String ="",
    val isDelivery: Boolean = false,
    val isRejected: Boolean = false,
    val isDone: Boolean = false,
    val isDelivered: Boolean = false,
)