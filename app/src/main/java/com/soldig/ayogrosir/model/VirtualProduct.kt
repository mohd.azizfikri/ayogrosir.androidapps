package com.soldig.ayogrosir.model


data class PulsaProduct(
    val pulsaProvider: PulsaProvider,
    val skuCode: String,
    val adminFee: Double,
    val warungFee: Double,
    val subtotal: Double,
    val totalPrice: Double,
    val productName: String,
)

data class ListrikProduct(
    val productName: String,
    val skuCode: String,
    val adminFee: Double,
    val warungFee: Double,
    val subTotal: Double,
    val totalPrice: Double
)

data class ListrikPascaBayar(
    val skuCode: String,
    val productName: String
)

data class PulsaPascaBayar(
    val skuCode: String,
    val productName: String
)

data class PDAMPascaBayar(
    val skuCode: String,
    val productName: String
)

data class VirtualProduct(
    val pulsaProduct : HashMap<PulsaProvider, List<PulsaProduct>>,
    val listrikProduct : List<ListrikProduct>,
    val listrikPascaBayar: List<ListrikPascaBayar>,
    val pulsaPascaBayarList : List<PulsaPascaBayar>,
    val pdamPascaBayar: List<PDAMPascaBayar>
)

data class SelectedVirtualProduct(
    var refId: String?,
    var skuCode: String,
    var customerNumber: String,
    var totalPrice: Double = 0.0,
    var virtualPaymentType : VirtualPaymentType,
    var virtualType : VirtualType,
    var productName: String,
    val adminFee: Double,
    val warungFee: Double,
    val subTotal: Double,
    val productCategory: String
)