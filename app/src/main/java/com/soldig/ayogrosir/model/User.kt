package com.soldig.ayogrosir.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    var phoneNumber: String,
    var pin: String
): Parcelable