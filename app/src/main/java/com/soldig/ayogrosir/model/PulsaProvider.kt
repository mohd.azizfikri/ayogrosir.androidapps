package com.soldig.ayogrosir.model

enum class PulsaProvider {
    Indosat,
    XL,
    Axis,
    Three,
    Smartfren,
    Ceria,
    Telkomsel,
    Invalid,
    Unkown;




    companion object {
        private val numberList =  hashMapOf(
            "0811" to Telkomsel,
            "0812" to Telkomsel,
            "0813" to Telkomsel,
            "0821" to Telkomsel,
            "0822" to Telkomsel,
            "0852" to Telkomsel,
            "0853" to Telkomsel,
            "0823" to Telkomsel,
            "0851" to Telkomsel,
            "0814" to Indosat,
            "0815" to Indosat,
            "0816" to Indosat,
            "0855" to Indosat,
            "0856" to Indosat,
            "0857" to Indosat,
            "0858" to Indosat,
            "0817" to XL,
            "0818" to XL,
            "0819" to XL,
            "0859" to XL,
            "0877" to XL,
            "0878" to XL,
            "0838" to Axis,
            "0831" to Axis,
            "0832" to Axis,
            "0833" to Axis,
            "0895" to Three,
            "0896" to Three,
            "0897" to Three,
            "0898" to Three,
            "0899" to Three,
            "0881" to Smartfren,
            "0882" to Smartfren,
            "0883" to Smartfren,
            "0884" to Smartfren,
            "0885" to Smartfren,
            "0886" to Smartfren,
            "0887" to Smartfren,
            "0888" to Smartfren,
            "0889" to Smartfren,
            "0828" to Ceria
        )

        fun pulsaProviderByPhoneNumber(phoneNumber: String): PulsaProvider {
            if(phoneNumber.length < 5) return Invalid
            return numberList[phoneNumber.substring(0,4)] ?: Unkown
        }

        fun getPulsaByName(name: String) : PulsaProvider {
            val providerList = enumValues<PulsaProvider>()
            return providerList.firstOrNull { it.name.equals(name, ignoreCase = true) } ?: Invalid
        }

        fun getSKUCodeForPostPaid(phoneNumber: String) : String?{
            val isDevelopmentNumber = phoneNumber == "081234554320" || phoneNumber == "081234554321" || phoneNumber == "081234554322" || phoneNumber == "081234554323" || phoneNumber == "081234554324"
            if(isDevelopmentNumber) return "hp"
            return when(pulsaProviderByPhoneNumber(phoneNumber)) {
                Indosat -> "matrix_postpaid"
                Telkomsel -> "halo_postpaid"
                Smartfren -> "smartfren_postpaid"
                Three -> "three_postpaid"
                XL -> "xl_postpaid"
                else -> null
            }
        }

    }
}