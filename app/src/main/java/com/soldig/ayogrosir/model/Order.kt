package com.soldig.ayogrosir.model

import com.soldig.ayogrosir.presentation.feature.home.HomeViewState.ProductSection.ProductListItem.POSProductListItem


data class Order(
    val product: POSProductListItem,
    var quantity: Int,
     //Price when the order is being ordered ( cause the product price can be change any time )
    var currentTotalPrice: Double
)

