package com.soldig.ayogrosir.model

enum class DiscountType {
    Percentage,
    Nominal,
    Nothing
}