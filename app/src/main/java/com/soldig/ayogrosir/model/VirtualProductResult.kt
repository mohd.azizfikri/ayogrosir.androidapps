package com.soldig.ayogrosir.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


enum class VirtualProductResultStatus {
    Success,
    Fail,
    Pending
}



@Parcelize
data class VirtualProductResult(
    var totalPrice: Double,
    var adminFee: Double,
    var warungFee: Double,
    var subTotal: Double,
    var status: VirtualProductResultStatus,
    var refId: String,
    var customerNumber: String,
    var virtualProductType : VirtualType,
    var virtualProductPaymentType: VirtualPaymentType,
    var message: String,
    val productNaem: String

) : Parcelable