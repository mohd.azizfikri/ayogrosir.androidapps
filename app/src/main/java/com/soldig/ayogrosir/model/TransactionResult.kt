package com.soldig.ayogrosir.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TransactionResult(
    val transactionId: String,
    val totalPrice: Double,
    val totalChange: Double,
    val totalPaid: Double
) : Parcelable