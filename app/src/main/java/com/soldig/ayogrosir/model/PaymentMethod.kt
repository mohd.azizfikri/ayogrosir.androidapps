package com.soldig.ayogrosir.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PaymentMethod(
    val id: String,
    val name: String,
    val categoryId: String
) : Parcelable