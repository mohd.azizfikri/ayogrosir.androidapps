package com.soldig.ayogrosir.model

data class VirtualProductHistory(
    val orderNumber : String,
    val date: String,
    val jenis: String,
    val subTotal: String,
    val feeWarung: String,
    val feeAdmin: String,
    val total: String
)