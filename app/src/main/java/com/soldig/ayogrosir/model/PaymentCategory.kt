package com.soldig.ayogrosir.model

data class PaymentCategory(
    val id: String,
    val name: String,
    var paymentMethods: List<PaymentMethod> = listOf(),
    val paymentType: PaymentCategoryType
)

enum class PaymentCategoryType{
    CASH,
    NOT_CASH
}