package com.soldig.ayogrosir.model

data class Report(
    val beginningBalanceTotal: Double = 0.0,
    val billTotal: Int = 0,
    val soldProductQty: Int = 0,
    val canceledBill: Int = 0,
    val totalCanceledAmount: Double = 0.0,
    val canceledProductQty: Int = 0,
    val discountTotal: Double = 0.0,
    val totalAmountWithoutDiscount: Double = 0.0,
    val totalAmountAfterDiscount: Double = 0.0,
    val totalAmountPaidWithCash : Double = 0.0,
    val totalAmountPaidWithNotCash: Double = 0.0,
    val totalAllCash: Double = 0.0,
    val soldProduct : List<ProductReport> = listOf(),
    val canceledProduct: List<ProductReport> = listOf(),
    val reportWallet: List<ReportWallet> = listOf()
    )
data class ProductReport(
    val name: String,
    val qty: Int
)