package com.soldig.ayogrosir.model

data class SettingModel(
    var isDeliveryAvailable: Boolean,
    var scheduleList : List<Schedule>
)