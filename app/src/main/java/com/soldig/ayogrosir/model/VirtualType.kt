package com.soldig.ayogrosir.model

enum class VirtualType {
    Listrik,
    Pulsa,
    Air
}