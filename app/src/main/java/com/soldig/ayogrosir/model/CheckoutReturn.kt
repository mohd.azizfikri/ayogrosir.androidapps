package com.soldig.ayogrosir.model

data class CheckoutReturn(
    val transactionResult: TransactionDetail,
    val isCheckoutDoneInServer : Boolean
)