package com.soldig.ayogrosir.model

import com.google.gson.annotations.SerializedName

data class ResponseRiwayatTopup(

	@field:SerializedName("Response")
	val response: String? = null,

	@field:SerializedName("Message")
	val message: String? = null,

	@field:SerializedName("Data")
	val data: List<DataItemRiawayat>? = null
)

data class DataItemRiawayat(

	@field:SerializedName("NamaWarung")
	val namaWarung: String? = null,

	@field:SerializedName("BuktiTransfer")
	val buktiTransfer: String? = null,

	@field:SerializedName("TglTopup")
	val tglTopup: String? = null,

	@field:SerializedName("IdWarung")
	val idWarung: String? = null,

	@field:SerializedName("AlasanDitolak")
	val alasanDitolak: String? = null,

	@field:SerializedName("StatusPengajuan")
	val statusPengajuan: String? = null,

	@field:SerializedName("No")
	val no: String? = null,

	@field:SerializedName("KodeStatusPengajuan")
	val kodeStatusPengajuan: String? = null,

	@field:SerializedName("JmlTopup")
	val jmlTopup: String? = null
)
