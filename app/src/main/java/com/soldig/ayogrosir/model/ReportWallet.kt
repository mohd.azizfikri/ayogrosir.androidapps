package com.soldig.ayogrosir.model

data class ReportWallet(
    val total: Double,
    val name: String
)