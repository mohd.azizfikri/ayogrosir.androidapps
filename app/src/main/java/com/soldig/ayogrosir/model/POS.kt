package com.soldig.ayogrosir.model

import android.os.Parcelable
import com.soldig.ayogrosir.datasource.DataSourceConst
import kotlinx.parcelize.Parcelize

@Parcelize
data class POS(
    val phoneNumber: String,
    val token:String,
    val name: String,
    val alamat: String,
    val loginTime: Long,
    val profileImage: String = "",
):  Parcelable {

    companion object {
        fun produceFailedPOS() : POS {
            return POS(
                "",
                "",
                "",
                "",
                -1L,
                ""
            )
        }
    }

    fun getFullImageURL() : String = DataSourceConst.BASE_IMAGE_URL + profileImage

}