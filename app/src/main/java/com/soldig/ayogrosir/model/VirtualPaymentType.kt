package com.soldig.ayogrosir.model

enum class VirtualPaymentType {
    PraBayar,
    PascaBayar
}