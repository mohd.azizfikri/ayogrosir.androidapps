package com.soldig.ayogrosir.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

sealed class Schedule(val day: String,val hour: String) {
        companion object {
               const val CLOSE_HOUR = "Tutup"
        }
        class Open(day: String, hour: String): Schedule(day, hour)
        class Close(day: String) : Schedule(day,CLOSE_HOUR)
        fun toParcelableScheduler() : ParcelableSchedule = ParcelableSchedule(this.day,this.hour)
}

@Parcelize
data class ParcelableSchedule (val day: String,val hour:  String) :Parcelable {

    fun mapToSchedule() : Schedule{
        return if(this.hour == Schedule.CLOSE_HOUR){
            Schedule.Close(this.day)
        }else{
            Schedule.Open(this.day,this.hour)
        }
    }

}