package com.soldig.ayogrosir.model

data class Transaction(
    val paymentMethod: PaymentMethod,
    val cart: Cart,
    val paidMoney: Double,
    val change: Double = 0.0
)