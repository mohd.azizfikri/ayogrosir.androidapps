package com.soldig.ayogrosir.model

class TransactionHistory(
    val id: String,
    val orderNumber: String,
    val orderDate:  String,
    val totalItem : Int,
    val totalPrice: Double,
    val status : TransactionStatus
)