package com.soldig.ayogrosir.model

data class TransactionForSync(
    val transactionId: String,
    val paymentId: String,
    val paid: Double,
    val transactionDate: String,
    val listProduct: List<ProductTransactionForSync>
)

data class ProductTransactionForSync(
    val shopProductId: String,
    val discountType: DiscountType,
    val sellPrice: Double,
    val discountValue: Double,
    val currentPrice:  Double,
    val quantity: Int
)