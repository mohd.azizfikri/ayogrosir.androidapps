package com.soldig.ayogrosir.model

import android.graphics.Color

enum class TransactionStatus(val value: Int) {
    SELESAI(1),
    MENUNGGU_DITERIMA(2),
    DITOLAK(3),
    DIKIRIM(4),
    MENUNGGU_PEMBAYARAN(5),
    SUDAH_DIBAYAR(6),
    DIBATALKAN(7),
    DITERIMA(8),
    UNKNOWN(9);

    companion object {
        private val VALUES = values()
        fun getByValue(value: Int) = VALUES.firstOrNull { it.value == value }
    }

    fun getInString() : String{
        return this.name.replace("_", " ").lowercase().replaceFirstChar { it.uppercase() }
    }

    fun getStatusTextColor() : Int{
        val colorString : String= when(this) {
            MENUNGGU_DITERIMA -> "#FFAA00"
            DITOLAK ->  "#FF0000"
            DIKIRIM -> "#429E16"
            MENUNGGU_PEMBAYARAN -> "#3107E1"
            SUDAH_DIBAYAR -> "#7AB6FF"
            else -> "#076AE1"
        }
        return Color.parseColor(colorString)
    }
}