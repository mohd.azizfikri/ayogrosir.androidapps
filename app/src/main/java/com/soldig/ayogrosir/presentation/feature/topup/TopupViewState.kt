package com.soldig.ayogrosir.presentation.feature.topup

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TopupViewState(
     var topUpValue : Double = 0.0,
     var isTopUpDone: Boolean = false,
     var isFetching : Boolean = false
): Parcelable