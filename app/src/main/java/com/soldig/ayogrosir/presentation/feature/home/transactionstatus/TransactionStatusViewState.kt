package com.soldig.ayogrosir.presentation.feature.home.transactionstatus

import com.soldig.ayogrosir.utils.other.SingleEvent

data class TransactionStatusViewState(
    var isPrintingLoading: Boolean = false,
    var printedString: SingleEvent<String>? = null
)