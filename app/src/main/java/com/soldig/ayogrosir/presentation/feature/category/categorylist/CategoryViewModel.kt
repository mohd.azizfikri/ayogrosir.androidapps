package com.soldig.ayogrosir.presentation.feature.category.categorylist

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.category.CategoryInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel
@Inject constructor(
    private val categoryInteractor: CategoryInteractor
) : BaseViewModel<CategoryViewState>(){
    override fun initNewViewState(): CategoryViewState {
        return CategoryViewState(false)
    }


    fun getAllCategory(token:String){
        viewModelScope.launch(IO){
            categoryInteractor.getAllCategory.fetch(token).collect {
                onCollect(
                    response = it,
                    onLoading = { isLoading ->
                        setViewState(getCurrentViewStateOrNew().apply {
                            isFetching = isLoading
                        })
                    },
                    executeOnSuccess = {
                        setViewState(getCurrentViewStateOrNew().apply {
                            categoryList = it.data
                        })
                    }
                )
            }
        }
    }


}