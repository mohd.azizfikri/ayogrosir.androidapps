package com.soldig.ayogrosir.presentation.feature.category.categorylist

import android.os.Parcelable
import com.soldig.ayogrosir.model.ProductCategory
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryViewState(
    var isFetching: Boolean
) : Parcelable {


    @IgnoredOnParcel
    var categoryList : List<ProductCategory> = listOf()

}