package com.soldig.ayogrosir.presentation.commonlist.payment

import android.annotation.SuppressLint
import android.view.View
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.ReportProductItemBinding
import com.soldig.ayogrosir.model.ReportWallet
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class PaymentViewHolder(view : View) : RecyclerViewHolder<ReportWallet>(view) {

    private val binding = ReportProductItemBinding.bind(view);


    @SuppressLint("SetTextI18n")
    fun bind(reportWallet: ReportWallet) {
        with(binding){
            tvName.text = reportWallet.name
            tvQty.text = ": ${reportWallet.total.toMoneyFormat()}"
        }
    }

}