package com.soldig.ayogrosir.presentation.feature.topup_virtual

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.viewModels
import com.soldig.ayogrosir.databinding.ActivityTopUpVirtualBinding
import com.soldig.ayogrosir.presentation.feature.topup_virtual.riwayat_topup.RiwayatTopupActivity
import com.soldig.ayogrosir.utils.ext.launchDialogTopup
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import android.graphics.Bitmap
import android.net.Uri
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.soldig.ayogrosir.common.toast
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import android.provider.MediaStore.Images
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.utils.ext.launchDialogTopupNew
import com.soldig.ayogrosir.utils.ext.showShortSnackbar
import com.soldig.ayogrosir.utils.other.UIInteraction
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.text.DecimalFormat


@AndroidEntryPoint
class TopUpVirtualActivity : BaseLoggedInActivity() {
    private lateinit var binding: ActivityTopUpVirtualBinding
    private val viewModel: TopupVirtualViewModel by viewModels()
    private val viewModelSaldo: GetSaldoViewModel by viewModels()
    private val CAMERA_PIC_REQUEST = 0
    private val MY_CAMERA_REQUEST_CODE = 100
    private val PERMISSION_REQ_ID = 22
    var imageData = ""
    var filename = ""

    private val REQUESTED_PERMISSIONS = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTopUpVirtualBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        observeVM()
        initToolbar("Topup Saldo", binding.include)
        initTime(toolbar = binding.include)
        binding.btnRiwayatSaldo.setOnClickListener {
            val intent = Intent(baseContext, RiwayatTopupActivity::class.java)
            startActivity(intent)
        }
        launchDialogTopupNew()
        binding.tvCaraTopup.paint?.isUnderlineText = true
        binding.tvCaraTopup.setOnClickListener {
            launchDialogTopup(
                message = "2. Lampirkan bukti transfer/struk Transfer \n \n3. Masukan jumlah nominal Top Up \n \n4. Mohon tunggu verifikasi Admin Ayo Grosir untuk memastikan Top Up sudah berhasil \n \n5. Selesai, anda bisa menjual Product Virtual di Aplikasi POS",
                title = "Cara Topup Saldo"
            )
        }
        binding.btnSubmit.setOnClickListener {
            if(imageData != "" && binding.edtInputSaldo.text.toString() != ""){
                var param: HashMap<String, RequestBody> = HashMap<String, RequestBody>()
                param.apply {
                    param["jumlah_transfer"] = createRequestBody(binding.edtInputSaldo.text.toString())
                }
                val imagefile = File(imageData)
                val reqBody =
                    RequestBody.create(
                        "multipart/form-file".toMediaTypeOrNull(),
                        imagefile
                    )
                val partfile =
                    MultipartBody.Part.createFormData(
                        "bukti_transfer",
                        imagefile.name,
                        reqBody
                    )
                viewModel.topUp(currentPOS.token, param, partfile)
            } else {
                binding.root.showShortSnackbar("file tidak boleh kosong")
                //toast("file foto tidak tersedia")
                println("file tidak boleh kosong")
            }
        }

        binding.edtUploadBukti.setOnClickListener {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST)
        }

        if (checkSelfPermission(REQUESTED_PERMISSIONS[0], PERMISSION_REQ_ID) &&
            checkSelfPermission(REQUESTED_PERMISSIONS[1], PERMISSION_REQ_ID) &&
            checkSelfPermission(REQUESTED_PERMISSIONS[2], PERMISSION_REQ_ID)
        ) {
        }

        viewModelSaldo.getSaldo(currentPOS.token)
    }

    private fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission) !==
            PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            if (requestCode == PERMISSION_REQ_ID) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED || grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                    toast(
                        "Need permissions " + Manifest.permission.CAMERA + "/" + Manifest.permission.WRITE_EXTERNAL_STORAGE + "/" + Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    finish()
                    return
                }
            }
    }

    private fun observeVM() {
        viewModel.viewState.observe(this, { viewState ->
            with(viewState) {
                binding.loading.root.toggleVisibility(isLoading)
//                println("datame" + viewState.responDataTopup)
////                val dataku = viewState.responDataTopup?.get() ?: return@observe
                viewState.responDataTopup?.get()?.let {
                    if (it.message != null){
                        toast(it.message.toString())
                        finish()
                    }
                }
            }
        })
        viewModel.userInteraction.observe(this, {
            val interaction = it.get() ?: return@observe
            if (interaction is UIInteraction.GenericMessage) {
                binding.root.showShortSnackbar(interaction.getMessage(this))
            }
        })
        viewModelSaldo.viewState.observe(this, { viewState ->
            with(viewState) {
                binding.loading.root.toggleVisibility(isLoading)
//                println("datame" + viewState.responDataTopup)
////                val dataku = viewState.responDataTopup?.get() ?: return@observe
                viewState.responGetSaldo?.get()?.let {
                    println("saldoku" + it)
                    if (it.message != null){
                        binding.tvSaldo.text = "Rp. " + currencyBanyak(it.data?.get(0)?.saldoWarung.toString())
                        //toast(it.message.toString())
                    }
                }
            }
        })
        viewModelSaldo.userInteraction.observe(this, {
            val interaction = it.get() ?: return@observe
            if (interaction is UIInteraction.GenericMessage) {
                binding.root.showShortSnackbar(interaction.getMessage(this))
            }
        })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            val image = data?.extras!!["data"] as Bitmap?
            val tempUri: Uri? = image?.let { getImageUri(applicationContext, it) }
            // CALL THIS METHOD TO GET THE ACTUAL PATH
            val finalFile = File(getRealPathFromURI(tempUri))
            filename = finalFile.toString()
            println("filenama $filename")
            binding.edtUploadBukti.setText(filename)
            System.out.println("gambarku $finalFile")
        }
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes)
        val path = Images.Media.insertImage(inContext.contentResolver, inImage, "TOPUP", null)
        return Uri.parse(path)
    }

    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (contentResolver != null) {
            val cursor: Cursor? = contentResolver.query(uri!!, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                val idx: Int = cursor.getColumnIndex(Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()

                imageData = path
            }
        }
        return path
    }

    fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(
            Companion.MULTIPART_FORM_DATA.toMediaTypeOrNull(), s)
    }

    companion object {
        const val MULTIPART_FORM_DATA = "multipart/form-data"
    }

    fun currencyBanyak(s: String): String = DecimalFormat("###,###,###,###,###")
        .format(s.toDouble()).replace(",", ".")
}