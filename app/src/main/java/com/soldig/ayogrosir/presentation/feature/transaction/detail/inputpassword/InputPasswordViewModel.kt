package com.soldig.ayogrosir.presentation.feature.transaction.detail.inputpassword

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.utils.other.PasswordValidator
import com.soldig.ayogrosir.utils.other.SingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InputPasswordViewModel @Inject
constructor(
    private val passwordValidator: PasswordValidator
) : BaseViewModel<InputPasswordViewState>(){



    override fun initNewViewState(): InputPasswordViewState {
        return InputPasswordViewState()
    }


    fun validatePassword(){
        viewModelScope.launch(IO){
            setLoading(true)
            val passwordValid = passwordValidator.invalidatePassword(getCurrentViewStateOrNew().inputtedPassword)
            setViewState(getCurrentViewStateOrNew().apply {
                this.isValidatingPassword = false
                this.isInputPasswordSuccess = SingleEvent(passwordValid)
            })
        }
    }

    private fun setLoading(isLoading: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            this.isValidatingPassword = isLoading
        })
    }

    fun setCurrentPassword(newPassword: String){
        setViewState(getCurrentViewStateOrNew().apply {
            this.inputtedPassword = newPassword
        })
    }
}