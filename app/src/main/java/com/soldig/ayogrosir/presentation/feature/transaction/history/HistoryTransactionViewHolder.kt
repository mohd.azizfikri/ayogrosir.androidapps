package com.soldig.ayogrosir.presentation.feature.transaction.history

import android.annotation.SuppressLint
import android.view.View
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.HistoryTransactionListItemBinding
import com.soldig.ayogrosir.model.TransactionHistory
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder
import java.text.SimpleDateFormat
import java.util.*

class HistoryTransactionViewHolder(view: View) : RecyclerViewHolder<TransactionHistory>(view){
    private val binding = HistoryTransactionListItemBinding.bind(view)


    fun bind(transactionHistory: TransactionHistory, onTransactionClicked: (transactionHistory: TransactionHistory) -> Unit){
        with(transactionHistory) {
            binding.tvIdTransaction.text = this.orderNumber
            binding.tvDate.text = getDateInCorrectFormat(this.orderDate)
            binding.tvTotalQty.text = this.totalItem.toString()
            binding.tvTotalPrice.text = this.totalPrice.toMoneyFormat()
            binding.btnDetail.setOnClickListener {
                onTransactionClicked(transactionHistory)
            }

        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDateInCorrectFormat(date: String) : String{
        val currentLocal = Locale("id", "ID")
        val parsedDate : Date = try{
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            simpleDateFormat.parse(date)
        }catch (e : Exception){
            try{
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
                simpleDateFormat.parse(date)
            }catch (e : Exception){
                null
            }
        } ?: return date
        return SimpleDateFormat("dd/MMM/yyyy HH:mm",currentLocal).format(parsedDate)
    }
}