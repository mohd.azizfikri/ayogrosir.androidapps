package com.soldig.ayogrosir.presentation.feature.virutal.paymentresult

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.VirtualProductResult
import com.soldig.ayogrosir.utils.other.SingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VirtualPaymentResultViewModel @Inject
constructor(

) : BaseViewModel<VirtualPaymentResultViewState>(){



    override fun initNewViewState(): VirtualPaymentResultViewState {
        return VirtualPaymentResultViewState(false,null)
    }

    private fun setLoading(isLoading: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            isPrintingLoading = isLoading
        })
    }

    fun requestPrint(currentPOS: POS,virtualProductResult : VirtualProductResult) {
        viewModelScope.launch(IO){
            setLoading(true)
            val header =
                "[L]\n" +
                        "[C]${currentPOS.name}\n" +
                        "[C]${currentPOS.alamat}\n\n" +
                        "[L]No  ${virtualProductResult.refId}\n" +
//                        "[L]TGL ${transactionResult.date.split(" ")[0]}\n" +
//                        "[L]JAM ${transactionResult.date.split(" ")[1]}\n" +
                        "[C]================================\n"



            val itemDetailContent = "[L]\n" +
                    "[L]1 <b>${virtualProductResult.productNaem}</b>\n" +
                    "[L]  ${virtualProductResult.totalPrice.toMoneyFormat()} x 1[R]${(virtualProductResult.totalPrice).toMoneyFormat()}\n"



            val footer =
                "[C]================================\n" +
                        "[L]\n" +
                        "[R]TOTAL :[R]${virtualProductResult.totalPrice.toMoneyFormat()}\n" +
                        "[L]\n" +
                        "[L]\n" +
                        "[L]TERIMAKASIH TELAH BERBELANJA\n"+
                        "[L]\n" +
                        "[L]\n"

            val finalPrinted = header + itemDetailContent + footer
            println(finalPrinted)
            setViewState(getCurrentViewStateOrNew().apply {
                isPrintingLoading = false
                printedString = SingleEvent(finalPrinted)
            })
        }
    }
}