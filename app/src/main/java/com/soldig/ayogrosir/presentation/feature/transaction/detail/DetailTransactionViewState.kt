package com.soldig.ayogrosir.presentation.feature.transaction.detail

import android.os.Parcelable
import com.soldig.ayogrosir.model.TransactionDetail
import kotlinx.parcelize.Parcelize

@Parcelize
data class DetailTransactionViewState(
    var isFetching: Boolean = false,
    var transactionDetail : TransactionDetail? = null,
    var isTransactionCanceled: Boolean = false,
    var transactionProductList: ArrayList<ArrayList<String>>  = ArrayList(),
    var isPrintingLoading: Boolean = false,
    var printedString: String = ""
) : Parcelable