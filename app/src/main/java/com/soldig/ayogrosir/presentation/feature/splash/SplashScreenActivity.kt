package com.soldig.ayogrosir.presentation.feature.splash

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.soldig.ayogrosir.base.BaseActivity
import com.soldig.ayogrosir.databinding.SplashScrrenActivityBinding
import com.soldig.ayogrosir.model.*
import com.soldig.ayogrosir.presentation.feature.MainActivity
import com.soldig.ayogrosir.presentation.feature.login.LoginActivity
import com.soldig.ayogrosir.utils.ext.getUserInSharedPref
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

@AndroidEntryPoint
class SplashScreenActivity : BaseActivity() {

    private lateinit var binding: SplashScrrenActivityBinding

    private val viewModel by viewModels<SplashScreenViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SplashScrrenActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        observeVM()
        checkIfIsLoggedIn()
    }

    private fun observeVM() {
        viewModel.isSyncDataDone.observe(this){
            if(it)startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
            })
        }
    }

    private fun checkIfIsLoggedIn() {
        lifecycleScope.launch(IO) {
            delay(5000)
            withContext(Main) {
                getUserInSharedPref()?.let {
                    startDataSyncText()
                    viewModel.checkSync(it)
                } ?: startActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    finish()
                })
            }

        }

    }

    private fun startDataSyncText() {
        lifecycleScope.launch(IO){
            var toBePrintedText = "Loading"
            var index =0
            delay(5000)
            while(true){
                if(index == 5){
                    toBePrintedText = "Loading"
                    index = 0
                }else{
                    toBePrintedText += " ."
                    index ++
                }
                withContext(Main){
                    binding.tvLoading.text = ""
                }
                delay(250)
            }
        }
    }
}
