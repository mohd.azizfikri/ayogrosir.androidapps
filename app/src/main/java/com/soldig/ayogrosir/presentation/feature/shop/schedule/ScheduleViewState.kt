package com.soldig.ayogrosir.presentation.feature.shop.schedule

import android.os.Parcelable
import com.soldig.ayogrosir.model.Schedule
import com.soldig.ayogrosir.utils.other.SingleEvent
import kotlinx.parcelize.Parcelize

@Parcelize
data class ScheduleViewState(
    var startTime: String = "Jam Buka",
    var endTime: String = "Jam Tutup",
    var isOpen: Boolean = false,
    var day: String = "Senin",

    var isFetching: Boolean = false
) : Parcelable {

    var isActionToogle: SingleEvent<Boolean> = SingleEvent(false)

    fun isValidToOpen() : Boolean{
        return startTime != Schedule.CLOSE_HOUR && endTime != Schedule.CLOSE_HOUR
//        return startTime != Schedule.CLOSE_HOUR && endTime != Schedule.CLOSE_HOUR
    }

    fun isTimeValid() : Boolean{
        val startTimeSplitted = startTime.split(":")
        val endTimeSplitted = endTime.split(":")
        if(startTimeSplitted.size != 2 && endTimeSplitted.size != 2) return false
        val startHour = startTimeSplitted[0].toIntOrNull()  ?: return false
        val endHour = endTimeSplitted[0].toIntOrNull() ?: return false
        if(startHour > endHour) return false
        if(startHour < endHour) return true
        val startMinute = startTimeSplitted[1].toIntOrNull() ?: return false
        val endMinute = endTimeSplitted[1].toIntOrNull() ?: return false
        return endMinute > startMinute
    }


}
