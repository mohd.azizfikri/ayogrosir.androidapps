package com.soldig.ayogrosir.presentation.feature.topup_virtual.riwayat_topup

import com.soldig.ayogrosir.model.ResponseRiwayatTopup
import java.text.SimpleDateFormat
import java.util.*

data class RiwayatTopupViewState(
    var responseRiwayatTopup: ResponseRiwayatTopup? = null,
    var startDateFilter: Date? = null,
    var endDateFilter: Date? = null,
    var statusState: String? = null,
    var isFetching: Boolean = false
) {
    fun getStartDateInString(): String? = startDateFilter?.let { getDateInString(it) }

    fun getEndDateInString(): String? = endDateFilter?.let { getDateInString(it) }

    fun getStatusRiwayat():String? = statusState


    private fun getDateInString(date: Date): String {
        val simpleDateFormat = SimpleDateFormat("dd/MMM/yyyy HH:mm", Locale("id", "ID"))
        return simpleDateFormat.format(date)
    }
}
