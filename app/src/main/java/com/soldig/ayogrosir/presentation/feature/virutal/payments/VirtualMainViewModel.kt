package com.soldig.ayogrosir.presentation.feature.virutal.payments

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.interactors.virtual.VirtualInteractor
import com.soldig.ayogrosir.model.*
import com.soldig.ayogrosir.utils.other.SingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VirtualMainViewModel @Inject constructor(
    private val virtualInteractor: VirtualInteractor
) : BaseViewModel<VirtualMainViewState>() {


    override fun initNewViewState(): VirtualMainViewState {
        return VirtualMainViewState()
    }


    fun getVirtualProduct(token: String) {
        viewModelScope.launch(IO) {
            virtualInteractor.getVirtualProductList.fetch(token).collect {
                onCollect(
                    response = it,
                    onLoading = {
                        setViewState(getCurrentViewStateOrNew().apply {
                            isFetching = it
                        })
                    },
                    executeOnSuccess = { successResponse ->
                        setViewState(getCurrentViewStateOrNew().apply {
                            this.pulsaSection.pulsaPraBayarList = successResponse.data.pulsaProduct
                            this.listrikSection.listrikProductList = successResponse.data.listrikProduct
                            this.pulsaSection.pulsaPascabayarList = successResponse.data.pulsaPascaBayarList
                            this.listrikSection.listrikPascaBayarList = successResponse.data.listrikPascaBayar.firstOrNull()
                            this.airSection.pdamProductList = successResponse.data.pdamPascaBayar
                        })
                    }
                )
            }
        }
    }


    fun checkListrikPostPaid(token: String, noListrik: String) {
        viewModelScope.launch(IO) {
            if(noListrik.isEmpty()){
                getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = "Untuk melalakukan cek tagihan, nomor listrik tidak boleh kosong!",
                            titleMessage = "Nomor Listrik Kosong"
                        )
                    )
                }
                return@launch
            }
            val currentViewState = getCurrentViewStateOrNew()
            val listrikPascaBayar = currentViewState.listrikSection.listrikPascaBayarList
            if(listrikPascaBayar != null){
                setViewState(getCurrentViewStateOrNew().apply() {
                    this.selectedVirtualProduct = null
                })
                virtualInteractor.postPaidCheck.fetch(token, listrikPascaBayar.skuCode, noListrik).collect { resource ->
                    onCollect(
                        response = resource,
                        onLoading = {
                            setViewState(getCurrentViewStateOrNew().apply {
                                isFetching = it
                            })
                        },
                        executeOnSuccess = { result ->
                            val data = result.data
                            setViewState(getCurrentViewStateOrNew().apply {
                                when (data.postPaidStatus) {
                                    PostPaidStatus.Success -> {
                                        listrikSection.currentListrikNumber = noListrik
                                        this.selectedVirtualProduct = SelectedVirtualProduct(
                                            refId = data.refId,
                                            skuCode = listrikPascaBayar.skuCode,
                                            customerNumber = noListrik,
                                            totalPrice = data.totalPrice,
                                            virtualPaymentType = VirtualPaymentType.PascaBayar,
                                            virtualType = VirtualType.Listrik,
                                            productName = listrikPascaBayar.productName,
                                            adminFee = data.adminFee,
                                            warungFee = data.warungFee,
                                            subTotal = data.subTotal,
                                            productCategory = "PLN"
                                        )
                                        this.successMessage = SingleEvent(
                                            PostPaidSuccessMessage(
                                                friendlyMessage = "Jumlah tagihan listrik anda adalah ${data.totalPrice.toMoneyFormat()}",
                                                titleMessage = "Berhasil mendapatkan jumlah tagihan"
                                            )
                                        )
                                    }
                                    PostPaidStatus.Fail -> {
                                        this.errorMessage = SingleEvent(
                                            PostPaidErrorMessage(
                                                actualMessage = "Penyebab Error : ${data.message}",
                                                friendlyMessage = "Hal tersebut dapat terjadi karena:\n1.Nomor Listrik anda salah\n2.Nomor Listrik bukan pascabayar",
                                                titleMessage = "Gagal cek tagihan"
                                            )
                                        )
                                    }
                                    PostPaidStatus.Pending -> {

                                    }
                                }
                            })
                        }
                    )
                }
            }else{
                getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = "Produk Listrik  belum tersedia",
                            titleMessage = "Produk Belum Tersedia"
                        )
                    )
                }
            }

        }
    }

    fun checkAirPostpaid(token: String, noAir: String) {
        viewModelScope.launch(IO) {
            if(noAir.isEmpty()){
                getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = "Untuk melalakukan cek tagihan, nomor PDAM tidak boleh kosong!",
                            titleMessage = "Nomor PDAM Kosong"
                        )
                    )
                }
                return@launch
            }
            val currentViewState = getCurrentViewStateOrNew()
            val selectedPDAM = currentViewState.airSection.currentSelectedPDAMPascaBayar
            if(selectedPDAM != null){
                setViewState(getCurrentViewStateOrNew().apply() {
                    this.selectedVirtualProduct = null
                })
                virtualInteractor.postPaidCheck.fetch(token, selectedPDAM.skuCode, noAir).collect { resource ->
                    onCollect(
                        response = resource,
                        onLoading = {
                            setViewState(getCurrentViewStateOrNew().apply {
                                isFetching = it
                            })
                        },
                        executeOnSuccess = { result ->
                            val data = result.data
                            setViewState(getCurrentViewStateOrNew().apply {
                                when (data.postPaidStatus) {
                                    PostPaidStatus.Success -> {
                                        airSection.currentNomorAir = noAir
                                        this.selectedVirtualProduct = SelectedVirtualProduct(
                                            refId = data.refId,
                                            skuCode = selectedPDAM.skuCode,
                                            customerNumber = noAir,
                                            totalPrice = data.totalPrice,
                                            virtualPaymentType = VirtualPaymentType.PascaBayar,
                                            virtualType = VirtualType.Listrik,
                                            productName = selectedPDAM.productName,
                                            adminFee = data.adminFee,
                                            warungFee = data.warungFee,
                                            subTotal = data.subTotal,
                                            productCategory = "PDAM"
                                        )
                                        this.successMessage = SingleEvent(
                                            PostPaidSuccessMessage(
                                                friendlyMessage = "Jumlah tagihan air anda adalah ${data.totalPrice.toMoneyFormat()}",
                                                titleMessage = "Berhasil mendapatkan jumlah tagihan"
                                            )
                                        )
                                    }
                                    PostPaidStatus.Fail -> {
                                        this.errorMessage = SingleEvent(
                                            PostPaidErrorMessage(
                                                actualMessage = "Penyebab Error : ${data.message}",
                                                friendlyMessage = "Hal tersebut dapat terjadi karena:\n1.Nomor air anda salah\n2.Lokasi pilihan tidak tepat",
                                                titleMessage = "Gagal cek tagihan"
                                            )
                                        )
                                    }
                                    PostPaidStatus.Pending -> {

                                    }
                                }
                            })
                        }
                    )
                }
            }else{
                getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = "Anda harus memilihi lokasi pdam terlebih dahulu",
                            titleMessage = "Lokasi PDAM belum dipilih"
                        )
                    )
                }
            }

        }
    }

    fun checkPulsaPostPaid(token: String, phoneNumberString: String) {
        viewModelScope.launch(IO) {
            val currentSelectedPulsa =
                getCurrentViewStateOrNew().pulsaSection.currentSelectedPascabayarPulsa
            setViewState(getCurrentViewStateOrNew().apply() {
                this.selectedVirtualProduct = null
            })
            if (currentSelectedPulsa != null) {
                virtualInteractor.postPaidCheck.fetch(token, currentSelectedPulsa.skuCode, phoneNumberString)
                    .collect { resource ->
                        onCollect(
                            response = resource,
                            onLoading = {
                                setViewState(getCurrentViewStateOrNew().apply {
                                    isFetching = it
                                })
                            },
                            executeOnSuccess = { result ->
                                val data = result.data
                                setViewState(getCurrentViewStateOrNew().apply {
                                    when (data.postPaidStatus) {
                                        PostPaidStatus.Success -> {
                                            this.pulsaSection.currentPascaBayarPhoneNumber = phoneNumberString
                                            this.selectedVirtualProduct = SelectedVirtualProduct(
                                                refId = data.refId,
                                                skuCode =  currentSelectedPulsa.skuCode,
                                                customerNumber = phoneNumberString,
                                                totalPrice = data.totalPrice,
                                                virtualPaymentType = VirtualPaymentType.PascaBayar,
                                                virtualType = VirtualType.Pulsa,
                                                productName = currentSelectedPulsa.productName,
                                                adminFee = data.adminFee,
                                                warungFee = data.warungFee,
                                                subTotal = data.subTotal,
                                                productCategory = "Pulsa"
                                            )
                                            this.successMessage = SingleEvent(
                                                PostPaidSuccessMessage(
                                                    friendlyMessage = "Jumlah tagihan kartu anda adalah ${data.totalPrice.toMoneyFormat()}",
                                                    titleMessage = "Berhasil mendapatkan jumlah tagihan"
                                                )
                                            )
                                        }
                                        PostPaidStatus.Fail -> {
                                            this.errorMessage = SingleEvent(
                                                PostPaidErrorMessage(
                                                    actualMessage = "Penyebab Error : ${data.message}",
                                                    friendlyMessage = "Hal tersebut dapat terjadi karena:\n1.Nomor anda salah\n2.Nomor bukan nomor pascabayar",
                                                    titleMessage = "Gagal cek tagihan"
                                                )
                                            )
                                        }
                                        PostPaidStatus.Pending -> {
                                            this.errorMessage = SingleEvent(
                                                PostPaidErrorMessage(
                                                    actualMessage = "Penyebab Error : ${data.message}",
                                                    friendlyMessage = "Hal tersebut dapat terjadi karena:\n1.Nomor anda salah\n2.Nomor bukan nomor pascabayar",
                                                    titleMessage = "Gagal cek tagihan"
                                                )
                                            )
                                        }
                                    }
                                })
                            }
                        )
                    }
            } else {
                setViewState(getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = "Anda harus memilih provider terlebih dahulu",
                            titleMessage = "Gagal cek tagihan pulsa"
                        )
                    )
                })
            }

        }
    }
    fun changeCurrentMode(virtualType: VirtualType) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.currentVirtual = virtualType
            this.selectedVirtualProduct = null
            this.sectionChange = SingleEvent(true)
        })
    }

    fun changeCurrentPDAM(pdamPascaBayar: PDAMPascaBayar){
        setViewState(getCurrentViewStateOrNew().apply {
            this.airSection.currentSelectedPDAMPascaBayar = pdamPascaBayar
            this.airSection.currentNomorAir = null
            this.sectionChange = SingleEvent(true)
        })
    }

    fun changeCurrentListrikMode(listrikType: VirtualPaymentType) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.listrikSection.currentListrikType = listrikType
            this.selectedVirtualProduct = null
            this.listrikSection.selectedListrikIndex = -1
            this.sectionChange = SingleEvent(true)
        })
    }

    fun changeCurrentPulsaType(VirtualPaymentType: VirtualPaymentType) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.pulsaSection.currentPulsaType = VirtualPaymentType
            this.selectedVirtualProduct = null
            this.sectionChange = SingleEvent(true)
        })
    }


    fun changeCurrentSelectedPrabayarProvider(provider: PulsaProvider) {
        setViewState(getCurrentViewStateOrNew().apply {
            pulsaSection.currentSelectedPrabayarProvider = provider
        })
    }

    fun changeCurrentSelectedPascaBayar(pulsaPascaBayar: PulsaPascaBayar) {
        setViewState(getCurrentViewStateOrNew().apply {
            pulsaSection.currentSelectedPascabayarPulsa = pulsaPascaBayar
        })
    }


    fun changeCurrentSelectedPrabayarPulsa(pulsaProduct: PulsaProduct) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.selectedVirtualProduct = SelectedVirtualProduct(
                null,
                pulsaProduct.skuCode,
                "",
                pulsaProduct.totalPrice,
                VirtualPaymentType.PraBayar,
                VirtualType.Pulsa,
                pulsaProduct.productName,
                adminFee = pulsaProduct.adminFee,
                warungFee = pulsaProduct.warungFee,
                subTotal = pulsaProduct.subtotal,
                productCategory = "Pulsa"
            )

        })
    }

    fun changeCurrentSelectedPrabayarListrik(item: ListrikProduct) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.selectedVirtualProduct = SelectedVirtualProduct(
                null,
                item.skuCode,
                "",
                item.totalPrice,
                VirtualPaymentType.PraBayar,
                VirtualType.Listrik,
                item.productName,
                adminFee = item.adminFee,
                warungFee = item.warungFee,
                subTotal = item.subTotal,
                productCategory = "PLN"
            )
        })
    }


    fun doAirPascaBayar(token:String, airNumber: String){
        viewModelScope.launch(IO){
            val currentViewState = getCurrentViewStateOrNew()
            if(airNumber == currentViewState.airSection.currentNomorAir){
                doPayment(token, currentViewState.selectedVirtualProduct, airNumber)
            }else{
                setViewState(getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = "Anda harus melakukan cek tagihan terlebih dahulu",
                            titleMessage = "Anda Belum cek tagihan!"
                        )
                    )
                })
            }
        }
    }

    fun doPulsaPascaBayarPayment(token: String, phoneNumberString: String, friendlyErrorMessage: String ){
        viewModelScope.launch(IO){
            val currentViewState = getCurrentViewStateOrNew()
            if(phoneNumberString == currentViewState.pulsaSection.currentPascaBayarPhoneNumber){
                doPayment(token, currentViewState.selectedVirtualProduct, phoneNumberString)
            }else{
                setViewState(getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = friendlyErrorMessage,
                            titleMessage = "Anda Belum cek tagihan!"
                        )
                    )
                })
            }
        }
    }

    fun doListrikPascabayarPayment(token: String, listrikNumber: String, friendlyErrorMessage: String ){
        viewModelScope.launch(IO){
            val currentViewState = getCurrentViewStateOrNew()
            if(listrikNumber == currentViewState.listrikSection.currentListrikNumber){
                doPayment(token, currentViewState.selectedVirtualProduct, listrikNumber)
            }else{
                setViewState(getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = friendlyErrorMessage,
                            titleMessage = "Anda Belum cek tagihan!"
                        )
                    )
                })
            }
        }
    }




    fun doPrabayarPayment(token: String, phoneNumberString: String, friendlyErrorMessage: String, titleErrorMessage : String){
        if(phoneNumberString.isNotEmpty()){
            doPayment(token,getCurrentViewStateOrNew().selectedVirtualProduct ,phoneNumberString)
        }else{
            setViewState(getCurrentViewStateOrNew().apply {
                this.errorMessage = SingleEvent(
                    PostPaidErrorMessage(
                        actualMessage = "",
                        friendlyMessage = friendlyErrorMessage,
                        titleMessage = titleErrorMessage
                    )
                )
            })
        }

    }

     fun reset(){
        setViewState(VirtualMainViewState())
     }

    private fun doPayment(token: String, currentSelectedProduct: SelectedVirtualProduct?, number: String) {
        viewModelScope.launch(IO) {
            if(currentSelectedProduct!= null){
                virtualInteractor.doPayment.fetch(token, currentSelectedProduct.apply {
                    customerNumber = number
                }).collect { resource ->
                    Log.w("TES http","RETURN ON")
                    onCollect(
                        response = resource,
                        onLoading = {
                            setViewState(getCurrentViewStateOrNew().apply {
                                isFetching = it
                            })
                        },
                        executeOnSuccess = { result ->
                            setViewState(getCurrentViewStateOrNew().apply {
                                when (result.data.status) {
                                    VirtualProductResultStatus.Success -> {
                                        this.selectedVirtualProduct = null
                                        this.successTransaction = SingleEvent(result.data)
                                    }
                                    VirtualProductResultStatus.Fail -> {
                                        this.errorMessage = SingleEvent(
                                            PostPaidErrorMessage(
                                                actualMessage = "Penyebab Error : ${result.data.message}",
                                                friendlyMessage = "Gagal melakukan pembelian. Coba beberapa saat lagi",
                                                titleMessage = "Gagal melakukan pembelian"
                                            )
                                        )
                                    }
                                    VirtualProductResultStatus.Pending -> {
                                        //dibawah ini popup pending
//                                        this.successMessage = SingleEvent(
//                                            PostPaidSuccessMessage(
//                                                friendlyMessage = "Pemebelian anda sedang dalam status pending. Pembelian mungkin berhasil atau gagal",
//                                                titleMessage = "Pembelian pending"
//                                            )
//                                        )

                                        this.selectedVirtualProduct = null
                                        this.successTransaction = SingleEvent(result.data)
                                    }
                                }
                            })
                        }
                    )
                }
            }else{
                setViewState(getCurrentViewStateOrNew().apply {
                    this.errorMessage = SingleEvent(
                        PostPaidErrorMessage(
                            actualMessage = "",
                            friendlyMessage = "Pilih terlebih dahulu product yang ingin anda beli ",
                            titleMessage = "Gagal melakukan pembelian"
                        )
                    )
                })
            }

        }
    }


    fun getCurrentSelectedListrikIndex(): Int =
        getCurrentViewStateOrNew().listrikSection.selectedListrikIndex

}