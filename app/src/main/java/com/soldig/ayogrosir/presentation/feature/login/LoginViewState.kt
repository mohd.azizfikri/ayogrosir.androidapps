package com.soldig.ayogrosir.presentation.feature.login

import android.os.Parcelable
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.User
import com.soldig.ayogrosir.utils.other.SingleEvent
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class LoginViewState(
    var user: User = User("", ""),
    private var _pos: POS? = null,
    var isLoading: Boolean = false,
) : Parcelable {

    @IgnoredOnParcel
    var token : SingleEvent<POS>? = null
    set(value) {
        println("New  token $value")
        _pos = value?.peek()
        field = value
    }
}