package com.soldig.ayogrosir.presentation.feature.home.customnominal

import com.soldig.ayogrosir.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class CustomNominalViewModel
@Inject constructor(

) : BaseViewModel<CustomNominalViewState>(){

    override fun initNewViewState(): CustomNominalViewState {
        return CustomNominalViewState()
    }

    fun setMinimumPrice(newMinimumPrice: Double){
        setViewState(getCurrentViewStateOrNew().apply {
            minimumPrice = newMinimumPrice
        })
    }

    fun setCurrentPrice(currentPrice: Double){
        setViewState(getCurrentViewStateOrNew().apply {
            inputtedPrice = currentPrice
        })
    }
}