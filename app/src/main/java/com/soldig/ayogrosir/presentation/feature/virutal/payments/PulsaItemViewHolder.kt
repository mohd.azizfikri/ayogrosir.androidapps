package com.soldig.ayogrosir.presentation.feature.virutal.payments

import android.content.res.ColorStateList
import android.view.View
import androidx.core.content.ContextCompat
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.PaymentMethodItemBinding
import com.soldig.ayogrosir.model.PulsaProduct
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class PulsaItemViewHolder(view: View) : RecyclerViewHolder<PulsaProduct>(view) {

    private val binding = PaymentMethodItemBinding.bind(view)

    fun bind(value : PulsaProduct, isSelected : Boolean = false,  onClick: (index: Int) -> Unit) {
        with(binding.btnPaymentType){
            text = value.totalPrice.toMoneyFormat()
            setOnClickListener {
                println("Is selected ? $isSelected" )
                onClick(adapterPosition)
            }
            backgroundTintList =  ColorStateList.valueOf(ContextCompat.getColor(context, if(isSelected) R.color.payment_on_click_bakcground else R.color.white))
            setTextColor(ColorStateList.valueOf(ContextCompat.getColor(context, if(isSelected) R.color.payment_on_click_text else R.color.text)))
            setStrokeColorResource(if(isSelected) R.color.payment_on_click_bakcground else R.color.black)
        }
    }
}