package com.soldig.ayogrosir.presentation.feature.home.payment

import android.content.res.ColorStateList
import android.view.View
import androidx.core.content.ContextCompat
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.PaymentMethodItemBinding
import com.soldig.ayogrosir.model.PaymentMethod
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class PaymentMethodViewHolder(view: View) : RecyclerViewHolder<PaymentMethod>(view) {

    private val binding = PaymentMethodItemBinding.bind(view)

    fun bind(paymentMethod: PaymentMethod, isClicked: Boolean, onPaymentClickedListener : (paymentMethod: PaymentMethod, index:Int) -> Unit ){
        with(binding.btnPaymentType){
            text = paymentMethod.name.toDoubleOrNull()?.toMoneyFormat() ?: paymentMethod.name
            setOnClickListener {
                if(isClicked){
                    onPaymentClickedListener(paymentMethod, -1)
                }else{
                    onPaymentClickedListener(paymentMethod, adapterPosition)
                }

            }
            val context = binding.root.context
            backgroundTintList =  ColorStateList.valueOf(ContextCompat.getColor(context, if(isClicked) R.color.payment_on_click_bakcground else R.color.white))
            setTextColor(ColorStateList.valueOf(ContextCompat.getColor(context, if(isClicked) R.color.payment_on_click_text else R.color.text)))
            setStrokeColorResource(if(isClicked) R.color.payment_on_click_bakcground else R.color.btn_yelow)
        }
    }

}
