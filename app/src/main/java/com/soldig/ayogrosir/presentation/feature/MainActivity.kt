package com.soldig.ayogrosir.presentation.feature

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ui.AppBarConfiguration
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.common.startActivity
import com.soldig.ayogrosir.databinding.ActivityMainBinding
import com.soldig.ayogrosir.databinding.FragmentHomeBinding
import com.soldig.ayogrosir.databinding.NavHeaderMainBinding
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.presentation.feature.login.LoginActivity
import com.soldig.ayogrosir.presentation.feature.order.orderlist.OrderTransactionActivity
import com.soldig.ayogrosir.presentation.feature.report.ReportShopActivity
import com.soldig.ayogrosir.presentation.feature.shop.closeshop.CloseShopActivity
import com.soldig.ayogrosir.presentation.feature.shop.setting.SettingActivity
import com.soldig.ayogrosir.presentation.feature.transaction.history.HistoryTransactionActivity
import com.soldig.ayogrosir.presentation.feature.virutal.payments.VirtualMainFragment
import com.soldig.ayogrosir.utils.ext.getUserInSharedPref
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*
import com.soldig.ayogrosir.presentation.feature.home.HomeFragment
import com.soldig.ayogrosir.presentation.feature.splash.SplashScreenActivity
import com.soldig.ayogrosir.presentation.feature.topup_virtual.GetSaldoViewModel
import com.soldig.ayogrosir.utils.ext.goVisible
import com.soldig.ayogrosir.utils.ext.showShortSnackbar
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import com.soldig.ayogrosir.utils.other.UIInteraction
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@AndroidEntryPoint
class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val viewModelSaldo: GetSaldoViewModel by viewModels()

    lateinit var currentPOS : POS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        currentPOS = getUser()
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(view)

        val calendar = Calendar.getInstance()
        val id = Locale("in", "ID")
        val simpleDateFormat = SimpleDateFormat("EEEE, dd MMMM yyyy", id)
        val dateTime = simpleDateFormat.format(calendar.time)
        binding.include.tvTime.text = dateTime

        binding.include.mToolbar.goVisible()
        setSupportActionBar(binding.include.mToolbar)

        val radius = resources.getDimension(R.dimen.roundcorner)
        val navViewBackground = binding.navView.background as MaterialShapeDrawable
        navViewBackground.shapeAppearanceModel = navViewBackground.shapeAppearanceModel
            .toBuilder()
            .setTopRightCorner(CornerFamily.ROUNDED, radius)
            .setBottomRightCorner(CornerFamily.ROUNDED, radius)
            .build()


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.include.mToolbar,
            R.string.open_drawer_desc,
            R.string.close_drawer_desc
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        binding.navView.setNavigationItemSelectedListener(this)
        binding.navView.getHeaderView(0)?.let{
            val navHeaderBinding = NavHeaderMainBinding.bind(it)
            navHeaderBinding.usernameTxt.text = currentPOS.name
            Glide
                .with(this)
                .load(currentPOS.getFullImageURL())
                .into(navHeaderBinding.imgProPic)
        }
        supportFragmentManager.beginTransaction()
            .replace(R.id.nav_host_fragment, HomeFragment())
            .commit()
        binding.navClose.setOnClickListener {
            startActivity<CloseShopActivity> {  }
        }
        supportActionBar?.let{
            it.title = currentPOS.name
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_drawer)
        }

        vmHandle()
        viewModelSaldo.getSaldo(currentPOS.token)
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        return super.onKeyDown(keyCode, event)
    }
    override fun onStart() {
        super.onStart()
        viewModelSaldo.getSaldo(currentPOS.token)
    }

    fun toggleLoading(isLoading: Boolean){
        binding.loadingRoot.root.toggleVisibility(isLoading)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                val fragment = HomeFragment();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment)
                    .commit();
            }
            R.id.nav_update -> {
                lifecycleScope.launch(IO) {
                    delay(500)
                    withContext(Dispatchers.Main) {
//                        if(isNetworkAvailable()) {
                            toggleLoading(true)
                            viewModelSaldo.syncWithServer(currentPOS)
                            Log.w("KaWaNSAD aa 0", "product/list: "+ "Awal")
//                        }else{
//                            binding.root.showShortSnackbar("Tidak ada akses internet! Periksa koneksi anda")
//                        }
                    }
                }
//                toggleLoading(true)
//                viewModelSaldo.syncWithServer(currentPOS)
            }
            R.id.nav_order -> {
                startActivity<OrderTransactionActivity> { }
            }
            R.id.nav_history -> {
                startActivity<HistoryTransactionActivity> { }
            }
            R.id.nav_report -> {
                startActivity<ReportShopActivity> { }
            }
            R.id.nav_setting -> {
                startActivity<SettingActivity> { }
            }
            R.id.nav_product_virtual -> {
                val fragment = VirtualMainFragment();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment)
                    .commit();
            }
        }
        //close navigation drawer
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return false
    }

    private fun getUser() : POS = getUserInSharedPref() ?: logout()


    override fun onBackPressed() {
        if(binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }else{
            super.onBackPressed()
        }


    }

    private fun logout() : POS {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
        return POS.produceFailedPOS()
    }


    private fun refreshApps() : POS {
        startActivity(Intent(this, SplashScreenActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
        finish()
        return POS.produceFailedPOS()
    }

    fun vmHandle(){
        viewModelSaldo.viewState.observe(this, { viewState ->
            with(viewState) {
                viewState.responGetSaldo?.get()?.let {

                    try {
                        val saldoku = it.data?.get(0)?.saldoWarung?.toInt()
                        println("saldoku$saldoku")
                        if (saldoku != null) {
                            if (saldoku <= 10000) {
                                binding.navView.getHeaderView(0)?.let {
                                    val navHeaderBinding = NavHeaderMainBinding.bind(it)
                                    navHeaderBinding.tvSaldoMinus.visibility = View.VISIBLE

                                }
                            }
                        }
                    }catch (e: Exception){
                        binding.navView.getHeaderView(0)?.let {
                            val navHeaderBinding = NavHeaderMainBinding.bind(it)
                            navHeaderBinding.tvSaldoMinus.visibility = View.VISIBLE

                        }
                    }

                }


            }
        })

        viewModelSaldo.userInteraction.observe(this, {
            val interaction = it.get() ?: return@observe
            if (interaction is UIInteraction.GenericMessage) {
                binding.root.showShortSnackbar(interaction.getMessage(this))
            }

        })

        viewModelSaldo.isUpdateDone.observe(this){
            if(it)refreshApps()
        }
    }
}