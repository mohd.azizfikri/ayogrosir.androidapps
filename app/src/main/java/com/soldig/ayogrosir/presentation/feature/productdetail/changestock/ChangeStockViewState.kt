package com.soldig.ayogrosir.presentation.feature.productdetail.changestock

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class ChangeStockViewState(
    var newStock : Int
) : Parcelable{
}