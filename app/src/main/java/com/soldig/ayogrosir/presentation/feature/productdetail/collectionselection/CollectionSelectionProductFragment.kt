package com.soldig.ayogrosir.presentation.feature.productdetail.collectionselection

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseDialogFragment
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.CollectionSelectionProductFragmentBinding
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.presentation.feature.productdetail.ProductDetailViewModel
import com.soldig.ayogrosir.utils.ext.goGone
import com.soldig.ayogrosir.utils.ext.showShortSnackbar
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf

@AndroidEntryPoint
class CollectionSelectionProductFragment : BaseDialogFragment() {
    private var _binding: CollectionSelectionProductFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<SelectionViewModel>()
    private val activityViewModel by activityViewModels<ProductDetailViewModel>()

    private lateinit var mListener: OnSelectionSelectedListener
    private lateinit var currentProductId: String

    fun setListener(listener: OnSelectionSelectedListener) {
        mListener = listener
    }

    fun setCurrentProductId(productId: String) {
        currentProductId = productId
    }


    private val currentPOS: POS by lazy {
        val activity = requireActivity()
        if (activity is BaseLoggedInActivity) {
            activity.currentPOS
        } else {
            throw  Exception("This fragment only for loggedin activity")
        }
    }

    private val selectionAdapter =
        adapterOf<SelectionViewType> {
            diff(
                areItemsTheSame = { old, new ->
                    old.areItemTheSame(new)
                },
                areContentsTheSame = { old, new ->
                    false
                }
            )
            register(
                layoutResource = R.layout.collection_selection_product_item,
                viewHolder = ::SelectionProductExistViewHolder,
                onBindViewHolder = { vh, _, it ->
                    with(vh.binding) {
                        Glide
                            .with(root)
                            .load(it.product.getImageURL())
                            .into(imgProduct)
                        with(btnUse) {
                            val isSameProduct = it.product.productId == currentProductId
                            text = if (isSameProduct) "Hapus" else "Ganti"
                            backgroundTintList = ColorStateList.valueOf(
                                ContextCompat.getColor(
                                    context,
                                    if (isSameProduct) R.color.white else R.color.primary
                                )
                            )
                            setTextColor(
                                ColorStateList.valueOf(
                                    ContextCompat.getColor(
                                        context,
                                        if (!isSameProduct) R.color.white else R.color.primary
                                    )
                                )
                            )
                            setOnClickListener {
                                if (isSameProduct) {
                                    mListener.onDelete(vh.adapterPosition + 1)
                                } else {
                                    mListener.onAdd(vh.adapterPosition + 1)
                                }

                            }
                        }
                    }
                }
            )
            register(
                layoutResource = R.layout.collection_selection_empty_item,
                viewHolder = ::SelectionEmptyViewHolder,
                onBindViewHolder = { vh, _, it ->
                    with(vh.binding) {
                        btnUse.setOnClickListener {
                            mListener.onAdd(vh.adapterPosition + 1)
                        }
                    }
                }
            )
        }

    override fun rootView(): View {
        return binding.root
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = CollectionSelectionProductFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeVM()
    }

    private fun observeVM() {
        viewModel.viewState.observe(viewLifecycleOwner) {
            selectionAdapter.submitList(it.selectedList)
            binding.rvListProduct.toggleVisibility(!it.isFetchingList)
            binding.loading.toggleVisibility(it.isFetchingList)
        }
        activityViewModel.viewState.observe(viewLifecycleOwner) {
            binding.bigLoading.root.toggleVisibility(it.productSelectionUpdate.isSelectionUpdating)
            binding.loading.toggleVisibility(it.productSelectionUpdate.isSelectionUpdating)
            it.productSelectionUpdate.successMessage?.get()?.let{ message ->
                binding.root.showShortSnackbar(message)
            }
            it.productSelectionUpdate.shouldFetchNewList?.get()?.let{
                viewModel.getAllAvailableProduct(currentPOS)
            }

        }
    }

    private fun initView() {
        initClickListener()
        initSelectionRV()
        binding.bigLoading.progresbar.goGone()
    }

    private fun initSelectionRV() {
        binding.rvListProduct.adapter = selectionAdapter
    }

    private fun initClickListener() {
        with(binding) {
            btnCancel.setOnClickListener {
                dismiss()
            }
        }
    }

    interface OnSelectionSelectedListener {
        fun onAdd(position: Int)
        fun onDelete(position: Int)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}