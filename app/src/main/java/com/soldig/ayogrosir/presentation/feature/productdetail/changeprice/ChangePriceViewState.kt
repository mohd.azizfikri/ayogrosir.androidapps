package com.soldig.ayogrosir.presentation.feature.productdetail.changeprice

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChangePriceViewState(
    var changePriceMode: ChangePriceMode = ChangePriceMode.BUY,
    var currentPrice : Double = 0.0,
    var isPriceChanged : Boolean = false
): Parcelable