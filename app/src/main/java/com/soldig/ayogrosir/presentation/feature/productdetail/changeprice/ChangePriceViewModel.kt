package com.soldig.ayogrosir.presentation.feature.productdetail.changeprice

import com.soldig.ayogrosir.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ChangePriceViewModel
@Inject constructor(

) : BaseViewModel<ChangePriceViewState>(){


    fun setCurrentPrice(newPrice : Double){
        setViewState(getCurrentViewStateOrNew().apply {
            if (newPrice < currentPrice) {
                currentPrice
                isPriceChanged = false
            }
            currentPrice = newPrice
            isPriceChanged = true
        })
    }

    fun setCurrentMode(newMode : ChangePriceMode){
        setViewState(getCurrentViewStateOrNew().apply {
            println("Change current mode ${newMode.name}")
            changePriceMode = newMode
        })
    }

    override fun initNewViewState(): ChangePriceViewState {
        return ChangePriceViewState()
    }
}