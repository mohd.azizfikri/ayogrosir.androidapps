package com.soldig.ayogrosir.presentation.feature.productdetail.changediscount

import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.model.DiscountType
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class ChangeDiscountViewModel
@Inject constructor(

): BaseViewModel<ChangeDiscountViewState>(){
    override fun initNewViewState(): ChangeDiscountViewState {
        return ChangeDiscountViewState()
    }


    fun onNominalValueChanged(newNominalValue : Double){
        setViewState(getCurrentViewStateOrNew().apply {
            nominalValue = newNominalValue
        })
    }


    fun onPercentageValueChanged(newPercentageValue: Double){
        setViewState(getCurrentViewStateOrNew().apply {
            percentageValue = newPercentageValue
        })
    }


    fun setCurrentDiscountType(discountType: DiscountType){
        setViewState(getCurrentViewStateOrNew().apply {
            this.discountType = discountType
        })
    }
}