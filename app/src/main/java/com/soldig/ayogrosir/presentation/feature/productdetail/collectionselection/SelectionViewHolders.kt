package com.soldig.ayogrosir.presentation.feature.productdetail.collectionselection

import android.view.View
import com.soldig.ayogrosir.databinding.CollectionSelectionEmptyItemBinding
import com.soldig.ayogrosir.databinding.CollectionSelectionProductItemBinding
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class SelectionProductExistViewHolder(view: View) : RecyclerViewHolder<SelectionViewType.ProductExist>(view){

    val binding = CollectionSelectionProductItemBinding.bind(view)
}

class SelectionEmptyViewHolder(view: View) : RecyclerViewHolder<SelectionViewType.Empty>(view){

    val binding = CollectionSelectionEmptyItemBinding.bind(view)
}
