package com.soldig.ayogrosir.presentation.feature.category.categorylist

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.soldig.ayogrosir.model.ProductCategory

class CategoryContract : ActivityResultContract<Nothing, ProductCategory?>() {

    override fun createIntent(context: Context, input: Nothing?): Intent = Intent(context, CategoryActivity::class.java)

    override fun parseResult(resultCode: Int, intent: Intent?): ProductCategory? {
        return when(resultCode){
            Activity.RESULT_OK -> {
                intent?.getParcelableExtra<ProductCategory>(CategoryActivity.CATEGORY_CONTRACT_KEY)
            }
            else -> null
        }
    }

}