package com.soldig.ayogrosir.presentation.feature.shop.setting

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.soldig.ayogrosir.model.Schedule
import com.soldig.ayogrosir.databinding.ScheduleListItemBinding

class ScheduleAdapter(
    private val onAturClicked : (schedule: Schedule) -> Unit
) : RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {

    private val list: MutableList<Schedule> = mutableListOf<Schedule>()

    fun newList(list: List<Schedule>){
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }
    inner class ViewHolder(val binding: ScheduleListItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ScheduleListItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(list[position]) {
                binding.tvDay.text = this.day
                binding.tvHour.text = this.hour
                binding.btnChange.setOnClickListener {
                    onAturClicked(this)
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }
}

