package com.soldig.ayogrosir.presentation.feature.order.orderlist

import android.view.View
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.OrderTransactionListItemBinding
import com.soldig.ayogrosir.model.TransactionHistory
import com.soldig.ayogrosir.model.TransactionStatus
import com.soldig.ayogrosir.utils.ext.goVisible
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class OrderTransactionViewHolder(view: View): RecyclerViewHolder<TransactionHistory>(view) {

    private val binding = OrderTransactionListItemBinding.bind(view)

    fun bind(transactionHistory: TransactionHistory, orderTransactionListListener: OrderTransactionListListener){
        with(binding){
            with(transactionHistory){
                tvIdTransaction.text = orderNumber
                tvDate.text = orderDate
                tvTotalQty.text = totalItem.toString()
                tvTotalPrice.text = totalPrice.toMoneyFormat()
                tvStatus.text = status.getInString()

                tvStatus.setTextColor(status.getStatusTextColor())

                btnRejected.toggleVisibility(status == TransactionStatus.MENUNGGU_DITERIMA)
                btnAccept.toggleVisibility(status == TransactionStatus.MENUNGGU_DITERIMA)
                btnDeliver.toggleVisibility(status == TransactionStatus.SUDAH_DIBAYAR)
                btnDone.toggleVisibility(status == TransactionStatus.DIKIRIM)
                btnDetail.goVisible()

                btnRejected.setOnClickListener { orderTransactionListListener.onRejectClicked(transactionHistory) }
                btnAccept.setOnClickListener { orderTransactionListListener.onAcceptClicked(transactionHistory) }
                btnDeliver.setOnClickListener { orderTransactionListListener.onDeliverClicked(transactionHistory)}
                btnDone.setOnClickListener { orderTransactionListListener.onDoneClicked(transactionHistory)}
                btnDetail.setOnClickListener { orderTransactionListListener.onDetailClicked(transactionHistory) }
            }
        }


    }
    interface OrderTransactionListListener {
        fun onDetailClicked(transactionHistory: TransactionHistory)
        fun onDeliverClicked(transactionHistory: TransactionHistory)
        fun onRejectClicked(transactionHistory: TransactionHistory)
        fun onDoneClicked(transactionHistory: TransactionHistory)
        fun onAcceptClicked(transactionHistory: TransactionHistory)
    }

}