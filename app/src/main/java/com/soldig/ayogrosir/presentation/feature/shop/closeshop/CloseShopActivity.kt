package com.soldig.ayogrosir.presentation.feature.shop.closeshop

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.dantsu.escposprinter.EscPosPrinter
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.dantsu.escposprinter.textparser.PrinterTextParserImg
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.CloseShopActivityBinding
import com.soldig.ayogrosir.model.ProductReport
import com.soldig.ayogrosir.model.ReportWallet
import com.soldig.ayogrosir.presentation.commonlist.payment.PaymentViewHolder
import com.soldig.ayogrosir.presentation.commonlist.reportlist.ReportViewHolder
import com.soldig.ayogrosir.utils.ext.launchInformationDialog
import com.soldig.ayogrosir.utils.ext.showLongSnackbar
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.text.SimpleDateFormat
import java.util.*


@AndroidEntryPoint
class CloseShopActivity : BaseLoggedInActivity() {
    private lateinit var binding: CloseShopActivityBinding

    private val viewModel : CloseShopViewModel by viewModels()

    companion object {
        private const val BLUETOOTH_AND_FINE_LOCATION = 50
    }

    private val soldAdapter = adapterOf<ProductReport> {
        diff(
            areItemsTheSame = { old: ProductReport, new: ProductReport -> false},
            areContentsTheSame = { _: ProductReport, _: ProductReport -> false }
        )
        register(
            viewHolder = ::ReportViewHolder,
            layoutResource = R.layout.report_product_item,
            onBindViewHolder = { vh, _, it ->
                vh.bind(it)
            }
        )
    }

    private val paymentAdapter = adapterOf<ReportWallet> {
        diff(
            areItemsTheSame = { old: ReportWallet, new: ReportWallet -> false},
            areContentsTheSame = { _: ReportWallet, _: ReportWallet -> false }
        )
        register(
            viewHolder = ::PaymentViewHolder,
            layoutResource = R.layout.report_product_item,
            onBindViewHolder = { vh, _, it ->
                vh.bind(it)
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CloseShopActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
        viewModel.fetchReport(currentPOS, Date(currentPOS.loginTime))
    }

    @SuppressLint("SetTextI18n")
    private fun observeVM() {
        viewModel.viewState.observe(this){ viewState ->
            if(viewState.isClosed) logout()
            with(binding){
                val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
                tvDate.text =  ": ${simpleDateFormat.format(Date())}"
                viewState.report.let{
                    soldAdapter.submitList(it.soldProduct)
                    paymentAdapter.submitList(it.reportWallet)
                    tvTotalBeginingBalance.text = ": ${it.beginningBalanceTotal.toMoneyFormat()}"
                    tvTotalBill.text = ": ${it.billTotal}"
                    tvTotalSoldProduct.text = ": ${it.soldProductQty}"
                    tvTotalCanceledProduct.text  = ": ${it.canceledProductQty}"
                    tvTotalCanceledBill.text = ": ${it.canceledBill}"
                    tvTotalCancledAmount.text  = ": ${it.totalCanceledAmount.toMoneyFormat()}"
                    tvTotalDiscount.text = ": ${it.discountTotal.toMoneyFormat()}"
                    tvTotalAmount.text= ": ${it.totalAmountWithoutDiscount.toMoneyFormat()}"
                    tvTotalAmountAfterDiscount.text = ": ${it.totalAmountAfterDiscount.toMoneyFormat()}"
                    tvTotalCashTransaction.text = ": ${it.totalAmountPaidWithCash.toMoneyFormat()}"
                    tvTotalNonCashTransaction.text = ": ${it.totalAmountPaidWithNotCash.toMoneyFormat()}"
                    tvTotalCashAmount.text = ": ${it.totalAllCash.toMoneyFormat()}"
                }
                loading.root.toggleVisibility(viewState.isFetching)
                btnPrint.setOnClickListener {
                    println(viewState.printedString)
                    onPrint(viewState.printedString)
                }
                if(viewState.isClosed) logout()
            }
        }

        viewModel.userInteraction.observe(this) {
            val uiInteraction = it.get() ?: return@observe
            if (uiInteraction !is UIInteraction.DoNothing) {
                binding.root.showLongSnackbar(uiInteraction.getMessage(this))
            }
        }
    }

    private fun initView() {
        initToolbar(getString(R.string.drawer_item_close_shop),binding.toolbar)
        initClickListener()
        initSoldProductRV()
        initPaymentRV()
    }

    private fun initPaymentRV() {
        with(binding.rvPayment){
            layoutManager = LinearLayoutManager(context)
            adapter = paymentAdapter
        }
    }

    private fun initSoldProductRV() {
        with(binding.rvSold){
            layoutManager = LinearLayoutManager(context)
            adapter = soldAdapter
        }
    }

    @AfterPermissionGranted(BLUETOOTH_AND_FINE_LOCATION)
    private fun onPrint(printedString: String) {
        println("After pemission granted")
        if (EasyPermissions.hasPermissions(
                this,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            doPrint(printedString)
        } else {
            println("Tidak punya permission")
            EasyPermissions.requestPermissions(
                this,
                "Blueetoth dan Loakasi",
                BLUETOOTH_AND_FINE_LOCATION,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun doPrint(printedString : String) {
        try{
            println("Printing")
            val printer = EscPosPrinter(BluetoothPrintersConnections.selectFirstPaired(), 203, 48f, 32)
            printer
                .printFormattedText("[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, this.getApplicationContext().getResources().getDrawableForDensity(R.drawable.ic_logo_kawanmart, DisplayMetrics.DENSITY_MEDIUM))+"</img>\n" + printedString)
        }catch(exception : Exception){
            launchInformationDialog(
                message = "Cek Koneksi dengan printer lalu coba lakukan print lagi",
                title = "Gagal melakukan print"
            )
        }

    }

    private fun initClickListener() {
        with(binding){
            btnExit.setOnClickListener {
                viewModel.closeShop(currentPOS.token)
            }
        }
    }
}