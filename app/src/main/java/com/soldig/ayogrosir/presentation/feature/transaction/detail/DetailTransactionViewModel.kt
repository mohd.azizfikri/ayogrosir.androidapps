package com.soldig.ayogrosir.presentation.feature.transaction.detail

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.interactors.transaction.TransactionInteractor
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.TransactionDetail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailTransactionViewModel
@Inject constructor(
    private val transactionInteractor: TransactionInteractor
) : BaseViewModel<DetailTransactionViewState>() {

    override fun initNewViewState(): DetailTransactionViewState {
        return DetailTransactionViewState()
    }

    fun getTransactionDetail(pos: POS, id: String) {
        viewModelScope.launch(IO) {
            transactionInteractor.getTransactionDetail.fetch(pos, id).collect {
                onCollect(
                    response = it,
                    onLoading = {
                        setViewState(getCurrentViewStateOrNew().apply {
                            isFetching = it
                        })
                    },
                    executeOnSuccess = {
                        setTransactionDetail(pos, it.data)
                    }
                )
            }
        }
    }
    fun cancelTransaction(token: String, id: String) {
        viewModelScope.launch(IO) {
            transactionInteractor.cancelTransaction.fetch(token, id).collect {
                onCollect(
                    response = it,
                    onLoading = {
                        setViewState(getCurrentViewStateOrNew().apply {
                            isFetching = it
                        })
                    },
                    executeOnSuccess = {
                        setViewState(getCurrentViewStateOrNew().apply {
                            isTransactionCanceled = true
                        })
                    }
                )
            }
        }
    }
    private fun setTransactionDetail(pos: POS, transactionDetail: TransactionDetail) {
        viewModelScope.launch(Default) {
            val transactionList = ArrayList<ArrayList<String>>()
            transactionList.add(arrayListOf("Produk", "Jml", "Harga"))
            transactionDetail.productList.forEachIndexed { index, it ->
                transactionList.add(
                    arrayListOf(
                        it.name,
                        it.jml.toString(),
                        it.priceAfterDiscount.toMoneyFormat()
                    )
                )
                if (it.discountValue != 0.0) {
                    transactionList.add(arrayListOf("Diskon", it.discountValue.toMoneyFormat()))
                }
                if (index != transactionDetail.productList.lastIndex) {
                    transactionList.add(arrayListOf())
                }
            }
            val printedString = getPrintString(pos, transactionDetail)
            setViewState(getCurrentViewStateOrNew().apply {
                this.transactionProductList = transactionList
                this.transactionDetail = transactionDetail
                this.printedString = printedString
            })
        }
    }
    private fun getPrintString(currentPOS: POS, transactionResult: TransactionDetail): String {

        val header =
                    "[L]\n" +
                    "[C]${currentPOS.name}\n" +
                    "[C]${currentPOS.alamat}\n\n" +
                    "[L]No  ${transactionResult.orderNumber}\n" +
                    "[L]TGL ${transactionResult.date.split(" ")[0]}\n" +
                    "[L]JAM ${transactionResult.date.split(" ")[1]}\n" +
                    "[C]================================\n"

        var itemDetailContent = ""

        transactionResult.productList.forEachIndexed { index, transactionProductDetail ->
            itemDetailContent += ("[L]\n" +
                    "[L]${index + 1} <b>${transactionProductDetail.name}</b>\n" +
                    "[L]  ${transactionProductDetail.priceBeforeDiscount.toMoneyFormat()} x ${transactionProductDetail.jml}[R]${(transactionProductDetail.priceBeforeDiscount * transactionProductDetail.jml).toMoneyFormat()}\n")

            if (transactionProductDetail.discountValue > 0.0) {
                itemDetailContent += "[L]  Diskon[R]${(transactionProductDetail.priceBeforeDiscount - transactionProductDetail.priceAfterDiscount).toMoneyFormat()}\n"
            }
        }

        val footer =
            "[C]================================\n" +
                    "[L]\n" +
                    "[R]TOTAL :[R]${transactionResult.totalAfterDiscount.toMoneyFormat()}\n" +
                    "[R]Bayar :[R]${transactionResult.totalPaid.toMoneyFormat()}\n" +
                    "[R]Pembayaran :[R]${transactionResult.paymentWith}\n" +
                    "[R]Kembalian :[R]${transactionResult.totalChange.toMoneyFormat()}\n" +
                    "[R]Anda Hemat :[R]${transactionResult.totalDiscount.toMoneyFormat()}\n" +
                    "[L]\n" +
                    "[L]\n" +
                    "[L]TERIMAKASIH TELAH BERBELANJA\n" +
                    "[L]\n" +
                    "[L]\n"

        return header + itemDetailContent + footer
    }


}