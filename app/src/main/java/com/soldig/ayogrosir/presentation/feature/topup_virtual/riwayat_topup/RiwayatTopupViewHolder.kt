package com.soldig.ayogrosir.presentation.feature.topup_virtual.riwayat_topup

import android.graphics.Color
import android.view.View
import android.widget.Filter
import android.widget.Filterable
import com.bumptech.glide.Glide
import com.soldig.ayogrosir.databinding.ItemRiwayatTopupBinding
import com.soldig.ayogrosir.model.DataItemRiawayat
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder
import java.text.DecimalFormat

class RiwayatTopupViewHolder(view: View) : RecyclerViewHolder<DataItemRiawayat>(view), Filterable{

    val binding = ItemRiwayatTopupBinding.bind(view);
    lateinit var modelList: MutableList<DataItemRiawayat>
    private var models: MutableList<DataItemRiawayat> = ArrayList()

    init {
        modelList = models
    }

    fun bind(data : DataItemRiawayat){
        with(binding) {

            Glide
                .with(root)
                .load(data.buktiTransfer)
                .into(binding.imgBukti)
            binding.tvOrderNumber.text
            binding.tvDate.text = data.tglTopup
            binding.tvJumlah.text = "Rp. " + currencyBanyak(data.jmlTopup.toString())
            if (data?.kodeStatusPengajuan.equals("0")){
                binding.tvStatus.setTextColor(Color.parseColor("#FFAA00"))
            } else if(data?.kodeStatusPengajuan.equals("1")) {
                binding.tvStatus.setTextColor(Color.parseColor("#076AE1"))
            } else {
                binding.tvStatus.setTextColor(Color.parseColor("#FF0000"));
            }
            binding.tvStatus.text = data.statusPengajuan
            binding.tvKeterangan.text = data.alasanDitolak
            binding.tvOrderNumber.text = data.no

//                binding.root.setOnClickListener {
//                    onRiwayatClicked(this)
//                }
        }
    }

    fun currencyBanyak(s: String): String = DecimalFormat("###,###,###,###,###")
        .format(s.toDouble()).replace(",", ".")


     override fun getFilter(): Filter {

        return object : Filter() {
            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                modelList = p1!!.values as MutableList<DataItemRiawayat>

            }

            override fun performFiltering(p0: CharSequence?): FilterResults {
                val charString: String = p0.toString()
                modelList = if (charString.isEmpty()) {
                    models
                } else {
                    val filteredList: MutableList<DataItemRiawayat> = mutableListOf()
                    for (s: DataItemRiawayat in models) {
                        when {
                            s.statusPengajuan!!.toLowerCase().contains(charString.toLowerCase()) -> filteredList.add(s)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = modelList
                return filterResults
            }
        }
    }

}