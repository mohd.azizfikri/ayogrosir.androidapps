package com.soldig.ayogrosir.presentation.feature.productdetail.changediscount

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.afollestad.materialdialogs.utils.MDUtil.textChanged
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseDialogFragment
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.ChangeDiscountFragmentBinding
import com.soldig.ayogrosir.presentation.feature.productdetail.ProductDetailViewModel
import com.soldig.ayogrosir.model.DiscountType
import com.soldig.ayogrosir.model.Product
import com.soldig.ayogrosir.utils.Constant
import com.soldig.ayogrosir.utils.ext.goGone
import com.soldig.ayogrosir.utils.ext.goVisible
import com.soldig.ayogrosir.utils.validator.CommonStringValidator
import com.soldig.ayogrosir.utils.validator.DoubleValidator
import com.soldig.ayogrosir.utils.validator.InputValidation
import com.soldig.ayogrosir.utils.validator.then
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ChangeDiscountFragment : BaseDialogFragment() {
    private var _binding: ChangeDiscountFragmentBinding? = null
    private val binding get() = _binding!!


    private val activityViewModel by activityViewModels<ProductDetailViewModel>()
    private val viewModel by viewModels<ChangeDiscountViewModel>()

    private lateinit var mListener: OnChangeDiscountListener




    fun setListener(listener: OnChangeDiscountListener) {
        mListener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = ChangeDiscountFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }







    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeVM()
    }

    override fun rootView(): View = binding.root


    private fun observeVM() {
        activityViewModel.viewState.observe(viewLifecycleOwner) {
            populateProduct(it.chosenProduct)
        }
        viewModel.viewState.observe(viewLifecycleOwner) {
            handleDiscountTypeChange(it.discountType)
        }
    }

    private fun populateProduct(chosenProduct: Product?) {
        chosenProduct ?: return
        val currentViewState = viewModel.getCurrentViewStateOrNew()
        with(binding) {
            val isRadioGroupEmpty =
                !radioNothing.isChecked && !radioNominal.isChecked && !radioPercentage.isChecked
            if (isRadioGroupEmpty) {
                when (chosenProduct.discountType) {
                    DiscountType.Nominal -> {
                        val nominalValue = currentViewState.nominalValue
                        etNominal.setText(if (nominalValue == 0.0) chosenProduct.discountValue.toLong().toString() else nominalValue.toLong().toString())
                    }
                    DiscountType.Percentage -> {
                        val percentageValue = currentViewState.percentageValue
                        etPercentage.setText(if (percentageValue == 0.0) chosenProduct.discountValue.toLong().toString() else percentageValue.toLong().toString())
                    }
                    else -> {
                    }
                }
                viewModel.setCurrentDiscountType(chosenProduct.discountType)
            }

        }
    }

    private fun handleDiscountTypeChange(discountType: DiscountType) {
        with(binding) {
            when (discountType) {
                DiscountType.Nominal -> {
                    radioNominal.isChecked = true
                    nominalAndPercentageContainer.goVisible()
                    etNominal.goVisible()
                    etPercentageContainer.goGone()
                    keyboard.goVisible()
                    keyboard.field = etNominal
                    etNominal.setSelection(etNominal.text?.length ?: 0)
                }
                DiscountType.Percentage -> {
                    radioPercentage.isChecked = true
                    nominalAndPercentageContainer.goVisible()
                    etNominal.goGone()
                    etPercentageContainer.goVisible()
                    keyboard.goVisible()
                    keyboard.field = etPercentage
                    etPercentage.setSelection(etPercentage.text.length)
                }
                DiscountType.Nothing -> {
                    radioNothing.isChecked = true
                    nominalAndPercentageContainer.goGone()
                    keyboard.goGone()
                }
            }
        }

    }


    private fun initView() {
        initListener()
        initKeyboardOnEditText()
        initCurrencyEditText()
        initEditTextListener()
        setRadioButtonListener()
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun initKeyboardOnEditText() {
        with(binding) {
            etNominal.setOnTouchListener { view, motionEvent ->
                requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                binding.keyboard.field = binding.etNominal
                false
            }
            etPercentage.setOnTouchListener { view, motionEvent ->
                requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                binding.keyboard.field = binding.etPercentage
                false
            }
        }

    }

    private fun initEditTextListener() {
        binding.etNominal.textChanged {

            val nominalValue = binding.etNominal.rawValue.toDouble()
            nominalValidator(nominalValue)
            viewModel.onNominalValueChanged(nominalValue)
        }
        binding.etPercentage.textChanged {
            val percentageValue = it.toString().toDoubleOrNull() ?: 0.0
            percentageValidator(percentageValue)
            viewModel.onPercentageValueChanged(percentageValue)
        }
    }

    private fun initCurrencyEditText() {
        with(binding.etNominal) {
            locale = Locale("id", "ID")
            decimalDigits = 0
        }
    }

    private fun initListener() {
        with(binding) {
            btnCancel.setOnClickListener {
                dismiss()
            }
            btnSave.setOnClickListener {
                dismiss()
                val viewState = viewModel.getCurrentViewStateOrNew()
                mListener.onSaveChangeDiscount(
                    viewState.discountType,
                    viewState.getChoosenTypeValue()
                )
            }
            dialogParent.setOnClickListener {
                dismiss()
            }
        }
    }

    private fun setRadioButtonListener() {
        val viewState = viewModel.getCurrentViewStateOrNew()
        with(binding) {
            radioNominal.setOnClickListener {
                viewModel.setCurrentDiscountType(DiscountType.Nominal)
                nominalValidator(viewState.nominalValue)
            }
            radioPercentage.setOnClickListener {
                viewModel.setCurrentDiscountType(DiscountType.Percentage)
                percentageValidator(viewState.percentageValue)
            }
            radioNothing.setOnClickListener {
                viewModel.setCurrentDiscountType(DiscountType.Nothing)
                clearError()
            }
        }
    }

    private fun percentageValidator(percentageValue: Double){
        val validation = CommonStringValidator
            .blankValidator(percentageValue.toString(), "Persentase diskon tidak boleh kosong")
            .then(
                DoubleValidator
                    .maximumThenValidator(
                        percentageValue,
                        100.0,
                        "Diskon tidak boleh lebih dari 100%"
                    )
                    .then(
                        DoubleValidator
                            .minimumThenValidator(percentageValue, 0.0, "Diskon tidak boleh 0%")
                    )
            )

        when (validation) {
            is InputValidation.Success -> clearError()

            is InputValidation.Error -> setError(validation.t)

        }

    }

    private fun nominalValidator(nominalValue : Double){
        val product = activityViewModel.getCurrentViewStateOrNew().chosenProduct ?: return
        val validation = CommonStringValidator
            .blankValidator(nominalValue.toString(), "Persentase diskon tidak boleh kosong")
            .then(
                DoubleValidator
                    .maximumThenValidator(
                        nominalValue,
                        product.sellPrice,
                        "Diskon tidak boleh lebih dari harga jual yaitu ${product.sellPrice.toMoneyFormat(Constant.PREFIX_RP)}"
                    )
                    .then(
                        DoubleValidator
                            .minimumThenValidator(nominalValue, 0.0, "Diskon tidak boleh Rp. 0")
                    )
            )

        when (validation) {
            is InputValidation.Success -> clearError()

            is InputValidation.Error -> setError(validation.t)

        }
    }

    private fun setError(message: String) {
        with(binding){
            tvErrorText.goVisible()
            tvErrorText.text = message
            btnSave.isEnabled = false
            btnSave.setStrokeColorResource(R.color.disable_btn)
        }
    }

    private fun clearError() {
        with(binding){
            tvErrorText.goGone()
            tvErrorText.text = ""
            btnSave.isEnabled = true
            btnSave.setStrokeColorResource(R.color.primary)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    interface OnChangeDiscountListener {
        fun onSaveChangeDiscount(discountType: DiscountType, discountValue: Double = 0.0)
    }
}