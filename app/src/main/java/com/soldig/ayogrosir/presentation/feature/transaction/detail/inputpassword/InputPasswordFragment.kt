package com.soldig.ayogrosir.presentation.feature.transaction.detail.inputpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseDialogFragment
import com.soldig.ayogrosir.databinding.InputPasswordFragmentBinding
import com.soldig.ayogrosir.utils.ext.goGone
import com.soldig.ayogrosir.utils.ext.goVisible
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import com.soldig.ayogrosir.utils.other.SingleEvent
import com.soldig.ayogrosir.utils.validator.CommonStringValidator
import com.soldig.ayogrosir.utils.validator.InputValidation
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InputPasswordFragment : BaseDialogFragment() {
    private var _binding: InputPasswordFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var mListener: OnInputPasswordDone
    private val viewModel : InputPasswordViewModel by viewModels()

    fun setListener(listener: OnInputPasswordDone) {
        mListener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = InputPasswordFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeVM()
    }

    private fun observeVM() {
        viewModel.viewState.observe(viewLifecycleOwner) {
            passwordValidator(it.inputtedPassword)
            onPasswordValidationDone(it.isInputPasswordSuccess)
            binding.form.visibility = if(it.isValidatingPassword) View.INVISIBLE else View.VISIBLE
            binding.loading.toggleVisibility(it.isValidatingPassword)
        }
    }

    private fun onPasswordValidationDone(inputPasswordSuccess: SingleEvent<Boolean>?) {
        val isPasswordCorrect = inputPasswordSuccess?.get() ?: return
        if(isPasswordCorrect){
            dismiss()
            mListener.onInputPasswordDone()
        }else{
            setError("PIN yang anda masukan salah")
        }
    }

    override fun rootView(): View {
        return binding.root
    }

    private fun initView() {
        initClickListener()
        initEditTextListener()
    }

    private fun initEditTextListener() {
        binding.etPassword.addTextChangedListener {
            viewModel.setCurrentPassword(it.toString())
        }
    }

    private fun passwordValidator(password: String) {
        val validation = CommonStringValidator
            .blankValidator(password, "PIN tidak boleh kosong")
        when (validation) {
            is InputValidation.Success -> clearError()
            is InputValidation.Error -> setError(validation.t)

        }
    }

    private fun setError(message: String) {
        with(binding) {
            tvErrorText.goVisible()
            tvErrorText.text = message
            btnSave.isEnabled = false
            btnSave.setStrokeColorResource(R.color.disable_btn)
        }
    }

    private fun clearError() {
        with(binding) {
            tvErrorText.goGone()
            tvErrorText.text = ""
            btnSave.isEnabled = true
            btnSave.setStrokeColorResource(R.color.primary)
        }
    }

    private fun initClickListener() {
        with(binding) {
            btnCancel.setOnClickListener {
                dismiss()
            }
            btnSave.setOnClickListener {
                viewModel.validatePassword()
            }
            container.setOnClickListener {
                dismiss()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    interface OnInputPasswordDone {
        fun onInputPasswordDone()
    }

}