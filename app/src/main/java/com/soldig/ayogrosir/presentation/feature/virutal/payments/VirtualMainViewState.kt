package com.soldig.ayogrosir.presentation.feature.virutal.payments

import com.soldig.ayogrosir.model.*
import com.soldig.ayogrosir.utils.other.SingleEvent




data class VirtualMainViewState(
    var currentVirtual: VirtualType = VirtualType.Pulsa,
    var successTransaction : SingleEvent<VirtualProductResult>? = null,
    var pulsaSection: PulsaSection = PulsaSection(),
    var listrikSection: ListrikSection = ListrikSection(),
    var isFetching: Boolean = false,
    var errorMessage : SingleEvent<PostPaidErrorMessage>? = null,
    var successMessage : SingleEvent<PostPaidSuccessMessage>? = null,
    var selectedVirtualProduct: SelectedVirtualProduct? = null,
    var airSection: AirSection = AirSection(),
    var sectionChange : SingleEvent<Boolean>? = null
) {
}
data class PostPaidSuccessMessage(
    var friendlyMessage: String,
    var titleMessage: String
)

data class PostPaidErrorMessage(
    var actualMessage : String,
    var friendlyMessage: String,
    var titleMessage: String
)

data class PulsaSection(
    var selectedPascaBayarIndex: Int = -1,
    var currentPascaBayarPhoneNumber: String? = "",
    var currentSelectedPrabayarProvider : PulsaProvider? = null,
    var currentSelectedPascabayarPulsa : PulsaPascaBayar? = null,
    var currentPulsaType : VirtualPaymentType = VirtualPaymentType.PraBayar,
    var pulsaPraBayarList: HashMap<PulsaProvider,List<PulsaProduct>> = hashMapOf(),
    var pulsaPascabayarList: List<PulsaPascaBayar> = listOf()
)

data class ListrikSection(
    var listrikPascaBayarList : ListrikPascaBayar? = null,
    var currentListrikNumber : String? = "",
    var currentListrikType : VirtualPaymentType  = VirtualPaymentType.PraBayar,
    var selectedListrikIndex : Int = -1,
    var listrikProductList : List<ListrikProduct> = listOf()
)

data class AirSection(
    var pdamProductList : List<PDAMPascaBayar> = listOf(),
    var currentSelectedPDAMPascaBayar  : PDAMPascaBayar? = null,
    var currentNomorAir: String? = null
)


