package com.soldig.ayogrosir.presentation.feature.productdetail.changeprice

enum class ChangePriceMode {
    SELL,
    BUY
}