package com.soldig.ayogrosir.presentation.feature.virutal.paymentresult

import com.soldig.ayogrosir.utils.other.SingleEvent

class VirtualPaymentResultViewState(
    var isPrintingLoading: Boolean = false,
    var printedString: SingleEvent<String>? = null
)