package com.soldig.ayogrosir.presentation.feature.home

import android.view.View
import androidx.core.content.ContextCompat
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.OrderListItemBinding
import com.soldig.ayogrosir.model.Order
import com.soldig.ayogrosir.presentation.feature.home.HomeViewState.ProductSection.ProductListItem.POSProductListItem
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class OrderViewHolder(view: View) : RecyclerViewHolder<Order>(view){

    val binding = OrderListItemBinding.bind(view)

    fun bind(order: Order, onAdd : (product: POSProductListItem) -> Unit, onDelete : (product: POSProductListItem) -> Unit){
        with(order){

            val product = order.product.posProduct
            binding.tvName.text = product.title
            binding.tvQty.text = quantity.toString()
            binding.tvValue.text = currentTotalPrice.toMoneyFormat()
            binding.btnPlus.let {
                it.setOnClickListener {
                    if(product.qty > 0){
                        onAdd(order.product)
                    }
                }
                it.background = ContextCompat.getDrawable(
                    binding.root.context, if(order.product.posProduct.qty == 0)
                        R.drawable.bg_circle_overlay else R.drawable.bg_circle_primary)
            }
            binding.btnMinus.let {
                it.setOnClickListener {
                    onDelete(order.product)

                }
            }
        }
    }

}