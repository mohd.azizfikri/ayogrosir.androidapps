package com.soldig.ayogrosir.presentation.feature.virutal.payments

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.google.android.material.button.MaterialButton
import com.skydoves.powerspinner.PowerSpinnerView
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.common.startActivity
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.DialogInformationBinding
import com.soldig.ayogrosir.databinding.FragmentVirtualBinding
import com.soldig.ayogrosir.model.*
import com.soldig.ayogrosir.presentation.feature.MainActivity
import com.soldig.ayogrosir.presentation.feature.topup_virtual.GetSaldoViewModel
import com.soldig.ayogrosir.presentation.feature.topup_virtual.TopUpVirtualActivity
import com.soldig.ayogrosir.presentation.feature.virutal.history.VirtualProductHistoryActivity
import com.soldig.ayogrosir.presentation.feature.virutal.paymentresult.VirtualPaymentResultActivity
import com.soldig.ayogrosir.presentation.feature.virutal.payments.spinneradapters.*
import com.soldig.ayogrosir.utils.ext.*
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.AndroidEntryPoint
import java.text.DecimalFormat

@AndroidEntryPoint
class VirtualMainFragment  : Fragment(){

    private val pos: POS by lazy {
        val activity = requireActivity()
        if (activity is MainActivity) {
            activity.currentPOS
        } else {
            throw Exception("")
        }
    }

    private val viewModel : VirtualMainViewModel by viewModels()

    private var _binding: FragmentVirtualBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val selectionList = ArrayList<PowerSpinnerView>()

    private var saldo : Int = 0
    private val viewModelSaldo: GetSaldoViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentVirtualBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initVMListener()
    }

    override fun onStart() {
        super.onStart()
        viewModel.changeCurrentMode(viewModel.getCurrentViewStateOrNew().currentVirtual)
        viewModel.getVirtualProduct(pos.token)
        viewModelSaldo.getSaldo(pos.token)
    }

    private fun initVMListener() {
        viewModel.viewState.observe(viewLifecycleOwner){ it ->
            it?.successTransaction?.get()?.let {
                viewModel.reset()
                startActivity<VirtualPaymentResultActivity> {
                    putExtra(VirtualPaymentResultActivity.VIRTUAL_TRANSACTION_RESULT_BUNDLE_KEY, it)
                }
            }
            it?.sectionChange?.get()?.let {
                selectionList.forEach { spinner ->
                    spinner.dismiss()
                }
            }
            with(binding){
                setButtonColor(btnAir, it.currentVirtual == VirtualType.Air)
                setButtonColor(btnListrik, it.currentVirtual == VirtualType.Listrik)
                setButtonColor(btnPulsa, it.currentVirtual == VirtualType.Pulsa)
                virtualAirView.root.toggleVisibility(it.currentVirtual == VirtualType.Air)
                virtualListrikView.root.toggleVisibility(it.currentVirtual == VirtualType.Listrik)
                virtualPulsaView.root.toggleVisibility(it.currentVirtual == VirtualType.Pulsa)
                tvTotal.text = it.selectedVirtualProduct?.totalPrice?.toMoneyFormat() ?: "--"
                loading.root.toggleVisibility(it.isFetching)
                btnBayar.isEnabled = it.selectedVirtualProduct != null
                handleButtonBayar(it)
            }
            if(it.currentVirtual == VirtualType.Listrik) setListrikSection(it.listrikSection)
            if(it.currentVirtual == VirtualType.Pulsa) setPulsaSection(it.pulsaSection)
            if(it.currentVirtual == VirtualType.Air) setAirSection(it.airSection)
            it.successMessage?.get()?.let { successMessage ->
                requireContext().launchInformationDialog(
                    title = successMessage.titleMessage,
                    message = successMessage.friendlyMessage
                )
            }
            it.errorMessage?.get()?.let{ errorMessage ->
                requireContext().launchErrorDialog(
                    title = errorMessage.titleMessage,
                    message = errorMessage.friendlyMessage,
                    errorMessage = errorMessage.actualMessage
                )
            }

        }

        activity?.let {
            viewModelSaldo.viewState.observe(it, { viewState ->
                with(viewState) {
                    binding.loading.root.toggleVisibility(isLoading)
    //                println("datame" + viewState.responDataTopup)
    ////                val dataku = viewState.responDataTopup?.get() ?: return@observe
                    viewState.responGetSaldo?.get()?.let {
                        println("saldoku" + it)
                        if (it.message != null){
                            binding.tvSaldo.text = "Rp. " + currencyBanyak(it.data?.get(0)?.saldoWarung.toString())
                            saldo = it.data?.get(0)?.saldoWarung?.toInt() ?: 0
                        }
                    }
                }
            })
        }

        activity?.let {
            viewModelSaldo.userInteraction.observe(it, {
                val interaction = it.get() ?: return@observe
                if (interaction is UIInteraction.GenericMessage) {
                    binding.root.showShortSnackbar(interaction.getMessage(requireActivity()))
                }

            })
        }
    }

    private fun setAirSection(airSection: AirSection) {
        with(binding.virtualAirView){
            selectionLocation.getSpinnerAdapter<PDAMPascaBayar>().setItems(airSection.pdamProductList)
        }
    }

    private fun handleButtonBayar(viewState: VirtualMainViewState) {
        val selectedProdcut = viewState.selectedVirtualProduct
        with(binding.btnBayar){
            setOnClickListener {
                selectedProdcut?.let{ product ->
                    requireContext().launchConfirmationDialog(
                        title= "Konfirmasi pembayaran ${product.virtualType.name}",
                        message = "Anda yakin akan memebeli ${product.virtualType.name} ${product.virtualPaymentType.name} sebanyak ${product.totalPrice.toMoneyFormat()}",
                        onPositiveButton =  {
                            when(viewState.currentVirtual){
                                VirtualType.Pulsa -> {

                                    val dataProduct = product.totalPrice.toInt()
                                    if(dataProduct <= saldo){
                                        val customerNumber = binding.virtualPulsaView.etPhone.text.toString()
                                        if(viewState.pulsaSection.currentPulsaType == VirtualPaymentType.PraBayar){
                                            viewModel.doPrabayarPayment(pos.token, customerNumber, "Nomor telepon harus diisi untuk melakukan pembelian pulsa" , "Nomor telepon kosong atau invalid!")
                                        }else{
                                            viewModel.doPulsaPascaBayarPayment(pos.token, customerNumber, "Lakukan cek tagihan pulsa terlebih dahulu")
                                        }
                                    } else {
                                        launchInformationDialogNew(
                                            message = "Silahkan lakukan penambahan saldo virtual agar dapat melanjutkan transaksi",
                                            title = "Saldo Virtual Tidak Cukup",
                                            textButton = "Ke Halaman Topup Saldo"
                                        )
                                    }


                                }
                                VirtualType.Listrik -> {
                                    val dataProduct = product.totalPrice.toInt()
                                    if (dataProduct <= saldo) {

                                        if (viewState.listrikSection.currentListrikType == VirtualPaymentType.PascaBayar) {
                                            val customerNumber =
                                                binding.virtualListrikView.etListrikPascabayar.text.toString()
                                            viewModel.doPrabayarPayment(
                                                pos.token,
                                                customerNumber,
                                                "Nomor listrik harus diisi untuk melakukan pembelian token",
                                                "Nomor listrik kosong atau invalid!"
                                            )
                                        } else {
                                            val customerNumber =
                                                binding.virtualListrikView.etListrikPrabayar.text.toString()
                                            viewModel.doListrikPascabayarPayment(
                                                pos.token,
                                                customerNumber,
                                                "Lakukan cek tagihan listrik terlebih dahulu"
                                            )
                                        }
                                    } else {
                                        launchInformationDialogNew(
                                            message = "Silahkan lakukan penambahan saldo virtual agar dapat melanjutkan transaksi",
                                            title = "Saldo Virtual Tidak Cukup",
                                            textButton = "Ke Halaman Topup Saldo"
                                        )
                                    }
                                }

                                VirtualType.Air -> {
                                    val dataProduct = product.totalPrice.toInt()
                                    if (dataProduct <= saldo) {
                                        viewModel.doAirPascaBayar(pos.token, binding.virtualAirView.etAir.text.toString())
                                    } else {
                                        launchInformationDialogNew(
                                            message = "Silahkan lakukan penambahan saldo virtual agar dapat melanjutkan transaksi",
                                            title = "Saldo Virtual Tidak Cukup",
                                            textButton = "Ke Halaman Topup Saldo"
                                        )
                                    }
                                }
                            }
                            it.dismiss()
                        },
                        onNegativeButton = {
                            it.dismiss()
                        }
                    )
                }

            }
            setStrokeColorResource(if(viewState.selectedVirtualProduct != null) R.color.primary else R.color.disable_btn)
        }
    }




    private fun setPulsaSection(pulsaSection: PulsaSection) {
        with(binding.virtualPulsaView){
            setButtonColor(btnPascaBayar, pulsaSection.currentPulsaType == VirtualPaymentType.PascaBayar)
            setButtonColor(btnPraBayar, pulsaSection.currentPulsaType == VirtualPaymentType.PraBayar)
            btnCheck.toggleVisibility(pulsaSection.currentPulsaType == VirtualPaymentType.PascaBayar)
            llPascabayarSection.toggleVisibility(pulsaSection.currentPulsaType == VirtualPaymentType.PascaBayar)
            llPrabayarSection.toggleVisibility(pulsaSection.currentPulsaType == VirtualPaymentType.PraBayar)
            selectionProviderPascabayar.toggleVisibility(pulsaSection.currentPulsaType == VirtualPaymentType.PascaBayar)
            selectionProviderPrabayar.toggleVisibility(pulsaSection.currentPulsaType == VirtualPaymentType.PraBayar)
            with(btnCheck){
                setOnClickListener {
                    viewModel.checkPulsaPostPaid(pos.token, etPhone.text.toString())
                }
                isEnabled = pulsaSection.currentSelectedPascabayarPulsa != null
                setStrokeColorResource(if(pulsaSection.currentSelectedPascabayarPulsa != null) R.color.primary else R.color.disable_btn)
            }
            pulsaSection.currentSelectedPrabayarProvider?.let {
                pulsaSection.pulsaPraBayarList[it]?.let{ pulsaProducts->
                    selectionNominal.getSpinnerAdapter<PulsaProduct>().setItems(pulsaProducts)
                }
            }
            selectionProviderPascabayar.getSpinnerAdapter<PulsaPascaBayar>().setItems(pulsaSection.pulsaPascabayarList)


        }
    }

    private fun setListrikSection(listrikSection: ListrikSection) {
        with(binding.virtualListrikView){
            setButtonColor(btnPascaBayar, listrikSection.currentListrikType == VirtualPaymentType.PascaBayar)
            setButtonColor(btnPraBayar, listrikSection.currentListrikType == VirtualPaymentType.PraBayar)
            llPascabayar.toggleVisibility(listrikSection.currentListrikType == VirtualPaymentType.PascaBayar)
            llPrabayr.toggleVisibility(listrikSection.currentListrikType == VirtualPaymentType.PraBayar)
            btnCheck.toggleVisibility(listrikSection.currentListrikType == VirtualPaymentType.PascaBayar)
            selectionListrikToken.getSpinnerAdapter<ListrikProduct>().setItems(listrikSection.listrikProductList)
            Log.w("Cek http::",(listrikSection.listrikProductList.size).toString())
        }
    }

    private fun setButtonColor(materialButton: MaterialButton, isActive : Boolean) {
        with(materialButton){
            backgroundTintList =  ColorStateList.valueOf(ContextCompat.getColor(context, if(isActive) R.color.payment_on_click_bakcground else R.color.white))
            setTextColor(ColorStateList.valueOf(ContextCompat.getColor(context, if(isActive) R.color.payment_on_click_text else R.color.text)))
            setStrokeColorResource(if(isActive) R.color.payment_on_click_bakcground else R.color.black)
        }
    }

    private fun initView() {
        initParentViewListener()
        initListrikSection()
        initPulsaSection()
        initAirSection()
        addSpinnerList()
    }

    private fun addSpinnerList() {
        with(binding){
            selectionList.add(virtualAirView.selectionLocation)
            selectionList.add(virtualListrikView.selectionListrikToken)
            selectionList.add(virtualPulsaView.selectionProviderPascabayar)
            selectionList.add(virtualPulsaView.selectionNominal)
            selectionList.add(virtualPulsaView.selectionProviderPrabayar)
        }
    }

    private fun initAirSection() {
        with(binding.virtualAirView){
            selectionLocation.apply {
                lifecycleOwner = viewLifecycleOwner
                setSpinnerAdapter(PDAMAdapter(this))
                setOnSpinnerItemSelectedListener<PDAMPascaBayar>{ _, _, _, newItem ->
                    viewModel.changeCurrentPDAM(newItem)
                }
            }
            btnCheck.setOnClickListener {
                viewModel.checkAirPostpaid(pos.token, etAir.text.toString())
            }
        }
    }

    private fun initPulsaSection() {
        with(binding.virtualPulsaView){
            btnPascaBayar.setOnClickListener {
                viewModel.changeCurrentPulsaType(VirtualPaymentType.PascaBayar)
            }
            btnPraBayar.setOnClickListener{
                viewModel.changeCurrentPulsaType(VirtualPaymentType.PraBayar)
            }
            selectionProviderPrabayar.apply {
                setSpinnerAdapter(PulsaProviderAdapter(this))
                lifecycleOwner = viewLifecycleOwner
                setOnSpinnerItemSelectedListener<PulsaProvider>{ oldIndex, oldItem, newIndex, newItem ->
                    viewModel.changeCurrentSelectedPrabayarProvider(newItem)
                }
                getSpinnerAdapter<PulsaProvider>().setItems(PulsaProvider.values().toList())
            }

            selectionProviderPascabayar.apply {
                lifecycleOwner = viewLifecycleOwner
                setSpinnerAdapter(PulsaPascabayarAdapter(this))
                setOnSpinnerItemSelectedListener<PulsaPascaBayar>{ _, _, _, newItem ->
                    viewModel.changeCurrentSelectedPascaBayar(newItem)
                }
            }
            selectionNominal.apply {
                setSpinnerAdapter(PulsaSpinnerAdapter(this))
                lifecycleOwner = viewLifecycleOwner
                setOnSpinnerItemSelectedListener<PulsaProduct>{ _, _, _, newItem ->
                    viewModel.changeCurrentSelectedPrabayarPulsa(newItem);
                }
            }
        }
    }

    private fun initListrikSection() {
        with(binding.virtualListrikView){
            btnPascaBayar.setOnClickListener {
                viewModel.changeCurrentListrikMode(VirtualPaymentType.PascaBayar)
            }
            btnPraBayar.setOnClickListener {
                viewModel.changeCurrentListrikMode(VirtualPaymentType.PraBayar)
            }
            btnCheck.setOnClickListener {
                viewModel.checkListrikPostPaid(pos.token, etListrikPascabayar.text.toString())
            }
            selectionListrikToken.apply {
                setSpinnerAdapter(ListrikPrabayarAdapter(this))
                lifecycleOwner = viewLifecycleOwner
                setOnSpinnerItemSelectedListener<ListrikProduct>{ _, _, _, newItem ->
                    viewModel.changeCurrentSelectedPrabayarListrik(newItem)
                }
            }
        }
    }

    private fun initParentViewListener() {
        with(binding){
            btnAir.setOnClickListener {
                viewModel.changeCurrentMode(VirtualType.Air)
            }
            btnListrik.setOnClickListener {
                viewModel.changeCurrentMode(VirtualType.Listrik)
            }
            btnPulsa.setOnClickListener {
                viewModel.changeCurrentMode(VirtualType.Pulsa)
            }
            btnRiwayat.setOnClickListener {
                val intent = Intent(requireActivity(), VirtualProductHistoryActivity::class.java)
                requireActivity().startActivity(intent)
            }

            btnTopUp.setOnClickListener {
                val intent = Intent(requireActivity(), TopUpVirtualActivity::class.java)
                requireActivity().startActivity(intent)
            }
        }
    }


    fun currencyBanyak(s: String): String = DecimalFormat("###,###,###,###,###")
        .format(s.toDouble()).replace(",", ".")



    fun launchInformationDialogNew(
        message: String, title: String, textButton: String ): MaterialDialog {
        return MaterialDialog(requireActivity())
            .cornerRadius(12f)
            .show {
                val dialogBinding =  DialogInformationBinding.inflate(LayoutInflater.from(this.view.context),this.view,false)
                customView(view = dialogBinding.root)
                dialogBinding.tvTitle.text =title;
                dialogBinding.tvMessage.text  =message;
                dialogBinding.btnYes.text = textButton
                dialogBinding.btnYes.setOnClickListener{
                    val intent = Intent(requireActivity(), TopUpVirtualActivity::class.java)
                    requireActivity().startActivity(intent)
                    this.dismiss()
                }


            }

    }
}