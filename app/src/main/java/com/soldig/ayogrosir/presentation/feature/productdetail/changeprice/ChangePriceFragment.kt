package com.soldig.ayogrosir.presentation.feature.productdetail.changeprice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseDialogFragment
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.ChangePriceFragmentBinding
import com.soldig.ayogrosir.utils.ext.goGone
import com.soldig.ayogrosir.utils.ext.goVisible
import com.soldig.ayogrosir.utils.validator.CommonStringValidator
import com.soldig.ayogrosir.utils.validator.DoubleValidator
import com.soldig.ayogrosir.utils.validator.InputValidation
import com.soldig.ayogrosir.utils.validator.then
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ChangePriceFragment : BaseDialogFragment() {
    private var _binding: ChangePriceFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var mListener: OnChangePriceListener
    private var initialPrice: Double = 0.0
    private var initialMode: ChangePriceMode = ChangePriceMode.BUY
    private var minimumPrice: Double = 0.0


    private val viewModel: ChangePriceViewModel by viewModels()

    fun setListener(listener: OnChangePriceListener) {
        mListener = listener
    }

    fun setType( currentPrice: Double) {
        initialMode = ChangePriceMode.BUY
        initialPrice = currentPrice
    }


    fun setType( currentPrice: Double, minimumPrice: Double) {
        initialMode = ChangePriceMode.SELL
        initialPrice = currentPrice
        this.minimumPrice = minimumPrice
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = ChangePriceFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeVM()
        initCurrentPrice()
    }

    private fun observeVM() {
        viewModel.viewState.observe(viewLifecycleOwner) {
            priceValidator(it.currentPrice, it.changePriceMode)
            binding.tvTitle.text =
                if (it.changePriceMode == ChangePriceMode.SELL) "Harga Jual" else "Harga Beli"
        }
    }

    private fun initCurrentPrice() {
        val currentViewState = viewModel.getCurrentViewStateOrNew()
        binding.etPrice.setValue(
            if (currentViewState.isPriceChanged) currentViewState.currentPrice.toLong() else initialPrice.toLong()
        )
        binding.etPrice.setSelection(binding.etPrice.text.toString().length)
        viewModel.setCurrentMode(initialMode)
    }

    override fun rootView(): View {
        return binding.root
    }

    private fun initView() {
        binding.etPrice.locale = Locale("id", "ID")
        binding.etPrice.decimalDigits = 0
        initClickListener()
        initEditTextListener()
    }

    private fun initEditTextListener() {
        binding.etPrice.addTextChangedListener {
            viewModel.setCurrentPrice(binding.etPrice.rawValue.toDouble())
        }
    }

    private fun priceValidator(nominalValue: Double, mode: ChangePriceMode) {
        val priceTypeInString = if (mode == ChangePriceMode.SELL) "Harga Jual" else "Harga Beli"
        var validation = CommonStringValidator
            .blankValidator(nominalValue.toString(), "$priceTypeInString tidak boleh kosong")
            .then(
                DoubleValidator
                    .minimumThenValidator(nominalValue, 0.0, "$priceTypeInString tidak boleh Rp. 0")
            )
        if (mode == ChangePriceMode.SELL) {
            validation = validation.then(
                DoubleValidator
                    .minimumThenValidator(
                        nominalValue,
                        minimumPrice,
                        "$priceTypeInString tidak boleh lebih kecil dari harga beli ( ${minimumPrice.toMoneyFormat()} )"
                    )
            )
        }
        when (validation) {
            is InputValidation.Success -> clearError()
            is InputValidation.Error -> setError(validation.t)

        }
    }

    private fun setError(message: String) {
        with(binding) {
            tvErrorText.goVisible()
            tvErrorText.text = message
            btnSave.isEnabled = false
            btnSave.setStrokeColorResource(R.color.disable_btn)
        }
    }

    private fun clearError() {
        with(binding) {
            tvErrorText.goGone()
            tvErrorText.text = ""
            btnSave.isEnabled = true
            btnSave.setStrokeColorResource(R.color.primary)
        }
    }

    private fun initClickListener() {
        with(binding) {
            btnCancel.setOnClickListener {
                dismiss()
            }
            btnSave.setOnClickListener {
                dismiss()
                val viewState = viewModel.getCurrentViewStateOrNew()
                mListener.onSavePrice(viewState.changePriceMode, viewState.currentPrice)
            }
            container.setOnClickListener {
                dismiss()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    interface OnChangePriceListener {
        fun onSavePrice(changePriceMode: ChangePriceMode, newPriceValue: Double)
    }

}