package com.soldig.ayogrosir.presentation.feature.home.transactionstatus

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.activity.viewModels
import com.dantsu.escposprinter.EscPosPrinter
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.dantsu.escposprinter.textparser.PrinterTextParserImg
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.TransactionStatusActivityBinding
import com.soldig.ayogrosir.model.TransactionDetail
import com.soldig.ayogrosir.utils.ext.launchInformationDialog
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions


@AndroidEntryPoint
class TransactionStatusActivity : BaseLoggedInActivity() {
    private lateinit var binding: TransactionStatusActivityBinding

    companion object {
        const val TRANSACTION_RESULT_BUNDLE_KEY =
            "com.soldig.amanahmart.presentation.feature.home.transaction_result_key"
        private const val BLUETOOTH_AND_FINE_LOCATION = 50
    }

    private val viewModel : TransactionStatusViewModel by viewModels()

    private val transactionResult: TransactionDetail by lazy {
        intent.extras?.get(TRANSACTION_RESULT_BUNDLE_KEY) as TransactionDetail?
            ?: throw Exception("No Transaction Result Included")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TransactionStatusActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
    }

    private fun observeVM() {
        viewModel.viewState.observe(this){
            binding.loading.root.toggleVisibility(it.isPrintingLoading)
            val toBePrinted = it.printedString?.get() ?: return@observe
            doPrint(toBePrinted)
        }
    }

    private fun initView() {
        initToolbar("Orderan Sukses", binding.toolbar)
        populateTransactionResult(transactionResult)
    }

    @SuppressLint("SetTextI18n")
    private fun populateTransactionResult(transactionResult: TransactionDetail) {
        with(binding) {
            tvTotalChange.text =
                "${getString(R.string.transaction_status_total_change_prefix)} \n ${transactionResult.totalChange.toMoneyFormat()}"
            tvTotalPaid.text =
                "${getString(R.string.transaction_status_total_paid_prefix)} \n ${transactionResult.totalPaid.toMoneyFormat()}"
            tvTotalPrice.text =
                "${getString(R.string.transaction_status_total_price_prefix)} \n ${transactionResult.totalAfterDiscount.toMoneyFormat()}"
            btnPrint.setOnClickListener {
                onPrint()
            }
        }
    }

    @AfterPermissionGranted(BLUETOOTH_AND_FINE_LOCATION)
    private fun onPrint() {
        println("After pemission granted")
        if (EasyPermissions.hasPermissions(
                this,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            viewModel.requestPrint(currentPOS, transactionResult)
        } else {
            println("Tidak punya permission")
            EasyPermissions.requestPermissions(
                this,
                "Blueetoth dan Loakasi",
                BLUETOOTH_AND_FINE_LOCATION,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun doPrint(printedString : String) {
        try{
            println("Printing")
            val printer = EscPosPrinter(BluetoothPrintersConnections.selectFirstPaired(), 203, 48f, 32)
//            if(isBluetoothConnected()){
                printer
                    .printFormattedText("[C]<img>" +
                            PrinterTextParserImg.bitmapToHexadecimalString(printer, this.getApplicationContext().getResources().getDrawableForDensity(R.drawable.ic_logo_kawanmart, DisplayMetrics.DENSITY_MEDIUM))+"</img>\n" +
                            printedString)
//            }else{
//                launchInformationDialog(
//                    message = "Cek Koneksi dengan printer lalu coba lakukan print lagi",
//                    title = "Gagal melakukan print"
//                )
//            }

        }catch ( exception : Exception){
            launchInformationDialog(
                message = "Periksa Koneksi dengan printer lalu coba lakukan print lagi",
                title = "Gagal melakukan print"
            )
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        println("Permision result")
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }
}