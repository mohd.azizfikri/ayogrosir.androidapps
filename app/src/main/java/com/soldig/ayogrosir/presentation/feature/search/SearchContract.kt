package com.soldig.ayogrosir.presentation.feature.search

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract

class SearchContract : ActivityResultContract<Any, String?>() {

    companion object{
        const val  CONTRACT_RETURN_KEY = "com.soldig.amanahmart.presentation.feature.search.contact"
    }

    override fun createIntent(context: Context, input: Any?): Intent {
        return Intent(context, SearchActivity::class.java)
    }

    override fun parseResult(resultCode: Int, intent: Intent?): String? {
        if(resultCode != Activity.RESULT_OK){
            return null
        }
        return intent?.getStringExtra(CONTRACT_RETURN_KEY)

    }


}