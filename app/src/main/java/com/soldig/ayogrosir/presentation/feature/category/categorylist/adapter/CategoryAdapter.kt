package com.soldig.ayogrosir.presentation.feature.category.categorylist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.soldig.ayogrosir.databinding.CategoryListItemBinding
import com.soldig.ayogrosir.model.ProductCategory

class CategoryAdapter(var categoryList: List<ProductCategory>, private var onCategoryClicked: (productCategory: ProductCategory) -> Unit)
    : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    fun changeList(newList: List<ProductCategory>){
        categoryList = newList
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: CategoryListItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = CategoryListItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(categoryList[position]) {
                binding.tvTitle.text = this.name
                binding.root.setOnClickListener {
                    onCategoryClicked(this)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }
}

