package com.soldig.ayogrosir.presentation.feature.report

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.ReportShopActivityBinding
import com.soldig.ayogrosir.model.ProductReport
import com.soldig.ayogrosir.presentation.commonlist.reportlist.ReportViewHolder
import com.soldig.ayogrosir.presentation.feature.transaction.history.HistoryTransactionActivity
import com.soldig.ayogrosir.utils.DatePickerFragment
import com.soldig.ayogrosir.utils.TimePickerFragment
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf

@AndroidEntryPoint
class ReportShopActivity : BaseLoggedInActivity(),
    DatePickerFragment.OnDatePicker,
    TimePickerFragment.OnTimePicker {
    private lateinit var binding: ReportShopActivityBinding


    private val canceledAdapter = adapterOf<ProductReport> {
        diff(
            areItemsTheSame = { old: ProductReport, new: ProductReport -> false},
            areContentsTheSame = { _: ProductReport, _: ProductReport -> false }
        )
        register(
            viewHolder = ::ReportViewHolder,
            layoutResource = R.layout.report_product_item,
            onBindViewHolder = { vh, _, it ->
                vh.bind(it)
            }
        )
    }

    private val soldAdapter = adapterOf<ProductReport> {
        diff(
            areItemsTheSame = { old: ProductReport, new: ProductReport -> false},
            areContentsTheSame = { _: ProductReport, _: ProductReport -> false }
        )
        register(
            viewHolder = ::ReportViewHolder,
            layoutResource = R.layout.report_product_item,
            onBindViewHolder = { vh, _, it ->
                vh.bind(it)
            }
        )
    }


    private val viewModel : ReportShopViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ReportShopActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
        viewModel.fetch(currentPOS.token)
    }

    @SuppressLint("SetTextI18n")
    private fun observeVM() {
        viewModel.viewState.observe(this){ viewState ->

            with(binding){
                viewState.report.let{
                    soldAdapter.submitList(it.soldProduct)
                    canceledAdapter.submitList(it.canceledProduct)
                    tvTotalBeginingBalance.text = ": ${it.beginningBalanceTotal.toMoneyFormat()}"
                    tvTotalBill.text = ": ${it.billTotal}"
                    tvTotalSoldProduct.text = ": ${it.soldProductQty}"
                    tvTotalCanceledBill.text = ": ${it.canceledBill}"
                    tvTotalCancledAmount.text  = ": ${it.totalCanceledAmount.toMoneyFormat()}"
                    tvTotalDiscount.text = ": ${it.discountTotal.toMoneyFormat()}"
                    tvTotalAmount.text= ": ${it.totalAmountWithoutDiscount.toMoneyFormat()}"
                    tvTotalAmountAfterDiscount.text = ": ${it.totalAmountAfterDiscount.toMoneyFormat()}"
                    tvTotalCanceledProduct.text = ": ${it.canceledProductQty}"
                    tvTotalCashTransaction.text = ": ${it.totalAmountPaidWithCash.toMoneyFormat()}"
                    tvTotalNonCashTransaction.text = ": ${it.totalAmountPaidWithNotCash.toMoneyFormat()}"
                    tvTotalCashAmount.text = ": ${it.totalAllCash.toMoneyFormat()}"

                }
                loading.root.toggleVisibility(viewState.isFetching)
                tvFirstDate.text = viewState.getStartDateInString()
                tvLastDate.text = viewState.getEndDateInString()
                tvWalletAmount.text = viewState.walletAmount
                tvWalletName.text = viewState.walletName
            }
        }
    }

    private fun initView() {
        initToolbar(getString(R.string.drawer_item_report),binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initClickListener()
        initReportProductRV()
    }

    private fun initReportProductRV() {
        with(binding.rvSold){
            layoutManager = LinearLayoutManager(context)
            adapter = soldAdapter
        }
        with(binding.rvCanceled){
            layoutManager = LinearLayoutManager(context)
            adapter = canceledAdapter
        }
    }

    private fun initClickListener() {
        binding.tvFirstDate.setOnClickListener {
            val instance: DialogFragment = DatePickerFragment.newInstance(FIRST_DATE)
            instance.show(supportFragmentManager, "datePicker")
        }

        binding.tvLastDate.setOnClickListener {
            val newFragment: DialogFragment = DatePickerFragment.newInstance(
                LAST_DATE
            )
            newFragment.show(supportFragmentManager, "datePicker")
        }
        binding.btnFilter.setOnClickListener {
            viewModel.fetch(currentPOS.token)
        }
    }

    override fun onDateSelect(date: String, type: Int) {
        val instance: DialogFragment = TimePickerFragment.newInstance(date, type)
        instance.show(supportFragmentManager, "timePicker")
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSelect(date: String, time: String, type: Int) {
        val dateInString = "$date $time"
        if (type == HistoryTransactionActivity.FIRST_DATE) {
            viewModel.setStartDate(dateInString)
        } else {
            viewModel.setEndDate(dateInString)
        }
    }

    companion object {
        const val FIRST_DATE = 1
        const val LAST_DATE = 2
    }
}