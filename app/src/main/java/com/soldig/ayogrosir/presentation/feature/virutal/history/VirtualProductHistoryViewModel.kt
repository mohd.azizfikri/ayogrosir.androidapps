package com.soldig.ayogrosir.presentation.feature.virutal.history

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.virtual.VirtualInteractor
import com.soldig.ayogrosir.model.VirtualProductHistory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class VirtualProductHistoryViewModel
    @Inject constructor(
        private val virtualInteractor: VirtualInteractor
    ): BaseViewModel<VirtualProductHistoryViewState>(){

    override fun initNewViewState(): VirtualProductHistoryViewState {
        return VirtualProductHistoryViewState()
    }

    fun getHistoryList(token: String ) {
        viewModelScope.launch(IO){
            virtualInteractor.getHistory.fetch(token).collect { response ->
                onCollect(
                    response = response,
                    onLoading = {
                        toogleLoading(it)
                    },
                    executeOnSuccess = {
                        setList(it.data)
                    }
                )
            }
        }
    }

    private fun setList(list : List<VirtualProductHistory>){
        setViewState(getCurrentViewStateOrNew().apply {
            this.virtualProductHistoryList = list
        })
    }



    private fun toogleLoading(loading: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            isFetching = loading
        })
    }

    fun setStartDate(date:String){
        setViewState(getCurrentViewStateOrNew().apply {
            startDateFilter = changeDateFormat(date)
        })
    }

    fun setEndDate(date: String){
        setViewState(getCurrentViewStateOrNew().apply {
            endDateFilter = changeDateFormat(date)
        })
    }


    @SuppressLint("SimpleDateFormat")
    private fun changeDateFormat(date: String): Date? {
        val format = "dd/MM/yyyy HH:mm"
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.parse(date)
    }

}