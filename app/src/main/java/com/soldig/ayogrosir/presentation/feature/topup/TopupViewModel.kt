package com.soldig.ayogrosir.presentation.feature.topup

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.home.HomeInteractor
import com.soldig.ayogrosir.interactors.splash.SplashSyncInteractor
import com.soldig.ayogrosir.interactors.topup.SetBeginingBalance
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.utils.other.PasswordValidator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class TopupViewModel
@Inject constructor(
    private val setBeginingBalance: SetBeginingBalance,
    private val passwordValidator: PasswordValidator,
    private val homeInteractor: HomeInteractor,
    private val syncWithServer: SplashSyncInteractor
) : BaseViewModel<TopupViewState>(){


    override fun initNewViewState(): TopupViewState {
        return TopupViewState()
    }


    private fun setLoading(isLoading : Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            this.isFetching = isLoading
        })
    }

    private fun setIsSuccess(posPassword: String){
        viewModelScope.launch(IO){
            passwordValidator.addNewPassword(posPassword)
            setViewState(getCurrentViewStateOrNew().apply {
                this.isTopUpDone = true
                this.isFetching = true
            })
        }

    }


    fun setBalanceValue(balance: Double){
        setViewState(getCurrentViewStateOrNew().apply {
            this.topUpValue = balance
        })
    }

    fun processingTopUp(pos: POS, balance: Double, token: String, posPassword: String){
        setLoading(true)
        syncWithServer(pos, balance, token, posPassword)
    }

    fun postStartingBalance(balance: Double, token: String, posPassword: String){
        viewModelScope.launch {
            withContext(IO){
                setBeginingBalance.setBeginingBalance(balance,token).collect {
                    onCollect(it/*,
                        onLoading = { isLoading ->
                            if(isLoading){
                                setLoading(isLoading)
                            }
                        }*/
                    ){
                        setIsSuccess(posPassword)
                    }
                }
            }
        }
    }


    fun syncWithServer(pos: POS, balance: Double, token: String, posPassword: String) {
        setLoading(true)
        viewModelScope.launch {
            syncWithServer.fetch(pos).collect {
                when(it){
                    is Resource.Success -> {
                        fetchProductList(pos, balance, token, posPassword)
                    }
                    is Resource.Error -> {
                        setLoading(false)
                    }
                    else -> {}
                }
            }
        }
    }

    fun fetchProductList(pos: POS, balance: Double, token: String, posPassword: String) {
        viewModelScope.launch(Dispatchers.IO) {
            homeInteractor.getAllProduct.fetch(pos).collect {
                onCollect(it){
                        result -> postStartingBalance(balance, token, posPassword)
                }
                /*when(it){
                    is Resource.Success -> {
                        postStartingBalance(balance, token, posPassword)
                    }
                    is Resource.Error -> {
                        setLoading(false)
                    }
                    else -> {}
                }*/
            }
        }
    }


}