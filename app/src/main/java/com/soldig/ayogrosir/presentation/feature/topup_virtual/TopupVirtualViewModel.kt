package com.soldig.ayogrosir.presentation.feature.topup_virtual

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.topup_virtual.TopupVirtualInteractor
import com.soldig.ayogrosir.model.ResponseTopup
import com.soldig.ayogrosir.utils.other.SingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

@HiltViewModel
class TopupVirtualViewModel
@Inject constructor(
    private val topupVirtualInteractor: TopupVirtualInteractor,
) : BaseViewModel<TopupVirtualViewState>() {

    override fun initNewViewState(): TopupVirtualViewState {
        return TopupVirtualViewState()
    }

    private fun setLoading(isLoading: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.isLoading = isLoading
        })
    }

    private fun setDataTopup(pos: ResponseTopup) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.responDataTopup = SingleEvent(pos)
        })
    }

    fun topUp(
        token: String,
        param: HashMap<String, @JvmSuppressWildcards RequestBody>,
        partFile: MultipartBody.Part?
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                topupVirtualInteractor.topUp.topUp(token, param, partFile).collect {
                    onCollect(it,
                        onLoading = { isLoading ->
                            setLoading(isLoading)
                        }
                    ) { resource ->
                        setLoading(false)

                        setDataTopup(resource.data)
                    }
                }

            }
        }
    }
}