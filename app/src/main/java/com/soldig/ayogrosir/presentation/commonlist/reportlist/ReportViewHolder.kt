package com.soldig.ayogrosir.presentation.commonlist.reportlist

import android.annotation.SuppressLint
import android.view.View
import com.soldig.ayogrosir.databinding.ReportProductItemBinding
import com.soldig.ayogrosir.model.ProductReport
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class ReportViewHolder(view : View) : RecyclerViewHolder<ProductReport>(view) {

    private val binding = ReportProductItemBinding.bind(view);


    @SuppressLint("SetTextI18n")
    fun bind(productReport: ProductReport) {
        with(binding){
            tvName.text = productReport.name
            tvQty.text = ": ${productReport.qty}"
        }
    }

}