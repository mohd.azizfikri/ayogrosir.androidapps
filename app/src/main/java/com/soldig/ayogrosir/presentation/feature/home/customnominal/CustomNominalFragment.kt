package com.soldig.ayogrosir.presentation.feature.home.customnominal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseDialogFragment
import com.soldig.ayogrosir.databinding.CustomNominalFragmentBinding
import com.soldig.ayogrosir.model.PaymentMethod
import com.soldig.ayogrosir.utils.ext.goGone
import com.soldig.ayogrosir.utils.ext.goVisible
import com.soldig.ayogrosir.utils.validator.CommonStringValidator
import com.soldig.ayogrosir.utils.validator.DoubleValidator
import com.soldig.ayogrosir.utils.validator.InputValidation
import com.soldig.ayogrosir.utils.validator.then
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class CustomNominalFragment : BaseDialogFragment() {
    private var _binding: CustomNominalFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var mListener: OnCustomNominalListener
    private lateinit var choosenPaymentMethod: PaymentMethod
    private var minimumPrice: Double = 0.0


    private val viewModel : CustomNominalViewModel by viewModels()

    fun setListener(listener: OnCustomNominalListener) {
        mListener = listener
    }

    fun setMinimumPrice(minimumPrice: Double){
        this.minimumPrice = minimumPrice
    }

    fun setPaymentMethod(paymentMethod: PaymentMethod){
        this.choosenPaymentMethod = paymentMethod
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        _binding = CustomNominalFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeVM()
    }

    private fun observeVM() {
        viewModel.viewState.observe(viewLifecycleOwner){ viewState ->
            priceValidator(viewState.inputtedPrice, minimumPrice)
            binding.btnSubmit.setOnClickListener {
                mListener.onSubmit(viewState.inputtedPrice, minimumPrice, choosenPaymentMethod)
                dismiss()
            }
        }
    }

    override fun rootView(): View {
        return binding.root
    }

    private fun initView() {
        binding.etCustomNominal.locale = Locale("id", "ID")
        binding.etCustomNominal.decimalDigits = 0
        initClickListener()
        initEtNominalListener()
    }

    private fun initEtNominalListener() {
        binding.etCustomNominal.addTextChangedListener {
            viewModel.setCurrentPrice(binding.etCustomNominal.rawValue.toDouble())
        }
        binding.etCustomNominal.setValue(minimumPrice.toString().toLongOrNull() ?: 0L)
    }

    private fun initClickListener() {
        with(binding){
            btnCancel.setOnClickListener {
                dismiss()
            }
        }
    }

    private fun priceValidator(nominalValue: Double, minimumPrice: Double) {
        val validation = CommonStringValidator
            .blankValidator(nominalValue.toString(), "Jumlah nominal tidak boleh kosong")
            .then(
                DoubleValidator
                    .minimumThenValidator(nominalValue, minimumPrice, "Nominal harus sama atau lebih dari jumlah pembelian")

            )

        when (validation) {
            is InputValidation.Success -> clearError()

            is InputValidation.Error -> setError(validation.t)

        }
    }


    private fun setError(message: String) {
        with(binding){
            tvErrorText.goVisible()
            tvErrorText.text = message
            btnSubmit.isEnabled = false
            btnSubmit.setStrokeColorResource(R.color.disable_btn)
        }
    }

    private fun clearError() {
        with(binding){
            tvErrorText.goGone()
            tvErrorText.text = ""
            btnSubmit.isEnabled = true
            btnSubmit.setStrokeColorResource(R.color.primary)
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    interface OnCustomNominalListener {
        fun onSubmit(customPaymentPrice : Double,minimumPrice: Double, paymentMethod: PaymentMethod)
    }
}