package com.soldig.ayogrosir.presentation.feature.transaction.history

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.DialogFragment
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.HistoryTransactionActivityBinding
import com.soldig.ayogrosir.model.TransactionHistory
import com.soldig.ayogrosir.presentation.feature.transaction.detail.DetailTransactionActivity
import com.soldig.ayogrosir.utils.DatePickerFragment
import com.soldig.ayogrosir.utils.TimePickerFragment
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf

@AndroidEntryPoint
class HistoryTransactionActivity : BaseLoggedInActivity(),
    DatePickerFragment.OnDatePicker,
    TimePickerFragment.OnTimePicker {
    private lateinit var binding: HistoryTransactionActivityBinding

    private val viewModel : HistoryTransactionViewModel by viewModels()

    private val transactionAdapter = adapterOf<TransactionHistory> {
        register(
            viewHolder = ::HistoryTransactionViewHolder,
            layoutResource = R.layout.history_transaction_list_item,
            onBindViewHolder = { vh, _, transactionHistory ->
                vh.bind(transactionHistory){
                    navigateToDetailActivity(it.id)
                }
            }
        )
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = HistoryTransactionActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
        viewModel.fetch(currentPOS)
    }

    private fun observeVM() {
        viewModel.viewState.observe(this){
            transactionAdapter.submitList(it.historyList)
            with(binding){
                rvListCategory.toggleVisibility(!it.isFetching && it.historyList.isNotEmpty())
                tvEmptyList.toggleVisibility(!it.isFetching && it.historyList.isEmpty())
                progresbar.toggleVisibility(it.isFetching)
                btnFiler.isEnabled = !it.isFetching
                btnFiler.setStrokeColorResource(if(it.isFetching) R.color.disable_btn else R.color.primary)
                tvFirstDate.text = it.getStartDateInString() ?: "Tanggal Awal"
                tvLastDate.text = it.getEndDateInString()  ?: "Tanggal Akhir"
            }
        }
    }
    private fun initView() {
        initToolbar("Riwayat Transaksi",binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initClickListener()
        initRv()
    }

    private fun initRv() {
        binding.rvListCategory.apply {
            adapter = transactionAdapter
        }
    }

    private fun initClickListener() {
        binding.tvFirstDate.setOnClickListener {
            val instance: DialogFragment = DatePickerFragment.newInstance(FIRST_DATE)
            instance.show(supportFragmentManager, "datePicker")
        }

        binding.tvLastDate.setOnClickListener {
            val newFragment: DialogFragment = DatePickerFragment.newInstance(LAST_DATE)
            newFragment.show(supportFragmentManager, "datePicker")
        }

        binding.btnFiler.setOnClickListener {
            viewModel.fetch(currentPOS)
        }
    }
    override fun onDateSelect(date: String, type: Int) {
        val instance: DialogFragment = TimePickerFragment.newInstance(date, type)
        instance.show(supportFragmentManager, "timePicker")
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSelect(date: String, time: String, type: Int) {
        val dateInString = "$date $time"
        if (type == FIRST_DATE) {
            viewModel.setStartDate(dateInString)
        } else {
            viewModel.setEndDate(dateInString)
        }
    }

    private fun navigateToDetailActivity(productId: String){
        val intent = Intent(this, DetailTransactionActivity::class.java).apply {
            putExtra(
                DetailTransactionActivity.TRANSACTION_ID_KEY,
                productId
            )
        }
        startActivity(intent)
    }

    companion object {
        const val FIRST_DATE = 1
        const val LAST_DATE = 2
    }
}