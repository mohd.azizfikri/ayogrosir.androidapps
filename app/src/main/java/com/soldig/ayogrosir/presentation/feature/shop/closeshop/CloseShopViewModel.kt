package com.soldig.ayogrosir.presentation.feature.shop.closeshop

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.interactors.closeshop.CloseShopInteractor
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.Report
import com.soldig.ayogrosir.utils.other.PasswordValidator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CloseShopViewModel
@Inject constructor(
    private val closeShopInteractor: CloseShopInteractor,
    private val passwordValidator: PasswordValidator
) : BaseViewModel<CloseShopViewState>() {
    override fun initNewViewState(): CloseShopViewState {
        return CloseShopViewState()
    }
    fun fetchReport(pos: POS, startDate: Date) {
        val viewState = getCurrentViewStateOrNew()
        viewModelScope.launch(Dispatchers.IO) {
            closeShopInteractor.getReport.fetch(pos.token, startDate, viewState.endDate)
                .collect {
                    onCollect(
                        response = it,
                        onLoading = {
                            setViewState(getCurrentViewStateOrNew().apply {
                                isFetching = it
                            })
                        },
                        executeOnSuccess = { successData ->
                            setNewReport(pos, successData.data)
                        }
                    )
                }
        }
    }
    fun closeShop(token: String){
        viewModelScope.launch {
            closeShopInteractor.closeShop.fetch(token).collect {
                onCollect(
                    response= it,
                    onLoading = {
                        setViewState(getCurrentViewStateOrNew().apply {
                            isFetching = it
                        })
                    },
                    executeOnSuccess = {
                        setViewState(getCurrentViewStateOrNew().apply {
                            logout()
                        })
                    }
                )
            }
        }
    }
    private fun setNewReport(pos: POS, report: Report) {
        viewModelScope.launch(Dispatchers.Default) {
            val printedString = getPrintString(pos, report)
            setViewState(getCurrentViewStateOrNew().apply {
                this.report = report
                this.printedString = printedString
            })
        }
    }
    private fun getPrintString(currentPOS: POS, report: Report): String {

        val header =
            "[L]\n" +
                    "[C]${currentPOS.name}\n" +
                    "[C]${currentPOS.alamat}\n\n" +
                    "[L]TGL ${getTodayDateOnly()}\n" +
                    "[C]================================\n"
        val content = "[L]\n" +
                "[L]Saldo Buka Toko [R]${report.beginningBalanceTotal.toMoneyFormat()}\n\n" +
                "[L]Total Struk [R]${report.billTotal}\n\n" +
                "[L]Produk Terjual [R]${report.soldProductQty}\n\n" +
                "[L]Struk Dibatalkan [R]${report.canceledBill}\n\n" +
                "[L]Tot. Penj. Dibatalkan [R]${report.totalCanceledAmount.toMoneyFormat()}\n\n" +
                "[L]Produk Dibatalkan [R]${report.canceledProductQty}\n\n" +
                "[L]Tot. Diskon [R]${report.discountTotal.toMoneyFormat()}\n\n" +
                "[L]Tot. Penj. [R]${report.totalAmountWithoutDiscount.toMoneyFormat()}\n\n" +
                "[L]Penj. Set. Diskon [R]${report.totalAmountAfterDiscount.toMoneyFormat()}\n\n"
        return header + content + "\n\n\n\n"
    }
    @SuppressLint("SimpleDateFormat")
    private fun getTodayDateOnly(): String {
        val format = "yyyy-MM-dd"
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.format(Date())
    }

    fun logout() {
        viewModelScope.launch(IO) {
            passwordValidator.deleteCurrentPassword()
            setViewState(getCurrentViewStateOrNew().apply {
                isFetching = false
                isClosed = true
            })
        }
    }
}