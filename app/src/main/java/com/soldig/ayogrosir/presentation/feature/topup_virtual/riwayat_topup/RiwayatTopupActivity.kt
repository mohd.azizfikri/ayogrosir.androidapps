package com.soldig.ayogrosir.presentation.feature.topup_virtual.riwayat_topup

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.activity.viewModels
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.ActivityRiwayatTopupBinding
import com.soldig.ayogrosir.model.DataItemRiawayat
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf
import android.view.View
import android.widget.AdapterView

import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.TimePicker
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


@AndroidEntryPoint
class RiwayatTopupActivity : BaseLoggedInActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var binding: ActivityRiwayatTopupBinding
    private val viewModel : RiwayatTopupViewModel by viewModels()
    private val mAdapter = RiwayatAdapterTopupNew()
    var list_of_items = arrayOf("Pending", "Berhasil", "Ditolak")
    var itemSelect = ""
    var datePilih: String? = null
    var datePilih2: String? = null

    var cal = Calendar.getInstance()


    var riwayatTopupAdapter = adapterOf<DataItemRiawayat> {
        register(
            viewHolder = :: RiwayatTopupViewHolder,
            layoutResource = R.layout.item_riwayat_topup,
            onBindViewHolder = { vh, _, transactionHistory ->
                vh.bind(transactionHistory)
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRiwayatTopupBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
        viewModel.getRiwayatTopup(currentPOS.token,"","","")
    }

//    override fun onStart() {
//        super.onStart()
//        viewModel.getRiwayatTopup(currentPOS.token, "", "", "")
//    }

    private fun observeVM() {
        viewModel.viewState.observe(this){
            it.responseRiwayatTopup?.data?.toMutableList()?.let { it1 -> mAdapter.setData(it1) }
            with(binding){
                rvRiwayatHistory.toggleVisibility(!it.isFetching && it.responseRiwayatTopup?.data?.isNotEmpty()==true)
                tvEmptyList.toggleVisibility(!it.isFetching && it.responseRiwayatTopup?.data?.isEmpty() == true)
                progresbar.toggleVisibility(it.isFetching)
                btnFiler.isEnabled = !it.isFetching
                btnFiler.setStrokeColorResource(if(it.isFetching) R.color.disable_btn else R.color.primary)
                tvFirstDate.text = it.getStartDateInString() ?: "Tanggal Awal"
                tvLastDate.text = it.getEndDateInString()  ?: "Tanggal Akhir"

            }
        }
    }

    private fun initView() {
        initToolbar("Riwayat Topup Saldo", binding.toolbar)
        initTime(toolbar = binding.toolbar)
//        binding.rvRiwayatHistory.apply {
//            adapter = riwayatTopupAdapter
//        }
        binding.rvRiwayatHistory.adapter = mAdapter
        initRV()
        initClickListener()

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.daftar_list,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerFilter.adapter = adapter
        binding.spinnerFilter.onItemSelectedListener = this
        binding.spinnerFilter.prompt = "Pilih Status";
    }
    private fun updateDateInView() {
        val myFormat = "yyyy-MM-dd HH:mm:ss" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        binding.tvFirstDate!!.text = sdf.format(cal.time)
        convertISOTimeToDate(sdf.format(cal.time))
    }
    private fun updateDateInView2() {
        val myFormat = "yyyy-MM-dd HH:mm:ss" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        binding.tvLastDate!!.text = sdf.format(cal.time)
        convertISOTimeToDate2(sdf.format(cal.time))
    }
    fun convertISOTimeToDate(isoTime: String): String? {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        var convertedDate: Date? = null
        var formattedDate: String? = null
        try {
            convertedDate = sdf.parse(isoTime)
            formattedDate = SimpleDateFormat("yyyy-MM-dd").format(convertedDate)
            datePilih = formattedDate
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return formattedDate
    }
    fun convertISOTimeToDate2(isoTime: String): String? {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        var convertedDate: Date? = null
        var formattedDate: String? = null
        try {
            convertedDate = sdf.parse(isoTime)
            formattedDate = SimpleDateFormat("yyyy-MM-dd").format(convertedDate)
            datePilih2 = formattedDate
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return formattedDate
    }
    private fun initRV() {
//        binding.rvRiwayatHistory.apply {
//            adapter = riwayatTopupAdapter
//        }
        binding.rvRiwayatHistory.adapter = mAdapter
    }
//    override fun onDateSelect(date: String, type: Int) {
//        val instance: DialogFragment = TimePickerFragment.newInstance(date, type)
//        instance.show(supportFragmentManager, "timePicker")
////        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
////        try {
////            val date: Date = format.parse(date)
////            System.out.println(date)
////
////
////        } catch (e: ParseException) {
////            e.printStackTrace()
////        }
//
//    }
//
//    @SuppressLint("SetTextI18n")
//    override fun onTimeSelect(date: String, time: String, type: Int) {
//        val dateInString = "$date $time"
//        if (type == HistoryTransactionActivity.FIRST_DATE) {
//            viewModel.setStartDate(dateInString)
//        } else {
//            viewModel.setEndDate(dateInString)
//        }
//    }
    private fun initClickListener() {
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        }
        val dateSetListener2 = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView2()
            }
        }
        binding.tvFirstDate!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(this@RiwayatTopupActivity,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
            }
        })
        binding.tvLastDate!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(this@RiwayatTopupActivity,
                    dateSetListener2,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
            }
        })
        binding.btnFiler.setOnClickListener {
//            mAdapter.filter.filter(itemSelect)
            println("data pilih " + datePilih.toString() + "date pilih " + datePilih2.toString())
            viewModel.getRiwayatTopup(currentPOS.token, datePilih.toString(), datePilih2.toString(), itemSelect)
        }
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        itemSelect = id.toString()
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }
}