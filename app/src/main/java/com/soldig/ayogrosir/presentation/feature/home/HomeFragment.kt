package com.soldig.ayogrosir.presentation.feature.home

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.common.dp
import com.soldig.ayogrosir.common.startActivity
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.HomeFragmentBinding
import com.soldig.ayogrosir.model.*
import com.soldig.ayogrosir.presentation.commonlist.productlist.*
import com.soldig.ayogrosir.presentation.feature.MainActivity
import com.soldig.ayogrosir.presentation.feature.category.categorylist.CategoryContract
import com.soldig.ayogrosir.presentation.feature.home.*
import com.soldig.ayogrosir.presentation.feature.home.HomeViewState.ProductSection.ProductListItem
import com.soldig.ayogrosir.presentation.feature.home.HomeViewState.ProductSection.ProductViewType.*
import com.soldig.ayogrosir.presentation.feature.home.customnominal.CustomNominalFragment
import com.soldig.ayogrosir.presentation.feature.home.payment.PaymentTypeFragment
import com.soldig.ayogrosir.presentation.feature.home.transactionstatus.TransactionStatusActivity
import com.soldig.ayogrosir.presentation.feature.productdetail.ProductDetailActivity
import com.soldig.ayogrosir.presentation.feature.search.SearchContract
import com.soldig.ayogrosir.utils.SpacesItemDecoration
import com.soldig.ayogrosir.utils.ext.*
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf


@AndroidEntryPoint
class HomeFragment : Fragment(),
    PaymentTypeFragment.OnPaymentTypeListener,
    CustomNominalFragment.OnCustomNominalListener{
    private val gridLayoutManager by lazy { GridLayoutManager(requireContext(), 4) }
    private var _binding: HomeFragmentBinding? = null
    private val viewModel: HomeViewModel by viewModels()
    private val startCategoryList = registerForActivityResult(CategoryContract()) {
        it ?: return@registerForActivityResult
        viewModel.filterProductByCategory(it)
    }
    private lateinit var  productListLayoutManager : LinearLayoutManager
    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if(productListLayoutManager.findLastCompletelyVisibleItemPosition() > 10 && viewModel.getCurrentViewStateOrNew().productSection.currentProductViewType == Kumpulan){
                binding.btnScrollToTop.goVisible()
            }else{
                binding.btnScrollToTop.goGone()
            }
        }
    }
    private val searchActivityDetail = registerForActivityResult(SearchContract()) {
        it ?: return@registerForActivityResult
        if(viewModel.getCurrentViewStateOrNew().productSection.currentProductViewType == Kumpulan){
            viewModel.filterProductKumpulanByName(it)
        }else{
            viewModel.filterProductGridByName(it)
        }
    }
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val pos: POS by lazy {
        val activity = requireActivity()
        if (activity is MainActivity) {
            activity.currentPOS
        } else {
            throw Exception("")
        }
    }
    private val gridAdapter = adapterOf<ProductListViewType> {
        diff(
            areItemsTheSame = { old: ProductListViewType, new: ProductListViewType ->
                old.areItemSame(
                    new
                )
            },
            areContentsTheSame = { old: ProductListViewType, new: ProductListViewType ->
                false
            }
        )
        register(
            viewHolder = ::ProductGridViewHolder,
            layoutResource = R.layout.product_grid_item,
            onBindViewHolder = { vh, _, it ->
                vh.bind(it.product) {
                    println("Trying to go to product $it")
                    viewModel.addProductToCart(it, isFromGrid = true)
                }
            }
        )
        register(
            viewHolder = ::ProductGridEmptyViewHolder,
            layoutResource = R.layout.product_grid_item_empty,
            onBindViewHolder = { _, _, _ -> /* Do Nothing */}
        )
    }
    private val listAdapter = adapterOf<ProductListHorizontalItemType> {
        diff(
            areItemsTheSame = { old: ProductListHorizontalItemType, new: ProductListHorizontalItemType -> old.productListItem.product.productId == new.productListItem.product.productId },
            areContentsTheSame = { _: ProductListHorizontalItemType, _: ProductListHorizontalItemType -> false }
        )
        register(
            viewHolder = ::ProductListViewHolder,
            layoutResource = R.layout.product_list_item,
            onBindViewHolder = { vh, _, it ->
                vh.bind(it.productListItem,
                    onCartClicked =  { product ->
                        viewModel.addProductToCart(product, false)
                    }
                ) {
                    goToProductDetailActivity(it)
                }
            }
        )
    }
    private val orderAdapter = adapterOf<Order> {
        diff(
            areContentsTheSame = { old: Order, new: Order ->
                false
            },
            areItemsTheSame = { old: Order, new: Order ->
                old.product.posProduct.id == new.product.posProduct.id
            }
        )
        register(
            viewHolder = ::OrderViewHolder,
            layoutResource = R.layout.order_list_item,
            onBindViewHolder = { vh, _, it ->
                vh.bind(
                    it,
                    { viewModel.addProductToCart(it, false) },
                    { viewModel.deleteProductFromCart(productListItem = it, amount = 1, isFromGrid = false) })
            }
        )
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeVM()
    }
    override fun onStart() {
        super.onStart()
        viewModel.fetchProductList(pos)
    }
    private fun initView() {
        initLoading()
        initRv()
        initClickListener()
    }
    private fun initLoading() {
        binding.loading.root.setBackgroundColor(Color.TRANSPARENT)
    }
    private fun initClickListener() {
        binding.btnScrollToTop.setOnClickListener {
            binding.rvListProduct.scrollToPosition(0)
        }
        with(binding) {
            tabLayout.addTab(binding.tabLayout.newTab().setText("Pilihan"))
            tabLayout.addTab(binding.tabLayout.newTab().setText("Kumpulan"))
//            etBarcode.setOnKeyListener(object : View.OnKeyListener{
//                override fun onKey(view: View?, keyKode: Int, event : KeyEvent?): Boolean {
//                    etBarcode.onKeyDown(keyKode, event)
//                    if (event?.action ==KeyEvent.ACTION_DOWN && keyKode == KeyEvent.KEYCODE_ENTER) {
//                        //do something here
//                        Toast.makeText(requireContext(), binding.etBarcode.text.toString(), Toast.LENGTH_SHORT).show()
//                        return true;
//                    }
//                    return false
//                }
//
//            })
//
            etBarcode.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    Toast.makeText(requireContext(), binding.etBarcode.text.toString() , Toast.LENGTH_SHORT).show()
                    etBarcode.text.clear()
                }
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            })
            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    when (tabLayout.selectedTabPosition) {
                        0 -> {
                            viewModel.setCurrentProductViewType(Pilihan)
                        }
                        1 -> {
                            viewModel.setCurrentProductViewType(Kumpulan)
                        }
                    }
                }
                override fun onTabUnselected(tab: TabLayout.Tab) {}
                override fun onTabReselected(tab: TabLayout.Tab) {}
            })
//            chipSearchKey.setOnCloseClickListener { view ->
//                //Your action here...
//            }
            chipSearchKey.setOnCloseIconClickListener{
                viewModel.filterProductByCategory(null)
            }
            chipSearchKey.setOnCloseIconClickListener {
                if(viewModel.getCurrentViewStateOrNew().productSection.currentProductViewType == Kumpulan){
                    viewModel.filterProductKumpulanByName(null)
                }else{
                    viewModel.filterProductGridByName(null)
                }
            }
            chipCategoryKey.setOnCloseIconClickListener {
                viewModel.filterProductByCategory(null)
            }
            //print("Listener dari chip ${chipSearchKey.onCloseClickListener}")
            btnCategory.setOnClickListener {
                startCategoryList.launch(null)              ;
            }
            tvSearch.setOnClickListener {
                searchActivityDetail.launch(Any())
            }
           /* swipeLayout.setOnRefreshListener {
                if (!viewModel.getCurrentViewStateOrNew().isCartEmpty()) {
                    confirmationOnCartExist("Detail pesanan akan direset bila anda merefresh daftar product, Apakah anda yakin ?",
                        onDismiss = {
                            swipeLayout.isRefreshing = false
                        }
                        ,onNotConfirmed = {
                            swipeLayout.isRefreshing = false
                        }
                    ) {
                        swipeLayout.isRefreshing = false
                        viewModel.clearCart()
                        viewModel.fetchProductList(pos)

                    }
                } else {
                    swipeLayout.isRefreshing = false
                    viewModel.fetchProductList(pos)
                }
            }*/
        }
    }
    private fun initRv() {
        initProductRv()
        initCartRv()
    }
    private fun initCartRv() {
        binding.rvListOrder.apply {
            adapter = orderAdapter
            itemAnimator?.changeDuration = 0
        }
    }
    private fun initProductRv() {
        with(binding) {
            productListLayoutManager =  LinearLayoutManager(requireContext())
            rvListProduct.apply {
                layoutManager = productListLayoutManager
                adapter = listAdapter
                addItemDecoration(SpacesItemDecoration(24.dp))
                addOnScrollListener(onScrollListener)
            }
            rvGridProduct.apply {
                layoutManager = gridLayoutManager
                adapter = gridAdapter
            }
        }
    }
    private fun observeVM() {
        viewModel.viewState.observe(viewLifecycleOwner) { homeViewState ->
            with(homeViewState.productSection) {
                toogleListLoading(isFetching)
                shouldGoToDetail?.get()?.let {
                    print("Harus ke detail untuk produk ${it.title}");
                    val intent = Intent(context, ProductDetailActivity::class.java).apply {
                        putExtra(
                            ProductDetailActivity.PRODUCT_EXTRAS_KEY,
                            ParcelableProduct.buildFromProduct(it)
                        )
                    }
                    startActivity(intent)
                }
                binding.btnScrollToTop.toggleVisibility(currentProductViewType == Kumpulan && productListLayoutManager.findLastCompletelyVisibleItemPosition() > 10)
                binding.rvGridProduct.toggleVisibility(currentProductViewType == Pilihan)
                binding.rvListProduct.toggleVisibility(currentProductViewType == Kumpulan)
                var isListUpdate = false;
                isNewList?.get()?.let {
                    gridAdapter.submitList(ArrayList(getGridFilteredProduct()))
                    listAdapter.submitList(ArrayList(getKumpulanFilteredProduct()))
                    isListUpdate = true;
                }
                if (!isListUpdate) {
                    isKumpuluanFilteredRecently?.get()?.let {
                        listAdapter.submitList(ArrayList(getKumpulanFilteredProduct()))
                    }
                }
                if (!isListUpdate){
                    isGridFilteredRecently?.get()?.let {
                        gridAdapter.submitList(ArrayList(getGridFilteredProduct()))
                    }
                }
                homeViewState.productSection.currentKumpulanFilterCategory?.let{
                    binding.chipCategoryKey.setText(it.name)
                }
                if(currentProductViewType == Kumpulan){
                    homeViewState.productSection.currentKumpulanFilterName?.let {
                        binding.chipSearchKey.setText(it)
                    }
                }else{
                    homeViewState.productSection.currentGridFilterName?.let {
                        binding.chipSearchKey.setText(it)
                    }
                }
                binding.chipCategoryKey.toggleVisibility(homeViewState.productSection.currentKumpulanFilterCategory != null && homeViewState.productSection.currentProductViewType == Kumpulan)

                val currentMode = homeViewState.productSection.currentProductViewType
                binding.chipSearchKey.toggleVisibility(
                    (homeViewState.productSection.currentKumpulanFilterName != null && currentMode == Kumpulan ) ||
                            (homeViewState.productSection.currentGridFilterName != null && currentMode == Pilihan )
                )
                currentlyUpdatedGridIndex?.get()?.let {
                    gridAdapter.notifyItemChanged(it)
                }
                currentlyUpdatedListIndex?.get()?.let {
                    listAdapter.notifyItemChanged(it)
                }
                binding.btnCategory.toggleVisibility(homeViewState.productSection.currentProductViewType == Kumpulan)
                /*dataSource?.get()?.let {
                    if (it == DataSource.Cache) {
                        binding.root.showLongSnackbar("Data produk adalah data lokal. Periksa koneksi anda dan refresh untuk mendapatkan data terbaru")
                    }
                }*/
            }
            with(homeViewState.cartSection) {
                with(binding) {

                    tvTotalQty.text = cart.totalItems.toString()
                    tvTotalDiscount.text = cart.totalDiscount.toMoneyFormat()
                    tvTotalPrice.text = cart.totalAfterDiscount.toMoneyFormat()
                    tvSubTotal.text = cart.totalBeforeDiscount.toMoneyFormat()
                    togglePaymentButton(cart.orderList.isEmpty())
                    cart.orderList.values.forEach {
                        println("Judul ${it.product.posProduct.title}  qty order : ${it.quantity}  , qty product : ${it.product.posProduct.qty}")
                    }
                    orderAdapter.submitList(cart.orderList.values.toMutableList())
                    btnPayment.setOnClickListener {
                        val paymentTypeFragment = PaymentTypeFragment()
                        paymentTypeFragment.setListener(this@HomeFragment)
                        paymentTypeFragment.setTotalTransactionPrice(cart.totalAfterDiscount)
                        paymentTypeFragment.show(
                            requireActivity().supportFragmentManager,
                            "paymentTypeFragment"
                        )
                    }
                }
            }
            with(homeViewState.checkoutSection) {
                val activity = requireActivity()
                if (activity is MainActivity) {
                    activity.toggleLoading(isFetching)
                }
                val checkoutResult = transactionResult?.get() ?: return@observe
                navigateToTransactionStatusActivity(checkoutResult)
            }
        }
        viewModel.userInteraction.observe(viewLifecycleOwner) {
            val uiInteraction = it.get() ?: return@observe
            if (uiInteraction !is UIInteraction.DoNothing) {
                binding.root.showLongSnackbar(uiInteraction.getMessage(requireContext()))
            }
        }
    }
    private fun goToProductDetailActivity(posProductListItem: ProductListItem) {
        val goToProductDetail: () -> Unit = {
            val intent = Intent(context, ProductDetailActivity::class.java).apply {
                putExtra(
                    ProductDetailActivity.PRODUCT_EXTRAS_KEY,
                    ParcelableProduct.buildFromProduct(posProductListItem.product)
                )
            }
            startActivity(intent)
        }
        if (viewModel.getCurrentViewStateOrNew().isProductExistInCart(posProductListItem.product.productId)) {
            confirmationOnCartExist("Produk akan dihapus dari keranjang bila anda mengakses detail produk , Apakah anda yakin ?", posProductListItem) {
                if(posProductListItem is ProductListItem.POSProductListItem){
                    println("${posProductListItem.posProduct.title} adalah pos product")
                    viewModel.deleteAllProductQtyFromProduct(posProductListItem)
                }else{
                    println("${posProductListItem.product.title} adalah bukan pos product")
                }
            }
        } else {
            goToProductDetail()
        }
    }
    private fun confirmationOnCartExist(
        message: String,
        posProductListItem: ProductListItem ?= null,
        onDismiss: () -> Unit = {},
        onNotConfirmed: () -> Unit = {},
        onConfirmed: () -> Unit
    ) {
        requireContext().launchConfirmationDialog(
            message = message,
            onDismiss = onDismiss,
            onPositiveButton = {
                if(posProductListItem == null){
                    viewModel.clearCart()
                }
                onConfirmed()
                it.dismiss()
            },
            onNegativeButton = {
                onNotConfirmed()
                it.dismiss()
            }
        )
    }
    private fun togglePaymentButton(isCartEmpty: Boolean) {
        with(binding.btnPayment) {
            isEnabled = !isCartEmpty
            backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(
                    requireContext(),
                    if (isCartEmpty) R.color.disable_btn else R.color.btn_yelow
                )
            )
        }
    }
    private fun toogleListLoading(fetching: Boolean) {
        with(binding) {
            loading.root.toggleVisibility(fetching)
            rvContainer.toggleVisibility(!fetching)
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    private fun onCustomNominal(minimumPrice: Double, paymentMethod: PaymentMethod) {
        val customNominalFragment = CustomNominalFragment()
        customNominalFragment.setListener(this)
        customNominalFragment.setMinimumPrice(minimumPrice)
        customNominalFragment.setPaymentMethod(paymentMethod)
        customNominalFragment.show(
            requireActivity().supportFragmentManager,
            "customNominalFragment"
        )
    }
    override fun onPay(
        paymentMethod: PaymentMethod,
        paymentType: PaymentCategoryType,
        minimumPrice: Double
    ) {
        when (paymentType) {
            PaymentCategoryType.CASH -> {
                val paymentValue: Double? = paymentMethod.name.toDoubleOrNull()
                if (paymentValue != null) {
                    viewModel.checkout(pos, paymentValue, paymentMethod)
                } else {
                    onCustomNominal(minimumPrice, paymentMethod)
                }
            }
            PaymentCategoryType.NOT_CASH -> {
                viewModel.checkout(pos, minimumPrice, paymentMethod)
            }
        }
    }
    private fun navigateToTransactionStatusActivity(checkoutResult: TransactionDetail) {
        startActivity<TransactionStatusActivity> {
            putExtra(TransactionStatusActivity.TRANSACTION_RESULT_BUNDLE_KEY, checkoutResult)
        }
    }
    override fun onSubmit(
        customPaymentPrice: Double,
        minimumPrice: Double,
        paymentMethod: PaymentMethod
    ) {
        viewModel.checkout(pos, customPaymentPrice, paymentMethod)
    }
}