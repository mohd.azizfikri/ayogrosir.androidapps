package com.soldig.ayogrosir.presentation.feature.virutal.history

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.fragment.app.DialogFragment
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.ActivityVirtualProductHistoryBinding
import com.soldig.ayogrosir.model.VirtualProductHistory
import com.soldig.ayogrosir.presentation.feature.transaction.history.HistoryTransactionActivity
import com.soldig.ayogrosir.utils.DatePickerFragment
import com.soldig.ayogrosir.utils.TimePickerFragment
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf

@AndroidEntryPoint
class VirtualProductHistoryActivity : BaseLoggedInActivity() ,     DatePickerFragment.OnDatePicker,
    TimePickerFragment.OnTimePicker  {
    private lateinit var binding: ActivityVirtualProductHistoryBinding

    private val viewModel : VirtualProductHistoryViewModel by viewModels()

    private val historyAdapter = adapterOf<VirtualProductHistory> {
        register(
            viewHolder = ::VirtualProductHistoryViewHolder,
            layoutResource = R.layout.item_virtual_product_history,
            onBindViewHolder = { vh, _, transactionHistory ->
                vh.bind(transactionHistory)
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVirtualProductHistoryBinding .inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
    }


    override fun onStart() {
        super.onStart()
        viewModel.getHistoryList(currentPOS.token)
        Log.w("http tpken",currentPOS.token)
    }

    private fun initView() {
        initToolbar("Riwayat Produk Virtual",binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initRV()
        initClickListener()
    }

    private fun initRV() {
        binding.rvListCategory.apply {
            adapter = historyAdapter
        }
    }

    private fun observeVM() {
        viewModel.viewState.observe(this){
            println("List yang ada adalah ${it.virtualProductHistoryList}")
            historyAdapter.submitList(it.virtualProductHistoryList)
            with(binding){
                rvListCategory.toggleVisibility(!it.isFetching && it.virtualProductHistoryList.isNotEmpty())
                tvEmptyList.toggleVisibility(!it.isFetching && it.virtualProductHistoryList.isEmpty())
                progresbar.toggleVisibility(it.isFetching)
                btnFiler.isEnabled = !it.isFetching
                btnFiler.setStrokeColorResource(if(it.isFetching) R.color.disable_btn else R.color.primary)
                tvFirstDate.text = it.getStartDateInString() ?: "Tanggal Awal"
                tvLastDate.text = it.getEndDateInString()  ?: "Tanggal Akhir"
            }
        }
    }

    private fun initClickListener() {
        binding.tvFirstDate.setOnClickListener {
            val instance: DialogFragment = DatePickerFragment.newInstance(HistoryTransactionActivity.FIRST_DATE)
            instance.show(supportFragmentManager, "datePicker")
        }

        binding.tvLastDate.setOnClickListener {
            val newFragment: DialogFragment = DatePickerFragment.newInstance(
                HistoryTransactionActivity.LAST_DATE
            )
            newFragment.show(supportFragmentManager, "datePicker")
        }

        binding.btnFiler.setOnClickListener {
        }

    }

    override fun onDateSelect(date: String, type: Int) {
        val instance: DialogFragment = TimePickerFragment.newInstance(date, type)
        instance.show(supportFragmentManager, "timePicker")
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSelect(date: String, time: String, type: Int) {
        val dateInString = "$date $time"
        if (type == HistoryTransactionActivity.FIRST_DATE) {
            viewModel.setStartDate(dateInString)
        } else {
            viewModel.setEndDate(dateInString)
        }
    }

}