package com.soldig.ayogrosir.presentation.feature.transaction.detail

import android.view.View
import com.soldig.ayogrosir.databinding.DetailTransactionProductItemBinding
import com.soldig.ayogrosir.utils.ext.goGone
import com.soldig.ayogrosir.utils.ext.goVisible
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder


class WithTextViewHolder(view: View) : RecyclerViewHolder<ArrayList<String>>(view){

    private val binding = DetailTransactionProductItemBinding.bind(view)

    fun bind(textList: List<String>){
        with(binding){
            if(textList.isNotEmpty()){
                decorator.goGone()
                when(textList.size){
                    1 -> {
                        with(tv1st){
                            goVisible()
                            text = textList[0]
                        }
                        tv2nd.goGone()
                        tv3rd.goGone()
                    }
                    2 -> {
                        with(tv1st){
                            goVisible()
                            text = textList[0]
                        }
                        with(tv3rd){
                            goVisible()
                            text = textList[1]
                        }
                        tv2nd.goGone()
                    }
                    else -> {
                        with(tv1st){
                            goVisible()
                            text = textList[0]
                            if(adapterPosition == 0) textSize = 16.0F
                        }
                        with(tv2nd){
                            goVisible()
                            text = textList[1]
                            if(adapterPosition == 0) textSize = 16.0F
                        }
                        with(tv3rd){
                            goVisible()
                            text = textList[2]
                            if(adapterPosition == 0) textSize = 16.0F
                        }
                    }
                }
            }else{
                containerTextExist.goGone()
                decorator.goVisible()
            }
        }
    }
}