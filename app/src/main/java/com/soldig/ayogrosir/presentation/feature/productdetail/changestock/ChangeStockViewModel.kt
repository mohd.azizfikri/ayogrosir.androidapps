package com.soldig.ayogrosir.presentation.feature.productdetail.changestock

import com.soldig.ayogrosir.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ChangeStockViewModel
@Inject constructor(
) : BaseViewModel<ChangeStockViewState>(){
    override fun initNewViewState(): ChangeStockViewState {
        return ChangeStockViewState(0)
    }


    fun updateNewStock(newStock: Int){
        setViewState(getCurrentViewStateOrNew().apply {
            this.newStock = newStock
        })
    }
}