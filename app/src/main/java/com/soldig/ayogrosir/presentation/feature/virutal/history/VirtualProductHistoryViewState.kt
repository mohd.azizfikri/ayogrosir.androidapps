package com.soldig.ayogrosir.presentation.feature.virutal.history

import com.soldig.ayogrosir.model.VirtualProductHistory
import java.text.SimpleDateFormat
import java.util.*

data class VirtualProductHistoryViewState(
    var virtualProductHistoryList : List<VirtualProductHistory> = listOf(),
    var startDateFilter: Date? = null,
    var endDateFilter: Date? = null,
    var isFetching: Boolean = false
) {



    fun getStartDateInString() : String? = startDateFilter?.let { getDateInString(it) }

    fun getEndDateInString() : String? = endDateFilter?.let { getDateInString(it) }

    private fun getDateInString(date: Date) : String {
        val simpleDateFormat = SimpleDateFormat("dd/MMM/yyyy HH:mm" , Locale("id", "ID") )
        return simpleDateFormat.format(date)
    }
}