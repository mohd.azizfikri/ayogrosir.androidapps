package com.soldig.ayogrosir.presentation.feature.virutal.payments.spinneradapters

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.skydoves.powerspinner.OnSpinnerItemSelectedListener
import com.skydoves.powerspinner.PowerSpinnerInterface
import com.skydoves.powerspinner.PowerSpinnerView
import com.skydoves.powerspinner.databinding.ItemDefaultPowerSpinnerLibraryBinding
import com.soldig.ayogrosir.model.ListrikProduct


class ListrikPrabayarAdapter (
    powerSpinnerView: PowerSpinnerView
) : RecyclerView.Adapter<ListrikPrabayarAdapter.ListirkPrabayarViewHolder>(),
    PowerSpinnerInterface<ListrikProduct> {

    companion object {
        private const val NO_SELECTED_INDEX = -1;
    }



    override var index: Int = powerSpinnerView.selectedIndex
    override val spinnerView: PowerSpinnerView = powerSpinnerView
    override var onSpinnerItemSelectedListener: OnSpinnerItemSelectedListener<ListrikProduct>? = null

    private val spinnerItems: MutableList<ListrikProduct> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListirkPrabayarViewHolder {
        val binding =
            ItemDefaultPowerSpinnerLibraryBinding.inflate(
                LayoutInflater.from(parent.context), parent,
                false
            )
        return ListirkPrabayarViewHolder(binding).apply {
            binding.root.setOnClickListener {
                val position = bindingAdapterPosition.takeIf { it != RecyclerView.NO_POSITION }
                    ?: return@setOnClickListener
                notifyItemSelected(position)
            }
        }
    }


    override fun setItems(itemList: List<ListrikProduct>) {
        this.spinnerItems.clear()
        this.spinnerItems.addAll(itemList)
        this.index = NO_SELECTED_INDEX
        notifyDataSetChanged()
    }

    override fun notifyItemSelected(index: Int) {
        if (index == NO_SELECTED_INDEX) return
        val oldIndex = this.index
        this.index = index
        this.spinnerView.notifyItemSelected(index, spinnerItems[index].productName)
        this.onSpinnerItemSelectedListener?.onItemSelected(
            oldIndex = oldIndex,
            oldItem = oldIndex.takeIf { it != NO_SELECTED_INDEX }?.let { spinnerItems[oldIndex] },
            newIndex = index,
            newItem = spinnerItems[index]
        )
    }

    override fun getItemCount() = spinnerItems.size

    class ListirkPrabayarViewHolder(private val binding: ItemDefaultPowerSpinnerLibraryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ListrikProduct, spinnerView: PowerSpinnerView) {
            binding.itemDefaultText.apply {
                text = item.productName
                typeface = spinnerView.typeface
                gravity = spinnerView.gravity
                setTextSize(TypedValue.COMPLEX_UNIT_PX, spinnerView.textSize)
                setTextColor(spinnerView.currentTextColor)
            }
            binding.root.setPadding(
                spinnerView.paddingLeft,
                spinnerView.paddingTop,
                spinnerView.paddingRight,
                spinnerView.paddingBottom
            )
        }
    }


    override fun onBindViewHolder(holder: ListirkPrabayarViewHolder, position: Int) {
        holder.bind(spinnerItems[position], spinnerView)
    }
}