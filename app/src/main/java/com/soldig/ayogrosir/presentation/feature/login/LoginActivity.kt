package com.soldig.ayogrosir.presentation.feature.login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.activity.viewModels
import com.soldig.ayogrosir.base.BaseActivity
import com.soldig.ayogrosir.databinding.LoginActivityBinding
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.presentation.feature.topup.TopUpActivity
import com.soldig.ayogrosir.model.User
import com.soldig.ayogrosir.utils.ext.showShortSnackbar
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import com.soldig.ayogrosir.utils.other.UIInteraction
import com.soldig.ayogrosir.utils.validator.InputValidation
import com.soldig.ayogrosir.utils.validator.PhoneNumberValidator
import com.soldig.ayogrosir.utils.validator.PinValidator
import com.soldig.ayogrosir.utils.validator.ValidationCallback
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


@AndroidEntryPoint
class LoginActivity : BaseActivity() {

    private lateinit var binding: LoginActivityBinding

    private val viewModel: LoginViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initKeyboardOnEditText()
        initTime(true, binding.include)
        observeVM()
        initListener()

    }


    @SuppressLint("ClickableViewAccessibility")
    private fun initKeyboardOnEditText() {
        with(binding) {
            etPin.setOnTouchListener { view, motionEvent ->
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                binding.keyboard.field = binding.etPin
                false
            }
            etPhone.setOnTouchListener { view, motionEvent ->
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                binding.keyboard.field = binding.etPhone
                false
            }
        }
    }

    private fun observeVM() {
        viewModel.viewState.observe(this, { viewState ->
            with(viewState) {
                binding.loading.root.toggleVisibility(isLoading)
                token?.get()?.let {
                    navigateToTopupActivity(it)
                }
            }
        })
        viewModel.userInteraction.observe(this, {
            val interaction = it.get() ?: return@observe
            if (interaction is UIInteraction.GenericMessage) {
                binding.root.showShortSnackbar(interaction.getMessage(this))
            }

        })
    }


    private fun initListener() {
        with(binding) {
            binding.btnLogin.setOnClickListener {
                val user = User(
                    etPhone.text.toString(),
                    etPin.text.toString()
                )
                viewModel.login(user)
            }

            keyboard.field = binding.etPhone
            etPin.setValidator(
                validationCallback = object : ValidationCallback<String, String> {
                    override fun validator(input: String): InputValidation<String> {
                        return PinValidator.validate(input, resources)
                    }

                    override fun onValidationDone(input: String, isValid: Boolean) {
                        updateButton()
                    }

                },
                binding.tilPin
            )
            etPhone.setValidator(
                validationCallback = object : ValidationCallback<String, String> {
                    override fun validator(input: String): InputValidation<String> {
                        return PhoneNumberValidator.validate(input, resources)
                    }

                    override fun onValidationDone(input: String, isValid: Boolean) {
                        updateButton()
                    }

                },
                binding.tilPhone
            )
            btnLogin.isEnabled = false
//            btnLogin.disableButton()

        }

    }

    private fun updateButton() {
        with(binding) {
            if (etPhone.isInputValid() &&  etPin.isInputValid()) {
                btnLogin.isEnabled = true
            } else {
                btnLogin.isEnabled = false
            }
        }
    }

    private fun navigateToTopupActivity(pos: POS) {
        startActivity(Intent(this, TopUpActivity::class.java).apply {
            putExtra(TopUpActivity.POS_KEY, pos)
            putExtra(TopUpActivity.PASS_KEY, binding.etPin.text.toString())
        })
        finish()
    }
}