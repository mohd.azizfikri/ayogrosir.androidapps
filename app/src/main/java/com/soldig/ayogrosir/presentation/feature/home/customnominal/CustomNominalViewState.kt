package com.soldig.ayogrosir.presentation.feature.home.customnominal

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CustomNominalViewState(
    var minimumPrice : Double = 0.0,
    var inputtedPrice : Double = 0.0
): Parcelable