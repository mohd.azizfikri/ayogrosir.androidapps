package com.soldig.ayogrosir.presentation.feature.transaction.history

import android.os.Parcelable
import com.soldig.ayogrosir.model.TransactionHistory
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class HistoryTransactionViewState(
    var startDateFilter: Date? = null,
    var endDateFilter: Date? = null,
    var isFetching: Boolean = false
) : Parcelable {

    @IgnoredOnParcel
    var historyList: List<TransactionHistory> = listOf()


    fun getStartDateInString() : String? = startDateFilter?.let { getDateInString(it) }

    fun getEndDateInString() : String? = endDateFilter?.let { getDateInString(it) }

    private fun getDateInString(date: Date) : String {
        val simpleDateFormat = SimpleDateFormat("dd/MMM/yyyy HH:mm" , Locale("id", "ID") )
        return simpleDateFormat.format(date)
    }
}