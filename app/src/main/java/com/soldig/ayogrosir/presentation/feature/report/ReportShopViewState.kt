package com.soldig.ayogrosir.presentation.feature.report

import com.soldig.ayogrosir.model.Report
import java.text.SimpleDateFormat
import java.util.*

data class ReportShopViewState(
    var isFetching: Boolean = false,
    var startDate: Date,
    var endDate: Date,
    var report: Report = Report(),
    var walletAmount  : String = "",
    var walletName  : String = ""
){

    fun getStartDateInString() : String = startDate.let { getDateInString(it) }

    fun getEndDateInString() : String = endDate.let { getDateInString(it) }

    private fun getDateInString(date: Date) : String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return simpleDateFormat.format(date)
    }
}
