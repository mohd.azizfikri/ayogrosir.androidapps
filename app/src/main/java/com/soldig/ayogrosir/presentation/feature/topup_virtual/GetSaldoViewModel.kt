package com.soldig.ayogrosir.presentation.feature.topup_virtual

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.home.HomeInteractor
import com.soldig.ayogrosir.interactors.splash.SplashSyncInteractor
import com.soldig.ayogrosir.interactors.topup_virtual.GetSaldoUserInteractor
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.ResponseGetSaldo
import com.soldig.ayogrosir.utils.other.SingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class GetSaldoViewModel
@Inject constructor(
    private val getSaldoUserInteractor: GetSaldoUserInteractor,
    private val homeInteractor: HomeInteractor,
    private val syncWithServer: SplashSyncInteractor
) : BaseViewModel<GetSaldoViewState>() {

    override fun initNewViewState(): GetSaldoViewState {
        return GetSaldoViewState()
    }

    val isUpdateDone : LiveData<Boolean>
        get() = _isUpdateDone

    private val _isUpdateDone = MutableLiveData(false)

    private fun setLoading(isLoading: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.isLoading = isLoading
        })
    }

    private fun setDataTopup(pos: ResponseGetSaldo) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.responGetSaldo = SingleEvent(pos)
        })
    }

    fun getSaldo(
        token: String
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                getSaldoUserInteractor.getSaldoUser.fetch(token).collect {
                    onCollect(it,
                        onLoading = { isLoading ->
                            setLoading(isLoading)
                        }
                    ) { resource ->
                        setLoading(false)

                        setDataTopup(resource.data)
                    }
                }

            }
        }
    }

    fun syncWithServer(
        pos: POS
    ) {
        viewModelScope.launch(IO)  {
            syncWithServer.fetch(pos).collect {
                when(it){
                    is Resource.Success -> {
                        fetchProductList(pos)
                    }
                    is Resource.Error -> {
                        _isUpdateDone.value = true
                    }
                    else -> {}
                }
            }
        }.invokeOnCompletion {
            Log.w("KaWaNSAD XX 0", "product/list: "+ "Awal")
        }
    }

    fun fetchProductList(pos: POS) {
        viewModelScope.launch(IO) {
            homeInteractor.getAllProduct.fetch(pos).collect {
                onCollect(it){
                    result -> setProductList()
                }
            }
        }
    }

    private fun setProductList() {
        _isUpdateDone.postValue(true)
    }

}