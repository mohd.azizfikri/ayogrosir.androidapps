package com.soldig.ayogrosir.presentation.feature.order.orderlist

import android.os.Parcelable
import com.soldig.ayogrosir.model.TransactionHistory
import com.soldig.ayogrosir.model.TransactionStatus
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class OrderTransactionViewState(
    var startDateFilter: Date? = null,
    var endDateFilter: Date? = null,
    var statusFilter : TransactionStatus? = null,
    var isFetching: Boolean = false,
) : Parcelable {

    @IgnoredOnParcel
    var orderList: List<TransactionHistory> = listOf()


    fun getStartDateInString() : String? = startDateFilter?.let { getDateInString(it) }

    fun getEndDateInString() : String? = endDateFilter?.let { getDateInString(it) }

    private fun getDateInString(date: Date) : String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return simpleDateFormat.format(date)
    }
}