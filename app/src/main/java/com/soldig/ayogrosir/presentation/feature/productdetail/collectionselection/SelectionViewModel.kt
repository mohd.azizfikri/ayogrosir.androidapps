package com.soldig.ayogrosir.presentation.feature.productdetail.collectionselection

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.productdetail.ProductDetailInteractor
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.Product
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SelectionViewModel
@Inject constructor(
    private val productDetailInteractor: ProductDetailInteractor
) : BaseViewModel<SelectionViewState>(){

    override fun initNewViewState(): SelectionViewState {
        return SelectionViewState()
    }


    fun getAllAvailableProduct(pos: POS){
        viewModelScope.launch(IO){
            productDetailInteractor.getAllAvailableProduct.fetch(pos).collect {
                onCollect(
                    response = it,
                    onLoading = {
                        setLoading(it)
                    },
                    executeOnSuccess = { result : Resource.Success<List<Product>> ->
                        setNewProductList(result.data)
                    }
                )
            }
        }
    }

    private fun setLoading(isLoading: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            isFetchingList = isLoading
        })
    }

    private fun setNewProductList(data: List<Product>) {
        viewModelScope.launch(IO){
            val newList = ArrayList(SelectionViewType.produce32EmptyProduct())
            Log.w("Select product","1. "+ data.size)
            data.forEach { product ->
                if(product.isPilihan && product.ordering != null){
                    product.ordering?.takeIf { it in 1..32 }?.let {
                        newList.set(it - 1, SelectionViewType.ProductExist(product))
                    }
                    Log.w("Select product","2. "+ product.title)
                }
            }
            setViewState(getCurrentViewStateOrNew().apply {
                selectedList = newList
                Log.w("Select product","3. "+ newList.size)
            })
        }
    }


}