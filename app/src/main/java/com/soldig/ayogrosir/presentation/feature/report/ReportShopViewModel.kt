package com.soldig.ayogrosir.presentation.feature.report

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.interactors.report.ReportInteractor
import com.soldig.ayogrosir.model.Report
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ReportShopViewModel
@Inject constructor(
    private val reportInteractor: ReportInteractor
) : BaseViewModel<ReportShopViewState>(){


    override fun initNewViewState(): ReportShopViewState {
        return ReportShopViewState(
            startDate = getCurrentDayStart(),
            endDate = Date()
        )
    }

    private fun getCurrentDayStart() : Date {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar[Calendar.MILLISECOND] = 0
        calendar[Calendar.SECOND] = 0
        calendar[Calendar.MINUTE] = 0
        calendar[Calendar.HOUR_OF_DAY] = 0
        return calendar.time
    }

    fun fetch(token: String){
        val viewState = getCurrentViewStateOrNew()
        viewModelScope.launch(IO){
            reportInteractor.getReport.fetch(token,viewState.startDate,viewState.endDate).collect {
                onCollect(
                    response = it,
                    onLoading = {
                        setViewState(getCurrentViewStateOrNew().apply {
                            isFetching = it
                        })
                    },
                    executeOnSuccess = { successData ->
                        setNewReport(successData.data)
                    }
                )
            }
        }

    }

    private fun setNewReport(report: Report){
        viewModelScope.launch(Default){
            println("Total Canceled Product adalah ${report.canceledProductQty}")
            var walletAmount = ""
            var walletName = ""
            report.reportWallet.forEachIndexed { index, reportWallet ->
                walletAmount += ":   ${reportWallet.total.toMoneyFormat()}${if(index!=report.reportWallet.lastIndex) "\n" else ""}"
                walletName += "${reportWallet.name}${if(index!=report.reportWallet.lastIndex) "\n" else ""}"
            }
            setViewState(getCurrentViewStateOrNew().apply {
                this.report = report
                this.walletAmount = walletAmount
                this.walletName = walletName
            })
        }
    }


    fun setStartDate(date:String){
        setViewState(getCurrentViewStateOrNew().apply {
            changeDateFormat(date)?.let {
                startDate = it
            }

        })
    }

    fun setEndDate(date: String){
        setViewState(getCurrentViewStateOrNew().apply {
            changeDateFormat(date)?.let {
                endDate = it
            }

        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun changeDateFormat(date: String): Date? {
        val format = "dd/MM/yyyy HH:mm"
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.parse(date)
    }
}