package com.soldig.ayogrosir.presentation.feature.productdetail

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.ProductDetailActivityBinding
import com.soldig.ayogrosir.presentation.feature.productdetail.changediscount.ChangeDiscountFragment
import com.soldig.ayogrosir.presentation.feature.productdetail.changeprice.ChangePriceFragment
import com.soldig.ayogrosir.presentation.feature.productdetail.changeprice.ChangePriceMode
import com.soldig.ayogrosir.presentation.feature.productdetail.changestock.ChangeStockFragment
import com.soldig.ayogrosir.presentation.feature.productdetail.collectionselection.CollectionSelectionProductFragment
import com.soldig.ayogrosir.model.DiscountType
import com.soldig.ayogrosir.model.ParcelableProduct
import com.soldig.ayogrosir.model.Product
import com.soldig.ayogrosir.utils.Constant
import com.soldig.ayogrosir.utils.ext.showLongSnackbar
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailActivity : BaseLoggedInActivity(), ChangePriceFragment.OnChangePriceListener,
    ChangeStockFragment.OnChangeStockListener, ChangeDiscountFragment.OnChangeDiscountListener,
    CollectionSelectionProductFragment.OnSelectionSelectedListener {
    private lateinit var binding: ProductDetailActivityBinding

    private val viewModel: ProductDetailViewModel by viewModels()

    companion object {
        const val PRODUCT_EXTRAS_KEY =
            "com.soldig.amanahmart.presentation.feature.detail.ProductDetailActivity.PRODUCT_EXTRAS_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ProductDetailActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
        getProductFromExtras()
    }

    private fun observeVM() {
        viewModel.viewState.observe(this) {
            println("Ada product ${it.chosenProduct?.isActive}")
            populateProduct(it.chosenProduct, it.productId)
            binding.loading.root.toggleVisibility(it.isUpdating)
        }

        viewModel.userInteraction.observe(this) {
            val uiInteraction = it.get() ?: return@observe
            if (uiInteraction !is UIInteraction.DoNothing) {
                binding.root.showLongSnackbar(uiInteraction.getMessage(this))
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun populateProduct(chosenProduct: Product?, productId: String?) {
        chosenProduct ?: return
        with(binding) {
            Glide
                .with(this.root)
                .load(chosenProduct.getImageURL())
                .centerInside()
                .into(imgProduct)

            tvName.text = ": ${chosenProduct.title}"
            tvDiscount.text = ": ${chosenProduct.getDisountString()}"
            tvPurchasePrice.text =
                ": ${chosenProduct.purchasePrice.toMoneyFormat(Constant.PREFIX_RP)}"
            tvSellPrice.text = ": ${chosenProduct.sellPrice.toMoneyFormat(Constant.PREFIX_RP)}"
            tvStock.text = ": ${chosenProduct.qty}"
            tvPackSize.text = ": ${chosenProduct.packSize}"
            toogleButton(chosenProduct.isActive)
        }
    }



    private fun toogleButton(active: Boolean) {
        with(binding.btnToogleActivate) {
            text = if (active) "Non Aktifkan" else "Aktifkan"
            backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(
                    this@ProductDetailActivity,
                    if (active) R.color.white else R.color.primary
                )
            )
            setTextColor(
                ColorStateList.valueOf(
                    ContextCompat.getColor(
                        this@ProductDetailActivity,
                        if (!active) R.color.white else R.color.primary
                    )
                )
            )
            setOnClickListener {
                viewModel.toggleProductIsActive(currentPOS)
            }
        }
    }


    private fun initView() {
        initToolbar("Produk Detail", binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initClickListener()
    }


    private fun getProductFromExtras() {
        (intent.extras?.get(ProductDetailActivity.PRODUCT_EXTRAS_KEY) as ParcelableProduct?)?.let { it ->
            print("Menrima product dengan id : ${it.id}")
            viewModel.setProduct(Product.buildFromParcelableProduct(it), it.id)
        } ?: throw Exception("No Product Given")
    }


    private fun initClickListener() {
        binding.btnChoice.setOnClickListener {
            val currentProduct = viewModel.getCurrentViewStateOrNew().chosenProduct ?: return@setOnClickListener
            val selectionFragment = CollectionSelectionProductFragment().apply {
                setListener(this@ProductDetailActivity)
                setCurrentProductId(currentProduct.productId)
                show(supportFragmentManager, "collectionSelectionProductFragment")
            }
        }

        binding.btnDiscount.setOnClickListener {
            val changeDiscountFragment = ChangeDiscountFragment()
            changeDiscountFragment.setListener(this)
            changeDiscountFragment.show(
                supportFragmentManager, "changeDiscountFragment"
            )
        }

        binding.btnChangePriceBuy.setOnClickListener {
            val changePriceFragment = ChangePriceFragment()
            changePriceFragment.setListener(this)
            changePriceFragment.setType(
                viewModel.getCurrentViewStateOrNew().chosenProduct?.purchasePrice ?: 0.0
            )
            changePriceFragment.show(
                supportFragmentManager, "changePriceFragmentBuy"
            )
        }

        binding.btnChangePriceSell.setOnClickListener {
            val changePriceFragment = ChangePriceFragment()
            changePriceFragment.setListener(this)
            changePriceFragment.setType(
                viewModel.getCurrentViewStateOrNew().chosenProduct?.sellPrice ?: 0.0,
                viewModel.getCurrentViewStateOrNew().chosenProduct?.purchasePrice ?: 0.0
            )
            changePriceFragment.show(
                supportFragmentManager, "changePriceFragmentSell"
            )
        }

        binding.btnStock.setOnClickListener {
            val changeStockFragment = ChangeStockFragment()
            changeStockFragment.setListener(this)
            changeStockFragment.show(
                supportFragmentManager, "changeStockFragment"
            )
        }

    }


    override fun onSavePrice(changePriceMode: ChangePriceMode, newPriceValue: Double) {
        when (changePriceMode) {
            ChangePriceMode.SELL -> viewModel.updateSellPrice(currentPOS, newPriceValue)
            ChangePriceMode.BUY -> viewModel.updateBuyPrice(currentPOS, newPriceValue)
        }
    }

    override fun onSaveChangeStock(newStock: Int) {
        viewModel.updateStock(currentPOS, newStock)
    }

    override fun onSaveChangeDiscount(discountType: DiscountType, discountValue: Double) {
        viewModel.updateDiscount(currentPOS, discountType, discountValue)
    }

    override fun onAdd(position: Int) {
        viewModel.makeProductTobePilihan(currentPOS, position)
    }

    override fun onDelete(position: Int) {
        viewModel.deleteProductFromPilihan(currentPOS)
    }


}