package com.soldig.ayogrosir.presentation.feature.home.transactionstatus

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.TransactionDetail
import com.soldig.ayogrosir.utils.other.SingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionStatusViewModel @Inject
constructor(

) : BaseViewModel<TransactionStatusViewState>(){



    override fun initNewViewState(): TransactionStatusViewState {
        return TransactionStatusViewState(false,null)
    }

    private fun setLoading(isLoading: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            isPrintingLoading = isLoading
        })
    }

    fun requestPrint(currentPOS: POS,transactionResult : TransactionDetail) {
        viewModelScope.launch(IO){
            setLoading(true)
            val header =
                        "[L]\n" +
                        "[C]${currentPOS.name}\n" +
                        "[C]${currentPOS.alamat}\n\n" +
                        "[L]No  ${transactionResult.orderNumber}\n" +
                        "[L]TGL ${transactionResult.date.split(" ")[0]}\n" +
                        "[L]JAM ${transactionResult.date.split(" ")[1]}\n" +
                        "[C]================================\n"



            var itemDetailContent = ""

            transactionResult.productList.forEachIndexed{ index, transactionProductDetail ->
                itemDetailContent += ( "[L]\n" +
                        "[L]${index+1} <b>${transactionProductDetail.name}</b>\n" +
                        "[L]  ${transactionProductDetail.priceBeforeDiscount.toMoneyFormat()} x ${transactionProductDetail.jml}[R]${(transactionProductDetail.priceBeforeDiscount * transactionProductDetail.jml).toMoneyFormat()}\n")

                if(transactionProductDetail.discountValue > 0.0) {
                    itemDetailContent += "[L]  Diskon[R]${(transactionProductDetail.priceBeforeDiscount-transactionProductDetail.priceAfterDiscount).toMoneyFormat()}\n"
                }
            }


            val footer =
                "[C]================================\n" +
                        "[L]\n" +
            "[R]TOTAL :[R]${transactionResult.totalAfterDiscount.toMoneyFormat()}\n" +
                    "[R]Bayar :[R]${transactionResult.totalPaid.toMoneyFormat()}\n" +
                    "[R]Pembayaran :[R]${transactionResult.paymentWith}\n" +
                    "[R]Kembalian :[R]${transactionResult.totalChange.toMoneyFormat()}\n" +
                    "[R]Anda Hemat :[R]${transactionResult.totalDiscount.toMoneyFormat()}\n" +
                    "[L]\n" +
                    "[L]\n" +
                    "[L]TERIMAKASIH TELAH BERBELANJA\n"+
                    "[L]\n" +
                    "[L]\n"

            val finalPrinted = header + itemDetailContent + footer
            println(finalPrinted)
            setViewState(getCurrentViewStateOrNew().apply {
                isPrintingLoading = false
                printedString = SingleEvent(finalPrinted)
            })
        }
    }
}