package com.soldig.ayogrosir.presentation.feature.shop.setting

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.SettingActivityBinding
import com.soldig.ayogrosir.presentation.feature.shop.schedule.ScheduleActivity
import com.soldig.ayogrosir.model.ParcelableSchedule
import com.soldig.ayogrosir.utils.ext.showShortSnackbar
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SettingActivity : BaseLoggedInActivity() {
    private lateinit var scheduleAdapter: ScheduleAdapter
    private lateinit var binding: SettingActivityBinding

    private val viewModel by viewModels<SettingViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SettingActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initToolbar(getString(R.string.drawer_item_settings), binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initView()
        observeVM()

    }

    override fun onStart() {
        super.onStart()
        fetchSchedule()
    }

    private fun fetchSchedule() {
        viewModel.getScheduleList(currentPOS.token)
    }

    private fun observeVM() {
        viewModel.viewState.observe(this){viewState ->
            binding.loading.root.toggleVisibility(viewState.isFetching)
            scheduleAdapter.newList(viewState.settingModel.scheduleList)
            binding.toogleDelivery.isChecked = viewState.settingModel.isDeliveryAvailable
        }
        viewModel.userInteraction.observe(this){
            val uiInteraction = it.get() ?: return@observe
            if(uiInteraction !is UIInteraction.DoNothing){
                binding.root.showShortSnackbar(uiInteraction.getMessage(this))
            }
        }
    }

    private fun initView() {
        initRv()
        initClickListener()
    }

    private fun initClickListener() {
        with(binding){
            toogleDelivery.setOnClickListener {
                viewModel.changeDeliveryStatus(currentPOS.token, toogleDelivery.isChecked)
            }
        }
    }

    private fun initRv() {
        scheduleAdapter = ScheduleAdapter{
            val intent = Intent(this,ScheduleActivity::class.java).apply {
                putExtra(ScheduleActivity.SCHEDULE_DATA_INTENT,ParcelableSchedule(it.day,it.hour))
            }
            startActivity(intent)
        }
        binding.rvListSchedule.apply {
            adapter = scheduleAdapter
        }
    }

}