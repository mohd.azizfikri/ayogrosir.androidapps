package com.soldig.ayogrosir.presentation.feature.search

import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.search.SearchInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SearchViewModel
@Inject constructor(
    private val searchInteractor: SearchInteractor
) : BaseViewModel<SearchViewState>(){
    override fun initNewViewState(): SearchViewState {
        return SearchViewState(false, SearchViewState.Mode.Keyboard,"" , true)
    }

    fun setSearchKey(searchKey : String){
        setIsEmpty(false)
        setViewState(getCurrentViewStateOrNew().apply {
            nameKey = searchKey
        })
    }

    private fun setIsEmpty(isEmpty: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            this.isEmpty = isEmpty
        })
    }

    fun setCurrentMode(inputMode: SearchViewState.Mode) {
        val viewState = getCurrentViewStateOrNew()
        if(viewState.mode != inputMode){
            setViewState(viewState.apply {
                this.mode = inputMode
            })
        }

    }

}