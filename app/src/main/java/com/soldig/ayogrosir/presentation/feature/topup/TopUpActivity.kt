package com.soldig.ayogrosir.presentation.feature.topup

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseActivity
import com.soldig.ayogrosir.databinding.TopupActivityBinding
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.presentation.feature.MainActivity
import com.soldig.ayogrosir.utils.ext.*
import com.soldig.ayogrosir.utils.other.UIInteraction
import com.soldig.ayogrosir.utils.validator.CommonStringValidator
import com.soldig.ayogrosir.utils.validator.InputValidation
import com.soldig.ayogrosir.utils.validator.ValidationCallback
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class TopUpActivity : BaseActivity() {


    companion object {
        const val POS_KEY = "com.soldig.amanahmart.presentation.feature.topup.TopUpActivity_POS_KEY"
        const val PASS_KEY =  "com.soldig.amanahmart.presentation.feature.topup.TopUpActivity_PASS_KEY"
    }

    private lateinit var binding: TopupActivityBinding

    private val viewModel by viewModels<TopupViewModel>()


    private val currentPOS: POS by lazy {
        (intent.extras?.getParcelable(POS_KEY) as POS?) ?: throw Exception("No POS Provided")
    }

    private val currentPOSPassword: String by lazy {
        (intent.extras?.getString(PASS_KEY)) ?: throw Exception("No POS Provided")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TopupActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initTime(true, binding.include)
        initKeyboardOnEditText()
        initView()
        observeVM()

    }

    private fun observeVM() {
        viewModel.viewState.observe(this){viewState ->
            with(binding){
                println("is loading ? ${viewState.isFetching}")
                loading.root.toggleVisibility(viewState.isFetching)
                if(viewState.isTopUpDone){
                    setUser(currentPOS)

                    navigateToMain()
                }
            }
        }
        viewModel.userInteraction.observe(this){
            val uiInteraction = it.get() ?: return@observe
            if(uiInteraction !is UIInteraction.DoNothing){
                binding.root.showShortSnackbar(uiInteraction.getMessage(this))
            }
        }
    }

    private fun initView() {
        initEditTextValidator()
        initListener()
    }

    private fun initEditTextValidator() {
        binding.etPopup.setValidator(
            object: ValidationCallback<String, String>{
                override fun validator(input: String): InputValidation<String> {
                    return CommonStringValidator.blankValidator(input, getString(R.string.input_topup_blank))
                }

                override fun onValidationDone(input: String, isValid: Boolean) {
                    viewModel.setBalanceValue(input.toDoubleOrNull() ?: 0.0)
                    if(isValid) binding.btnDone.enableButton() else binding.btnDone.disableButton()
                }

            },
            binding.tilTopup
        )
        binding.btnDone.disableButton()
    }

    private fun initListener() {
        with(binding){
            btnDone.setOnClickListener {
                etPopup.text.toString().toDoubleOrNull()?.let{
//                    viewModel.postStartingBalance(it, currentPOS.token, currentPOSPassword)
                    lifecycleScope.launch(Dispatchers.IO) {
                        delay(500)
                        withContext(Dispatchers.Main) {
//                            if(isNetworkAvailable()) {
                                viewModel.processingTopUp(currentPOS, it, currentPOS.token, currentPOSPassword)
//                            }else{
//                                binding.root.showShortSnackbar("Tidak ada akses internet! Periksa koneksi anda")
//                            }
                        }
                    }
                }
            }
            btnDone.goGone()
            btnDone.goVisible()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initKeyboardOnEditText() {
        binding.keyboard.field = binding.etPopup
        binding.etPopup.setOnTouchListener { view, motionEvent ->
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            binding.keyboard.field = binding.etPopup
            false
        }
    }



    private fun navigateToMain(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}