package com.soldig.ayogrosir.presentation.feature.shop.setting

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.MessageType
import com.soldig.ayogrosir.interactors.setting.SettingInteractor
import com.soldig.ayogrosir.model.SettingModel
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingViewModel
@Inject constructor(
    private val settingInteractor: SettingInteractor
) : BaseViewModel<SettingViewState>() {

    fun getScheduleList(token: String) {
        viewModelScope.launch(IO){
            settingInteractor.getCurrentSchedule.fetch(token).collect {
                onCollect(
                    response = it,
                    onLoading = {
                        setScheduleViewStateLoading(it)
                    },
                    executeOnSuccess = { result ->
                        setCurrentSetting(result.data)
                    }
                )
            }
        }
    }

    fun changeDeliveryStatus(token:String, isOpen: Boolean){
        viewModelScope.launch(IO){
            settingInteractor.setDeliveryStatus.fetch(token, isOpen).collect {
                onCollect(
                    response = it,
                    onLoading = {
                        setDeliveryFetchingStatus(isFetching = it)
                    },
                    onError = {
                      setCurrentStatus(!isOpen)
                    },
                    executeOnSuccess = { successResult ->
                        setCurrentStatus(isOpen)
                        setUserInteraction(UIInteraction.GenericMessage(MessageType.StringMessage(successResult.data)))
                    }
                )
            }
        }
    }

    private fun setCurrentStatus(isDeliveryAvailable: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            this.settingModel = this.settingModel.apply {
                this.isDeliveryAvailable = isDeliveryAvailable
            }
        })
    }

    private fun setDeliveryFetchingStatus(isFetching: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            isUpdatingDeliveryStatus = isFetching
        })
    }

    private fun setCurrentSetting(settingModel : SettingModel) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.settingModel = settingModel
        })
    }

    private fun setScheduleViewStateLoading(isLoading: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            isFetching = isLoading
        })
    }

    override fun initNewViewState(): SettingViewState {
        return SettingViewState()
    }

}