package com.soldig.ayogrosir.presentation.feature.home.payment

import com.soldig.ayogrosir.model.PaymentCategory

data class PaymentCategoryListItem(
    val paymentCategory: PaymentCategory,
    var clickedItem: Int = -1,
    var isSelected: Boolean = false
)