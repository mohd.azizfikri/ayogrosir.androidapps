package com.soldig.ayogrosir.presentation.feature.productdetail.changestock

import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.postDelayed
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.soldig.ayogrosir.base.BaseDialogFragment
import com.soldig.ayogrosir.databinding.ChangeStockFragmentBinding
import com.soldig.ayogrosir.presentation.feature.productdetail.ProductDetailViewModel
import com.soldig.ayogrosir.model.Product
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChangeStockFragment : BaseDialogFragment() {
    private var _binding: ChangeStockFragmentBinding? = null
    private val binding get() = _binding!!

    private val activityViewModel : ProductDetailViewModel by activityViewModels()
    private val viewModel: ChangeStockViewModel by viewModels()

    private lateinit var mListener: OnChangeStockListener

    fun setListener(listener: OnChangeStockListener) {
        mListener = listener
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        _binding = ChangeStockFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeVM()
    }

    override fun rootView(): View = binding.root

    private fun observeVM() {
        activityViewModel.viewState.observe(viewLifecycleOwner){
            plotProduct(it.chosenProduct)
        }
    }

    private fun plotProduct(chosenProduct: Product?) {
        chosenProduct ?: return
        val newStock = viewModel.getCurrentViewStateOrNew().newStock
        val newStockInString = if(newStock == 0) chosenProduct.qty.toString() else newStock.toString()
        binding.etStock.setText(newStockInString)
        binding.etStock.setSelection(newStockInString.length)
    }

    private fun initView() {
        initClickListener()
        initEtListener()
    }

    private fun initEtListener() {
        binding.etStock.requestFocus()
        binding.etStock.filters = arrayOf(object: InputFilter{
            override fun filter(
                source: CharSequence,
                start: Int,
                end: Int,
                dest: Spanned,
                dstart: Int,
                dend: Int
            ): CharSequence? {
                if(source.toString().isBlank() || source.toString().toIntOrNull() ?: 0 < 0){
                    return "0"
                }else if(source.toString()[0] == '0' && source.toString().length > 1){
                    return source.toString().split("0")[1]
                }
                return null
            }

        })
        binding.etStock.addTextChangedListener {
            binding.etStock.postDelayed(50L){
                binding.etStock.setSelection(it.toString().length)
            }
            viewModel.updateNewStock(it.toString().toIntOrNull() ?: 0)
        }
    }

    private fun initClickListener() {

        binding.btnCancel.setOnClickListener {
            dismiss()
        }
        binding.btnSave.setOnClickListener {
            dismiss()
            val newStock = viewModel.getCurrentViewStateOrNew().newStock
            mListener.onSaveChangeStock(newStock)
        }

        binding.ivPlus.setOnClickListener {
            val qty = binding.etStock.text.toString().toIntOrNull() ?: 0
            binding.etStock.setText((qty + 1).toString())
        }

        binding.ivMinus.setOnClickListener {
            val qty = binding.etStock.text.toString().toIntOrNull() ?: 0
            binding.etStock.setText(((qty - 1).takeIf { it > 0 } ?: 0).toString())
        }

        binding.container.setOnClickListener {
            dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    interface OnChangeStockListener {
        fun onSaveChangeStock(newStock : Int)
    }
}