package com.soldig.ayogrosir.presentation.feature.order.orderlist

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.MessageType
import com.soldig.ayogrosir.interactors.order.OrderInteractor
import com.soldig.ayogrosir.model.TransactionHistory
import com.soldig.ayogrosir.model.TransactionStatus
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class OrderTransactionViewModel
@Inject constructor(
    private val orderInteractor: OrderInteractor
) : BaseViewModel<OrderTransactionViewState>(){
    override fun initNewViewState(): OrderTransactionViewState {
        return OrderTransactionViewState()
    }


    fun fetch(token: String){
        val viewState = getCurrentViewStateOrNew()
        viewModelScope.launch(Dispatchers.IO){
            orderInteractor.getAllOrderList.fetch(token,viewState.startDateFilter, viewState.endDateFilter, viewState.statusFilter).collect {
                onCollect(
                    response = it,
                    onLoading = { isLoading ->
                        toogleLoading(isLoading)
                    },
                    executeOnSuccess = { successResult ->
                        setCurrentList(successResult.data)

                    }
                )
            }
        }
    }

    fun updateOrder(token: String,id:String, transactionStatus: TransactionStatus){
        val viewState = getCurrentViewStateOrNew()
        viewModelScope.launch(Dispatchers.IO){
            orderInteractor.updateOrder.fetch(token,id, transactionStatus).collect {
                onCollect(
                    response = it,
                    onLoading = { isLoading ->
                        toogleLoading(isLoading)
                    },
                    executeOnSuccess = { successResult ->
                        setUserInteraction(UIInteraction.GenericMessage(MessageType.StringMessage(successResult.data)))
                        fetch(token)
                    }
                )
            }
        }
    }

    private fun setCurrentList(data: List<TransactionHistory>) {
        setViewState(getCurrentViewStateOrNew().apply {
            orderList = data
        })
    }

    private fun toogleLoading(loading: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            isFetching = loading
        })
    }

    fun setStartDate(date:String){
        setViewState(getCurrentViewStateOrNew().apply {
            startDateFilter = changeDateFormat(date)
        })
    }

    fun setEndDate(date: String){
        setViewState(getCurrentViewStateOrNew().apply {
            endDateFilter = changeDateFormat(date)
        })
    }

    fun setStatus(transactionStatus: TransactionStatus?){
        setViewState(getCurrentViewStateOrNew().apply {
            statusFilter = transactionStatus
        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun changeDateFormat(date: String): Date? {
        val format = "dd/MM/yyyy HH:mm"
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.parse(date)
    }
}