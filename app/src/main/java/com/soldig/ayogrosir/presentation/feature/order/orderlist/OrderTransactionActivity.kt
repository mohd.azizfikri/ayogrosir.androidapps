package com.soldig.ayogrosir.presentation.feature.order.orderlist

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.DialogFragment
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.OrderTransactionActivityBinding
import com.soldig.ayogrosir.model.TransactionHistory
import com.soldig.ayogrosir.model.TransactionStatus
import com.soldig.ayogrosir.presentation.feature.transaction.detail.DetailTransactionActivity
import com.soldig.ayogrosir.presentation.feature.transaction.history.HistoryTransactionActivity
import com.soldig.ayogrosir.utils.DatePickerFragment
import com.soldig.ayogrosir.utils.TimePickerFragment
import com.soldig.ayogrosir.utils.ext.launchConfirmationDialog
import com.soldig.ayogrosir.utils.ext.launchOneChoiceDialog
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf


@AndroidEntryPoint
class OrderTransactionActivity : BaseLoggedInActivity(),
    DatePickerFragment.OnDatePicker,
    TimePickerFragment.OnTimePicker,
    OrderTransactionViewHolder.OrderTransactionListListener{
    private lateinit var binding: OrderTransactionActivityBinding
    private val viewModel : OrderTransactionViewModel by viewModels()

    private val orderAdapter = adapterOf<TransactionHistory> {
        register(
            viewHolder = ::OrderTransactionViewHolder,
            layoutResource = R.layout.order_transaction_list_item,
            onBindViewHolder = { vh, _, order ->
                vh.bind(order, this@OrderTransactionActivity)
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OrderTransactionActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
        viewModel.fetch(currentPOS.token)
    }

    private fun observeVM() {
        viewModel.viewState.observe(this){
            with(binding){
                loading.root.toggleVisibility(it.isFetching)
                rvListOrder.toggleVisibility(!it.isFetching && it.orderList.isNotEmpty())
                tvEmptyList.toggleVisibility(!it.isFetching && it.orderList.isEmpty())
                tvFirstDate.text = it.getStartDateInString() ?: "Tanggal Awal"
                tvLastDate.text = it.getEndDateInString()  ?: "Tanggal Akhir"
                btnStatusFilter.text = it.statusFilter?.getInString() ?: "Pilih Status"

            }
            orderAdapter.submitList(it.orderList)
        }
    }

    private fun initView() {
        initToolbar("Pesanan", binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initRv()
        initClickListener()

    }

    private fun initClickListener() {
        binding.tvFirstDate.setOnClickListener {
            val instance: DialogFragment = DatePickerFragment.newInstance(FIRST_DATE)
            instance.show(supportFragmentManager, "datePicker")
        }

        binding.tvLastDate.setOnClickListener {
            val newFragment: DialogFragment = DatePickerFragment.newInstance(LAST_DATE)
            newFragment.show(supportFragmentManager, "datePicker")
        }

        binding.btnFilter.setOnClickListener {
            viewModel.fetch(currentPOS.token)
        }

        binding.btnStatusFilter.setOnClickListener {
            val choices = TransactionStatus.values()
            val finalChoices = ArrayList<String>()
            finalChoices.addAll(choices.filter { it != TransactionStatus.UNKNOWN }.map { it.getInString() })
            finalChoices.add("Hapus Filter")
            launchOneChoiceDialog(
                "Pilih Status",
                finalChoices,
                viewModel.getCurrentViewStateOrNew().statusFilter?.value?.minus(1)  ?: finalChoices.lastIndex
            ){ materialDialog, selectedIndex ->
                if(selectedIndex == finalChoices.lastIndex){
                    viewModel.setStatus(null)
                }else{
                    viewModel.setStatus(choices[selectedIndex])
                }
                materialDialog.dismiss()
            }
        }
    }

    private fun initRv() {
        binding.rvListOrder.apply {
            adapter = orderAdapter
        }

    }


    override fun onDateSelect(date: String, type: Int) {
        val instance: DialogFragment = TimePickerFragment.newInstance(date, type)
        instance.show(supportFragmentManager, "timePicker")
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSelect(date: String, time: String, type: Int) {
        val dateInString = "$date $time"
        if (type == HistoryTransactionActivity.FIRST_DATE) {
            viewModel.setStartDate(dateInString)
        } else {
            viewModel.setEndDate(dateInString)
        }
    }

    companion object {
        const val FIRST_DATE = 1
        const val LAST_DATE = 2
    }

    override fun onDetailClicked(transactionHistory: TransactionHistory) {
        val intent = Intent(this, DetailTransactionActivity::class.java).apply {
            putExtra(
                DetailTransactionActivity.TRANSACTION_ID_KEY,
                transactionHistory.id.toString()
            )
            putExtra(
                DetailTransactionActivity.TYPE_KEY,
                DetailTransactionActivity.DetailType.Order
            )
        }
        startActivity(intent)
    }
    override fun onDeliverClicked(transactionHistory: TransactionHistory) {
        launchConfirmationDialog(
            message = "Apakah anda yakin ingin mengubah status pesanan menjadi dikirim?",
            onPositiveButton = {
                viewModel.updateOrder(currentPOS.token, transactionHistory.id.toString(), TransactionStatus.DIKIRIM)
                it.dismiss()
            },
            onNegativeButton = {
                it.dismiss()
            }
        )

    }

    override fun onRejectClicked(transactionHistory: TransactionHistory) {
        launchConfirmationDialog(
            message = "Apakah anda yakin ingin menolak pesanan?",
            onPositiveButton = {
                viewModel.updateOrder(currentPOS.token, transactionHistory.id.toString(), TransactionStatus.DITOLAK)
                it.dismiss()
            },
            onNegativeButton = {
                it.dismiss()
            }
        )

    }

    override fun onDoneClicked(transactionHistory: TransactionHistory) {
        launchConfirmationDialog(
            message = "Apakah anda yakin ingin menyelesaikan pesanan?",
            onPositiveButton = {
                viewModel.updateOrder(currentPOS.token, transactionHistory.id.toString(), TransactionStatus.SELESAI)
                it.dismiss()
            },
            onNegativeButton = {
                it.dismiss()
            }
        )

    }

    override fun onAcceptClicked(transactionHistory: TransactionHistory) {
        launchConfirmationDialog(
            message = "Apakah anda yakin ingin menerima pesanan?",
            onPositiveButton = {
                viewModel.updateOrder(currentPOS.token, transactionHistory.id.toString(), TransactionStatus.DITERIMA)
                it.dismiss()
            },
            onNegativeButton = {
                it.dismiss()
            }
        )
    }
}