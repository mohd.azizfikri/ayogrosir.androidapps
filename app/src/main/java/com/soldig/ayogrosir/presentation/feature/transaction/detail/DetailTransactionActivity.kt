package com.soldig.ayogrosir.presentation.feature.transaction.detail

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.dantsu.escposprinter.EscPosPrinter
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.dantsu.escposprinter.textparser.PrinterTextParserImg
import com.google.android.material.button.MaterialButton
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.DetailTransactionActivityBinding
import com.soldig.ayogrosir.model.TransactionDetail
import com.soldig.ayogrosir.model.TransactionStatus
import com.soldig.ayogrosir.presentation.feature.transaction.detail.inputpassword.InputPasswordFragment
import com.soldig.ayogrosir.utils.ext.*
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions

@AndroidEntryPoint
class DetailTransactionActivity : BaseLoggedInActivity() , InputPasswordFragment.OnInputPasswordDone{
    private lateinit var binding: DetailTransactionActivityBinding
    companion object {
        const val TRANSACTION_ID_KEY = "com.soldig.amanahmart.presentation.feature.transaction.detail.transaction_id_key"
        const val TYPE_KEY = "com.soldig.amanahmart.presentation.feature.transaction.detail.transaction_type_key"
        private const val BLUETOOTH_AND_FINE_LOCATION = 50
    }
    private val viewModel : DetailTransactionViewModel by viewModels()
    private val transactionId : String by lazy{
        intent.extras?.getString(TRANSACTION_ID_KEY) ?: throw  Exception("There should be transaction include")
    }

    private val transactionType : DetailType by lazy {
        intent.extras?.getSerializable(TYPE_KEY) as DetailType? ?: DetailType.Transaction
    }
    private val productAdapter = adapterOf<ArrayList<String>> {
        register(
            viewHolder =  ::WithTextViewHolder,
            layoutResource = R.layout.detail_transaction_product_item,
            onBindViewHolder = { vh, _, list ->
                vh.bind(list)
            }
        )
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DetailTransactionActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
        viewModel.getTransactionDetail(currentPOS,transactionId)
    }
    private fun initView() {
        initByType()
        initToolbar("TR0001", binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initRV()
    }
    private fun initByType() {
        with(binding){
            containerOrderType.toggleVisibility(transactionType == DetailType.Order)
            btnCancelTransaction.toggleVisibility(transactionType == DetailType.Transaction)
            btnPrintStruck.toggleVisibility(transactionType == DetailType.Transaction)
            btnSend.toggleVisibility(transactionType == DetailType.Order)
        }
    }
    private fun initRV() {
        with(binding.rvProductList){
            adapter = productAdapter
            layoutManager = LinearLayoutManager(this@DetailTransactionActivity)
        }
    }
    private fun observeVM() {
        viewModel.viewState.observe(this){ viewState ->
            populateTransaction(viewState.transactionDetail)
            with(binding){
                loading.root.toggleVisibility(viewState.isFetching)
                rvProductList.toggleVisibility(viewState.transactionProductList.isNotEmpty())
                tvEmptyList.toggleVisibility(viewState.transactionProductList.isEmpty())
                btnCancelTransaction.setOnClickListener {
                    launchConfirmationDialog(
                        message = "Apakah anda yakin akan melakukan cancel pada transaksi ini?",
                        onPositiveButton = {
                            launchPasswordDialogFragment()
                            it.dismiss()
                        },
                        onNegativeButton = {
                            it.dismiss()
                        }
                    )
                }
                btnPrintStruck.setOnClickListener {
                    val dialog = MaterialDialog(this@DetailTransactionActivity)
                        .customView(R.layout.dialog_confirmation, scrollable = true)
                        .cornerRadius(16F)
                    val customView = dialog.getCustomView()
                    customView.findViewById<MaterialButton>(R.id.btn_yes).setOnClickListener {
                        onPrint(viewState.printedString)
                        dialog.dismiss()
                    }
                    customView.findViewById<MaterialButton>(R.id.btn_no).setOnClickListener {
                        dialog.dismiss()
                    }
                    dialog.show()
                }
            }
            productAdapter.submitList(viewState.transactionProductList)
            if(viewState.isTransactionCanceled){
                this.onBackPressed()
            }
        }
    }
    private fun launchPasswordDialogFragment() {
        InputPasswordFragment().apply {
            setListener(this@DetailTransactionActivity)
            show(supportFragmentManager, "inputPasswordFragment")
        }
    }
    @SuppressLint("SetTextI18n")
    private fun populateTransaction(transactionDetail: TransactionDetail?) {
        transactionDetail ?: return
        with(binding){
//            btnCancelTransaction.toggleVisibility(transactionDetail.statusType != TransactionStatus.DIBATALKAN)
            btnCancelTransaction.toggleVisibility(transactionDetail.statusType == TransactionStatus.DIBATALKAN)
            supportActionBar?.title = transactionDetail.orderNumber
            tvStatus.text = transactionDetail.status.capitilizeFirstWord()
            tvStatus.setTextColor(transactionDetail.statusType.getStatusTextColor())
            tvId.text = "Order-ID\n${transactionDetail.orderNumber}"
            tvPaywith.text = "Metode Pembayaran\n${transactionDetail.paymentWith}"
            tvDate.text = "Tanggal\n${transactionDetail.date}"
            tvQty.text = "Total Barang\n${transactionDetail.totalItem}"
            tvTotalDiscount.text = "Total Diskon\n${transactionDetail.totalDiscount.toMoneyFormat()}"
            tvSubTotal.text = "Sub Total\n${transactionDetail.totalBeforeDiscount.toMoneyFormat()}"
            tvTotalPrice.text = "Total Harga\n${transactionDetail.totalAfterDiscount.toMoneyFormat()}"
            tvTotalPaid.text = "Total Bayar\n${transactionDetail.totalPaid.toMoneyFormat()}"
            tvTotalChange.text = "Total Kembalian\n${transactionDetail.totalChange.toMoneyFormat()}"
            tvName.text = "Nama\n${transactionDetail.buyerName ?: "Tidak Diketahui"}"
            tvAddress.text = "Alamat\n${transactionDetail.buyerAddress ?: "Tidak Diketahui"}"
            tvPhoneNumber.text = "Telepon\n${transactionDetail.buyerPhoneNumber ?: "Tidak Diketahui"}"
        }
    }
    @AfterPermissionGranted(BLUETOOTH_AND_FINE_LOCATION)
    private fun onPrint(printedString: String) {
        println("After pemission granted")
        if (EasyPermissions.hasPermissions(
                this,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            doPrint(printedString)
        } else {
            println("Tidak punya permission")
            EasyPermissions.requestPermissions(
                this,
                "Blueetoth dan Loakasi",
                BLUETOOTH_AND_FINE_LOCATION,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun doPrint(printedString : String) {
        try{
            println("Printing")
            val printer = EscPosPrinter(BluetoothPrintersConnections.selectFirstPaired(), 203, 48f, 32)
            printer
                .printFormattedText("[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, this.getApplicationContext().getResources().getDrawableForDensity(R.drawable.ic_logo_kawanmart, DisplayMetrics.DENSITY_MEDIUM))+"</img>\n" + printedString)
        }catch ( exception : Exception){
            launchInformationDialog(
                message = "Cek Koneksi dengan printer lalu coba lakukan print lagi",
                title = "Gagal melakukan print"
            )
        }
    }
    enum class DetailType{
        Order,
        Transaction
    }

    override fun onInputPasswordDone() {
        viewModel.cancelTransaction(currentPOS.token,transactionId)
    }
}