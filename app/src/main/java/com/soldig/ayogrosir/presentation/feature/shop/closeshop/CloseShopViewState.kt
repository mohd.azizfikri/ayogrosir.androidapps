package com.soldig.ayogrosir.presentation.feature.shop.closeshop

import com.soldig.ayogrosir.model.Report
import java.util.*

class CloseShopViewState(
    var isFetching: Boolean = false,
    var endDate: Date = Date(),
    var report: Report = Report(),
    var isClosed: Boolean = false,
    var printedString: String = "",
){



}