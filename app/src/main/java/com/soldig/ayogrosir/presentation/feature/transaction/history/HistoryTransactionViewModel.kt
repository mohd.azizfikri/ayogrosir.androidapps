package com.soldig.ayogrosir.presentation.feature.transaction.history

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.transaction.TransactionInteractor
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.TransactionHistory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class HistoryTransactionViewModel
@Inject constructor(
    private val transactionInteractor: TransactionInteractor
) : BaseViewModel<HistoryTransactionViewState>(){



    override fun initNewViewState(): HistoryTransactionViewState {
        return HistoryTransactionViewState()
    }


    fun fetch(pos : POS){
        val viewState = getCurrentViewStateOrNew()
        viewModelScope.launch(IO){
            transactionInteractor.getTransactionHistory.fetch(
                pos,viewState.startDateFilter, viewState.endDateFilter).collect {
                onCollect(
                    response = it,
                    onLoading = { isLoading ->
                        toogleLoading(isLoading)
                    },
                    executeOnSuccess = { successResult ->
                        setCurrentList(successResult.data)
                    }
                )
            }
        }
    }

    private fun setCurrentList(data: List<TransactionHistory>) {
        setViewState(getCurrentViewStateOrNew().apply {
            historyList = data
        })
    }

    private fun toogleLoading(loading: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            isFetching = loading
        })
    }

    fun setStartDate(date:String){
        setViewState(getCurrentViewStateOrNew().apply {
            startDateFilter = changeDateFormat(date)
        })
    }

    fun setEndDate(date: String){
        setViewState(getCurrentViewStateOrNew().apply {
            endDateFilter = changeDateFormat(date)
        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun changeDateFormat(date: String): Date? {
        val format = "dd/MM/yyyy HH:mm"
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.parse(date)
    }
}