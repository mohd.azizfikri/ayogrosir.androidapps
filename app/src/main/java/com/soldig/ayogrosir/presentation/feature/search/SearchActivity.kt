package com.soldig.ayogrosir.presentation.feature.search

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.core.view.isEmpty
import androidx.core.widget.addTextChangedListener
import com.konaire.numerickeyboard.util.suppressSoftKeyboard
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.SearchActivityBinding
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchActivity : BaseLoggedInActivity() {
    private lateinit var binding: SearchActivityBinding

    private val viewModel : SearchViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SearchActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
    }

    private fun observeVM() {
        viewModel.viewState.observe(this){
            with(binding){
                if(rvListProduct.isEmpty()) {
                    tvEmptyList.visibility = View.VISIBLE
                    tvEmptyList.text = "Produk dengan kata kunci '${it.nameKey}' tidak ditemukan"
                }
                tvEmptyList.visibility = View.VISIBLE
                progresbar.toggleVisibility(it.isFetching && it.mode != SearchViewState.Mode.Keyboard)
                rvListProduct.toggleVisibility(!it.isFetching && it.productList.isNotEmpty() && it.mode != SearchViewState.Mode.Keyboard)

                tvBackToSearchResult.text = "Kembali ke hasil pencarian \"${it.lastNotEmptySearchKey}\""
                btnSearch.isEnabled = !it.isFetching
                btnSearch.setStrokeColorResource(if(it.isFetching)R.color.disable_btn else R.color.primary)
                tvEmptyList.toggleVisibility(!it.isFetching && it.isEmpty && it.mode != SearchViewState.Mode.Keyboard )
                println("Adalah ${!it.isFetching}  ${!it.isEmpty} && ${it.mode == SearchViewState.Mode.Keyboard}")
                tvBackToSearchResult.toggleVisibility(!it.isFetching && it.productList.isNotEmpty() && it.mode == SearchViewState.Mode.Keyboard)
                keyboard.toggleVisibility(it.mode == SearchViewState.Mode.Keyboard)
            }
        }
    }

    private fun initView() {
        initToolbar("Cari Barang", binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initEtListener()
        initClicklListener()
    }

    private fun initClicklListener() {
        binding.btnSearch.setOnClickListener {
            val intent = Intent().apply {
                putExtra(SearchContract.CONTRACT_RETURN_KEY, viewModel.getCurrentViewStateOrNew().nameKey)
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        binding.tvBackToSearchResult.setOnClickListener {
            viewModel.setCurrentMode(SearchViewState.Mode.List)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initEtListener() {
        with(binding.etSearch){
            addTextChangedListener {
                viewModel.setSearchKey(it.toString())
            }
            setOnTouchListener { _, _ ->
                viewModel.setCurrentMode(SearchViewState.Mode.Keyboard)
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                suppressSoftKeyboard()
                false
            }
        }
        binding.keyboard.setEditedEditTextByKeboard(binding.etSearch)
    }


}