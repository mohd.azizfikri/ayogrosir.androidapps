package com.soldig.ayogrosir.presentation.feature.productdetail.changediscount

import android.os.Parcelable
import com.soldig.ayogrosir.model.DiscountType
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChangeDiscountViewState(
    var discountType: DiscountType = DiscountType.Nothing,
    var percentageValue: Double = 0.0,
    var nominalValue: Double = 0.0,
) : Parcelable {

    fun getChoosenTypeValue() : Double{
        return when(discountType){
            DiscountType.Nominal -> nominalValue
            DiscountType.Nothing -> 0.0
            DiscountType.Percentage -> percentageValue
        }
    }

}