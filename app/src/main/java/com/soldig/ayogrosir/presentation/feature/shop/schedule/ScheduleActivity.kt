package com.soldig.ayogrosir.presentation.feature.shop.schedule

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.DialogFragment
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.ScheduleActivityBinding
import com.soldig.ayogrosir.model.ParcelableSchedule
import com.soldig.ayogrosir.model.Schedule
import com.soldig.ayogrosir.utils.TimePickerFragment
import com.soldig.ayogrosir.utils.ext.*
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ScheduleActivity : BaseLoggedInActivity(),
    TimePickerFragment.OnTimePicker {


    companion object {
        const val SCHEDULE_DATA_INTENT = "com.soldig.amanahmart.presentation.feature.shop.schedule.schedule_data"
    }

    private lateinit var binding: ScheduleActivityBinding

    private lateinit var schedule : Schedule


    private val viewModel by viewModels<ScheduleViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ScheduleActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        getSchedule()
        initView()
        observeVM()
        initSchedule()
    }
    private fun initView() {
        initToolbar(schedule.day, binding.toolbar)
        initTime(toolbar = binding.toolbar)
        initListener()
    }
    private fun initSchedule() {
        viewModel.setCurrentSchedule(schedule)
    }
    private fun observeVM() {
        viewModel.viewState.observe(this){
            println("Start time dan End Time adalah ${it.startTime} dan ${it.endTime}")
            with(binding){
                toogle.apply {
                    isChecked = it.isOpen
                }
                tvStatus.text = if(it.isOpen)  "Buka" else "Tutup"
                toogle.isChecked = it.isOpen
                tvOpenHour.text = it.startTime
                tvCloseHour.text = it.endTime
                loading.root.toggleVisibility(it.isFetching)
                if(it.isActionToogle.get() == true) handleToogle(it)
            }

        }
        viewModel.userInteraction.observe(this){
            val uiInteraction = it.get() ?: return@observe
            if(uiInteraction !is UIInteraction.DoNothing){
                binding.root.showLongSnackbar(uiInteraction.getMessage(this))
            }
        }
    }

    private fun handleToogle(viewState: ScheduleViewState) {
        val isWantToBeOpen = viewState.isOpen
        if(isWantToBeOpen){
            if(viewState.isValidToOpen()){
                if(viewState.isTimeValid()){
                    launchConfirmationDialog(
                        message = "${getString(R.string.schedule_open_confirmation)} ${schedule.day}?",
                        onPositiveButton = {
                            viewModel.openToko(currentPOS.token)
                            it.dismiss()
                        },
                        onNegativeButton = {
                            viewModel.setIsOpen(!viewState.isOpen,false)
                            it.dismiss()
                        }
                    )
                }else{
                    launchInformationDialog(
                        message = getString(R.string.schedule_invalid_input),
                        onPositiveButton = {
                            viewModel.setIsOpen(!viewState.isOpen,false)
                            it.dismiss()
                        }
                    )
                }

            }else{
                launchInformationDialog(
                    message = "${getString(R.string.schedule_invalid_time)}!",
                    onPositiveButton = {
                        viewModel.setIsOpen(!viewState.isOpen,false)
                        it.dismiss()
                    }
                )
            }
        }else{
            launchConfirmationDialog(
                message = "${getString(R.string.schedule_close_message)} ${schedule.day}?",
                onPositiveButton = {
                    viewModel.closeToko(currentPOS.token)
                    it.dismiss()
                },
                onNegativeButton = {
                    viewModel.setIsOpen(!viewState.isOpen,false)
                    it.dismiss()
                }
            )
        }
    }
    private fun initListener() {
        with(binding){
            tvOpenHour.setOnClickListener {
                val instance: DialogFragment = TimePickerFragment.newInstance(type = TimePickerFragment.OPEN)
                instance.show(supportFragmentManager, "timePicker")
            }

            tvCloseHour.setOnClickListener {
                val newFragment: DialogFragment = TimePickerFragment.newInstance(type = TimePickerFragment.CLOSE)
                newFragment.show(supportFragmentManager, "datePicker")
            }
            toogle.setOnClickListener {
                viewModel.setIsOpen(toogle.isChecked, true)
            }
        }
    }
    private fun getSchedule() {
        (intent.extras?.get(SCHEDULE_DATA_INTENT) as ParcelableSchedule?)?.let { it ->
            schedule = it.mapToSchedule()
        } ?: throw Exception("No Schedule Given")
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSelect(date: String, time: String, type: Int) {
        if (type == TimePickerFragment.OPEN) {
            viewModel.setStartTime(time)
        } else {
            viewModel.setEndTime(time)
        }
    }
}