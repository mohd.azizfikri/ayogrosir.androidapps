package com.soldig.ayogrosir.presentation.feature.shop.setting

import android.os.Parcelable
import com.soldig.ayogrosir.model.SettingModel
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class SettingViewState(
    var isFetching: Boolean = false,
    var isUpdatingDeliveryStatus: Boolean = false,
): Parcelable{

    @IgnoredOnParcel
    var settingModel: SettingModel = SettingModel(false , listOf())




}

