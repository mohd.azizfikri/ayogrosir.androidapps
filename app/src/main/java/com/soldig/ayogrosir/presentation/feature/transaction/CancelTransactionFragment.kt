package com.soldig.ayogrosir.presentation.feature.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.soldig.ayogrosir.databinding.TransactionCancelFragmentBinding

class CancelTransactionFragment : Fragment() {
    private var _binding: TransactionCancelFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        _binding = TransactionCancelFragmentBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.btnCancel.setOnClickListener {
            activity?.onBackPressed()
        }
        binding.btnSubmit.setOnClickListener {
            activity?.onBackPressed()
        }
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}