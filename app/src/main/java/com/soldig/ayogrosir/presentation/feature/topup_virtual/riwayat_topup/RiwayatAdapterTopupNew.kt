package com.soldig.ayogrosir.presentation.feature.topup_virtual.riwayat_topup

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.databinding.ItemRiwayatTopupBinding
import com.soldig.ayogrosir.model.DataItemRiawayat
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RiwayatAdapterTopupNew : RecyclerView.Adapter<RiwayatAdapterTopupNew.IViewHolder>(), Filterable {
    private var models: MutableList<DataItemRiawayat> = ArrayList()
    var modelList: MutableList<DataItemRiawayat>

    init {
        modelList = models
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IViewHolder {
        return IViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_riwayat_topup,
                parent,
                false
            )
        )
    }

    override fun getFilter(): Filter {

        return object : Filter() {
            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                modelList = p1!!.values as MutableList<DataItemRiawayat>
                notifyDataSetChanged()
            }

            override fun performFiltering(p0: CharSequence?): FilterResults {
                val charString: String = p0.toString()
                modelList = if (charString.isEmpty()) {
                    models
                } else {
                    val filteredList: MutableList<DataItemRiawayat> = mutableListOf()
                    for (s: DataItemRiawayat in models) {
                        when {
                            s.kodeStatusPengajuan!!.toLowerCase().contains(charString.toLowerCase()) -> filteredList.add(s)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = modelList
                return filterResults
            }
        }
    }

    override fun getItemCount(): Int = modelList.size

    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        if (modelList.size > 0) {
            val item = modelList[position]
            holder.bindModel(item)
        }
    }

    fun setData(data: MutableList<DataItemRiawayat>) {
        if (models.size >= 0) {
            models.clear()
        }
        models.addAll(data)
        notifyDataSetChanged()
    }


    inner class IViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemRiwayatTopupBinding.bind(itemView);

        fun bindModel(model: DataItemRiawayat) {
            with(binding) {

                Glide
                    .with(root)
                    .load(model.buktiTransfer)
                    .into(binding.imgBukti)
                binding.tvOrderNumber.text
                binding.tvDate.text = model.tglTopup
                binding.tvJumlah.text = "Rp. " + currencyBanyak(model.jmlTopup.toString())
                if (model?.kodeStatusPengajuan.equals("0")){
                    binding.tvStatus.setTextColor(Color.parseColor("#FFAA00"))
                } else if(model?.kodeStatusPengajuan.equals("1")) {
                    binding.tvStatus.setTextColor(Color.parseColor("#076AE1"))
                } else {
                    binding.tvStatus.setTextColor(Color.parseColor("#FF0000"));
                }
                binding.tvStatus.text = model.statusPengajuan
                binding.tvKeterangan.text = model.alasanDitolak
                binding.tvOrderNumber.text = model.no

//                binding.root.setOnClickListener {
//                    onRiwayatClicked(this)
//                }
            }
        }
    }

    fun currencyBanyak(s: String): String = DecimalFormat("###,###,###,###,###")
    .format(s.toDouble()).replace(",", ".")


    fun filterDateRange(charText: String, charText1: String) {
        val sdf1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var convertedDate: Date? = null
        convertedDate = sdf1.parse(charText)

        val sdf2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var convertedDate2: Date? = null
        convertedDate2 = sdf2.parse(charText1)
//        models.clear()
        if (charText.equals("") || charText.equals(null)) {
            models.addAll(modelList)
        } else {
            println("modelnya : $models")
            println("modelistnya : $modelList")
            for (wp in modelList.toMutableList()) {
                try {
                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val strDate: Date = sdf.parse(wp.tglTopup)
                    println("masuk sini gak?" + strDate)
                    if (convertedDate2.after(strDate) && convertedDate.before(strDate)) {
                        models.addAll(listOf(wp))
                        notifyDataSetChanged()
                        println("data filter $wp")
                    } else {
                    }
                } catch (e: ParseException) {
                    println("masuk catch $e")
                    e.printStackTrace()
                }
            }
        }
    }

}
