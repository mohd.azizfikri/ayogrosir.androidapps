package com.soldig.ayogrosir.presentation.feature.topup_virtual.riwayat_topup

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.topup_virtual.RiwayatTopupInteractor
import com.soldig.ayogrosir.model.ResponseRiwayatTopup
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class RiwayatTopupViewModel
    @Inject constructor(
    private val riwayatTopupInteractor: RiwayatTopupInteractor,
) : BaseViewModel<RiwayatTopupViewState>() {

    override fun initNewViewState(): RiwayatTopupViewState {
        return RiwayatTopupViewState()
    }


    fun getRiwayatTopup(token: String, tgl1: String,tgl2: String,status: String) {
        viewModelScope.launch(Dispatchers.IO){
            riwayatTopupInteractor.getRiwayatTopupUser.fetch(token, tgl1, tgl2, status).collect { response ->
                onCollect(
                    response = response,
                    onLoading = {
                        toogleLoading(it)
                    },
                    executeOnSuccess = {
                        setList(it.data)
                    }
                )
            }
        }
    }


    private fun setList(list : ResponseRiwayatTopup){
        setViewState(getCurrentViewStateOrNew().apply {
            this.responseRiwayatTopup = list
        })
    }
    private fun toogleLoading(loading: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            isFetching = loading
        })
    }

    fun setStartDate(date:String){
        setViewState(getCurrentViewStateOrNew().apply {
            startDateFilter = changeDateFormat(date)
        })
    }

    fun setEndDate(date: String){
        setViewState(getCurrentViewStateOrNew().apply {
            endDateFilter = changeDateFormat(date)
        })
    }

    fun setStatus(status:String){
        setViewState(getCurrentViewStateOrNew().apply {
            statusState = status
        })
    }


    @SuppressLint("SimpleDateFormat")
    private fun changeDateFormat(date: String): Date? {
        val format = "dd/MM/yyyy HH:mm"
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.parse(date)
    }
}