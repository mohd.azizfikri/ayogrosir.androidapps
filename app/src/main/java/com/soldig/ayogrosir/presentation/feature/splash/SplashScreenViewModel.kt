package com.soldig.ayogrosir.presentation.feature.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.splash.SplashSyncInteractor
import com.soldig.ayogrosir.model.POS
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class SplashScreenViewModel
    @Inject constructor(
        private val syncWithServer: SplashSyncInteractor
    ): ViewModel() {



    val isSyncDataDone : LiveData<Boolean>
        get() = _isSyncDataDone

    private val _isSyncDataDone = MutableLiveData(false)

    fun checkSync(
        pos: POS
    ) {
        viewModelScope.launch {
            syncWithServer.fetchFromDB(pos).collect {
               when(it){
                   is Resource.Success -> {
                       _isSyncDataDone.value = true
                   }
                   is Resource.Error -> {
                       _isSyncDataDone.value = true
                   }
                   else -> {}
               }
            }
        }
    }

}