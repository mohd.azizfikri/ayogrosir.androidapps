package com.soldig.ayogrosir.presentation.commonlist.productlist

import android.content.res.ColorStateList
import android.view.View
import com.bumptech.glide.Glide
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.common.toMoneyFormat
import com.soldig.ayogrosir.databinding.ProductGridItemBinding
import com.soldig.ayogrosir.databinding.ProductListItemBinding
import com.soldig.ayogrosir.model.Product
import com.soldig.ayogrosir.presentation.feature.home.HomeViewState.ProductSection.*
import com.soldig.ayogrosir.utils.Constant
import com.soldig.ayogrosir.utils.ext.goGone
import com.soldig.ayogrosir.utils.ext.toggleVisibility
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class ProductGridViewHolder(view: View) : RecyclerViewHolder<ProductListViewType.Grid>(view) {

    private val itemBinding: ProductGridItemBinding = ProductGridItemBinding.bind(view)

    fun bind(productListItem: ProductListItem.POSProductListItem, onProductClicked: (product:  ProductListItem.POSProductListItem) -> Unit) {
        val product = productListItem.posProduct
        itemBinding.tvTitle.text = product.title
        itemBinding.tvPrice.text = product.sellAfterDiscountPrice.toMoneyFormat(Constant.PREFIX_RP)
        Glide.with(itemView.context)
            .load(product.getImageURL())
            .into(itemBinding.imgProduct)
        itemBinding.layerEmpty.toggleVisibility(product.qty <= 0 || !product.isActive)
        if(!product.isActive){
            itemBinding.tvKeterangan.text = "Tidak Aktif"
        }else{
            itemBinding.tvKeterangan.text = "Habis"
        }
        itemView.setOnClickListener {
            if (product.qty == 0 || !product.isActive) {
                return@setOnClickListener
            }
            onProductClicked(productListItem)
        }
    }


}


class ProductListViewHolder(view: View) : RecyclerViewHolder<ProductListHorizontalItemType>(view) {

    val itemBinding: ProductListItemBinding = ProductListItemBinding.bind(view)

    fun bind(productListItem: ProductListItem, onCartClicked: (product: ProductListItem.POSProductListItem)  -> Unit, onProductClicked: (product: ProductListItem) -> Unit,) {
        val product = productListItem.product
        itemBinding.tvTitle.text = product.title
        itemBinding.tvPriceExist.text = product.sellAfterDiscountPrice.toMoneyFormat(Constant.PREFIX_RP)
        itemBinding.tvPriceNotExist.text = product.sellAfterDiscountPrice.toMoneyFormat(Constant.PREFIX_RP)
        itemBinding.tvInvisiblePrice.text = product.sellAfterDiscountPrice.toMoneyFormat(Constant.PREFIX_RP)
        Glide.with(itemView.context)
            .load(product.getImageURL())
            .into(itemBinding.imgProduct)
        itemBinding.layerEmpty.goGone()
        itemBinding.tvPriceNotExist.toggleVisibility(product is Product.NotPOSProduct)
        itemBinding.tvPriceExist.toggleVisibility(product is Product.POSProduct)
        itemBinding.icCart.toggleVisibility(product is Product.POSProduct && product.isActive)
        with(itemBinding.icCart){
            imageTintList = ColorStateList.valueOf(itemBinding.root.context.getColor(if(product.qty > 0) R.color.primary else R.color.disable_btn))
        }
        itemBinding.icCart.setOnClickListener {
            if(productListItem is ProductListItem.POSProductListItem && product.qty > 0){
                onCartClicked(productListItem)
            }
        }
        itemView.setOnClickListener {
            onProductClicked(productListItem)
        }
    }

}


data class ProductListHorizontalItemType(val productListItem: ProductListItem)

class ProductGridEmptyViewHolder(view : View) : RecyclerViewHolder<ProductListViewType.GridEmpty>(view) {

}
