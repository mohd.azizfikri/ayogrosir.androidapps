package com.soldig.ayogrosir.presentation.feature.virutal.history

import android.view.View
import com.soldig.ayogrosir.databinding.ItemVirtualProductHistoryBinding
import com.soldig.ayogrosir.model.VirtualProductHistory
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class VirtualProductHistoryViewHolder(view: View) : RecyclerViewHolder<VirtualProductHistory>(view){

    val binding = ItemVirtualProductHistoryBinding.bind(view);


    fun bind(history : VirtualProductHistory){
        with(binding){
            tvDate.text = history.date
            tvOrderNumber.text = history.orderNumber
            tvTotalPrice.text = history.total
            tvType.text = history.jenis
            tvFeeAplikasi.text = history.feeAdmin
            tvFeeWarung.text = history.feeWarung
        }
    }

}