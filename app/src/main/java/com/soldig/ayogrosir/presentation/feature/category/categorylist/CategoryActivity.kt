package com.soldig.ayogrosir.presentation.feature.category.categorylist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.soldig.ayogrosir.base.BaseLoggedInActivity
import com.soldig.ayogrosir.databinding.CategoryActivityBinding
import com.soldig.ayogrosir.presentation.feature.category.categorylist.adapter.CategoryAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class  CategoryActivity : BaseLoggedInActivity() {
    private lateinit var binding: CategoryActivityBinding

    private val viewModel : CategoryViewModel by viewModels()

    companion object {
        const val CATEGORY_CONTRACT_KEY = "com.soldig.amanahmart.presentation.feature.category.categorylist.CATEGORY_CONTRACT_KEY"
    }

    private val categoryAdapter = CategoryAdapter(listOf()){
        val intent = Intent().apply {
            putExtra(CATEGORY_CONTRACT_KEY , it)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CategoryActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()
        observeVM()
        viewModel.getAllCategory(currentPOS.token)

    }

    private fun observeVM() {
        viewModel.viewState.observe(this){
            categoryAdapter.changeList(it.categoryList)
        }
    }

    private fun initView() {
        initToolbar("Pilih Kategori", binding.toolbar)
        initTime(toolbar = binding.toolbar)
        binding.rvListCategory.apply {
            adapter = categoryAdapter
        }
    }
}