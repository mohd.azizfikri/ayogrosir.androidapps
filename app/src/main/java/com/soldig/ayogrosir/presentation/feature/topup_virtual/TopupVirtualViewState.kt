package com.soldig.ayogrosir.presentation.feature.topup_virtual

import android.os.Parcelable
import com.soldig.ayogrosir.model.ResponseTopup
import com.soldig.ayogrosir.utils.other.SingleEvent
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class TopupVirtualViewState(
    private var _pos: ResponseTopup? = null,
    var isLoading: Boolean = false,
) : Parcelable {

    @IgnoredOnParcel
    var responDataTopup : SingleEvent<ResponseTopup>? = null
        set(value) {
            println("data topup $value")
            _pos = value?.peek()
            println("data respon $_pos")
            field = value
        }

}