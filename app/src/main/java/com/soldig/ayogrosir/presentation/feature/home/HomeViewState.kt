package com.soldig.ayogrosir.presentation.feature.home

import android.os.Parcelable
import com.soldig.ayogrosir.interactors.DataSource
import com.soldig.ayogrosir.model.*
import com.soldig.ayogrosir.presentation.commonlist.productlist.ProductListViewType
import com.soldig.ayogrosir.presentation.commonlist.productlist.ProductListHorizontalItemType
import com.soldig.ayogrosir.utils.ext.filterThenMapIndexed
import com.soldig.ayogrosir.utils.other.SingleEvent
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class HomeViewState(
    val productSection: ProductSection = ProductSection(),
    var cartSection: CartSection = CartSection(),
    ) : Parcelable {

    fun isProductExistInCart(productId: String): Boolean {
        return cartSection.cart.orderList.values.firstOrNull { order ->
            order.product.posProduct.productId == productId
        } != null
    }
    fun isCartEmpty(): Boolean {
        return cartSection.cart.orderList.isEmpty()
    }
    fun minCart(order: Order, cart: Cart ) {
        order.quantity = cart.totalItems
    }
    @IgnoredOnParcel
    val checkoutSection: CheckoutSection = CheckoutSection(false, null)

    @Parcelize
    data class ProductSection(
        var isFetching: Boolean = false,
        var currentKumpulanFilterName: String? = null,
        var currentKumpulanFilterCategory: ProductCategory? = null,
        var currentGridFilterName: String? = null,
        var currentProductViewType: ProductViewType = ProductViewType.Pilihan,
    ) : Parcelable {

        sealed class ProductListItem(
            var product: Product,
            var gridIndex: Int = -1,
            var listIndex: Int = -1
        ) {
            class POSProductListItem(
                var posProduct: Product.POSProduct,
                _gridIndex: Int,
                _listIndex: Int
            ) : ProductListItem(posProduct, _gridIndex, _listIndex)
            class JustProductListItem(_product: Product, _gridIndex: Int, _listIndex: Int) :
                ProductListItem(_product, _gridIndex, _listIndex)
        }
        @IgnoredOnParcel
        var shouldGoToDetail: SingleEvent<Product>? = null

        @IgnoredOnParcel
        var isKumpuluanFilteredRecently: SingleEvent<Boolean>? = null

        @IgnoredOnParcel
        var isGridFilteredRecently: SingleEvent<Boolean>? = null

        @IgnoredOnParcel
        var isNewList: SingleEvent<Boolean>? = null

        @IgnoredOnParcel
        var currentlyUpdatedGridIndex: SingleEvent<Int>? = null

        @IgnoredOnParcel
        var currentlyUpdatedListIndex: SingleEvent<Int>? = null


        @IgnoredOnParcel
        var allProductList = listOf<ProductListHorizontalItemType>()

        @IgnoredOnParcel
        var pilihanProductList = listOf<ProductListViewType>()


        fun getGridFilteredProduct(): List<ProductListViewType> {
            val filter = currentGridFilterName ?: return pilihanProductList
            return pilihanProductList.filterThenMapIndexed(
                predicate = { index, productListViewType ->
                    productListViewType is ProductListViewType.Grid && productListViewType.product.posProduct.title.lowercase(
                        Locale.getDefault()
                    ).contains(filter)
                },
                transform = { index, predicateIndex, productListViewType ->
                    if (productListViewType is ProductListViewType.Grid) {
                        productListViewType.apply {
                            product.apply {
                                val productReal =this.posProduct
                                println("Title : ${productReal.title}  ${productReal.qty}")
                                gridIndex = predicateIndex
                            }
                        }
                    } else {
                        productListViewType
                    }
                }
            )
        }

        fun getKumpulanFilteredProduct(): List<ProductListHorizontalItemType> {
            var filter: ((index: Int,product: ProductListHorizontalItemType) -> Boolean)? = null
            if (currentKumpulanFilterName == null && currentKumpulanFilterCategory == null) {
                println("Return semua product")
                return allProductList
            } else if (currentKumpulanFilterName != null && currentKumpulanFilterCategory != null) {
                println("Return berdasarkan filter nama dan kategori")
                filter = { index, it ->
                    val product = it.productListItem.product;
                    product.title.lowercase(Locale.getDefault())
                        .contains(currentKumpulanFilterName!!) && product.categoryId == currentKumpulanFilterCategory!!.id
                }
            } else if (currentKumpulanFilterName != null) {
                println("Return berdasarkan filter nama ")
                filter = { index, it ->

                    it.productListItem.product.title.lowercase(Locale.getDefault())
                        .contains(currentKumpulanFilterName!!)
                }
            } else {
                println("Return berdasarkan filter kategori ")
                filter = { index, it ->
                    it.productListItem.product.categoryId == currentKumpulanFilterCategory!!.id
                }
            }
            return allProductList.filterThenMapIndexed(
                predicate =  filter,
                transform = { index , filteredIndex, item ->
                    item.apply {
                        this.productListItem.listIndex = filteredIndex
                    }
                }
            )
        }

        @IgnoredOnParcel
        var dataSource: SingleEvent<DataSource>? = null

        enum class ProductViewType {
            Pilihan,
            Kumpulan
        }
    }

    @Parcelize
    data class CartSection(
        var cart: Cart = Cart()
    ) : Parcelable {
    }

    data class CheckoutSection(
        var isFetching: Boolean,
        var transactionResult: SingleEvent<TransactionDetail>? = null
    )
}