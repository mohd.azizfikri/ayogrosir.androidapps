package com.soldig.ayogrosir.presentation.feature.search

import android.os.Parcelable
import com.soldig.ayogrosir.presentation.commonlist.productlist.ProductListHorizontalItemType
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize


@Parcelize
data class SearchViewState(
    var isFetching: Boolean,
    var mode: Mode = Mode.Keyboard,
    var nameKey: String,
    var isEmpty : Boolean,
    var lastNotEmptySearchKey: String = ""
) : Parcelable {

    enum class Mode{
        Keyboard,
        List
    }

    @IgnoredOnParcel
    var productList : List<ProductListHorizontalItemType> = listOf()

}