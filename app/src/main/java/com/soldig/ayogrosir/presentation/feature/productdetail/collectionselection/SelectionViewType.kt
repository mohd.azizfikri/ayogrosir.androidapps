package com.soldig.ayogrosir.presentation.feature.productdetail.collectionselection

import com.soldig.ayogrosir.model.Product

sealed class SelectionViewType(val itemViewType: Int){

    companion object{
        const val PRODUCT_EXIST = 1
        const val EMPTY = 2

        fun produce32EmptyProduct() : List<SelectionViewType>{
            val newList = ArrayList<SelectionViewType>()
            for(i in 1..32){
                newList.add(Empty)
            }
            return newList
        }
    }

    class ProductExist(val product: Product) : SelectionViewType(PRODUCT_EXIST)

    object Empty : SelectionViewType(EMPTY)


    fun areItemTheSame(selectionViewType: SelectionViewType) : Boolean{
        return when(selectionViewType){
            is ProductExist -> {
                when(this){
                    is ProductExist -> {
                        this.product.productId == selectionViewType.product.productId
                    }

                    else -> {
                        false
                    }
                }
            }

            is Empty -> {
                when(this){
                    is Empty -> {
                        true
                    }

                    else -> {
                        false
                    }
                }
            }

        }
    }


}