package com.soldig.ayogrosir.presentation.feature.shop.schedule

import androidx.lifecycle.viewModelScope
import com.soldig.ayogrosir.base.BaseViewModel
import com.soldig.ayogrosir.interactors.MessageType
import com.soldig.ayogrosir.interactors.schedule.ScheduleInteractor
import com.soldig.ayogrosir.model.Schedule
import com.soldig.ayogrosir.utils.other.SingleEvent
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ScheduleViewModel
@Inject constructor(
    private val scheduleInteractor: ScheduleInteractor
) : BaseViewModel<ScheduleViewState>() {



    fun setIsFetching(isFetching: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            this.isFetching = isFetching
        })
    }

    fun setIsOpen(isOpen : Boolean,isActionToogle: Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            this.isOpen = isOpen
            this.isActionToogle = SingleEvent(isActionToogle)
        })
    }

    fun setStartTime(startTime: String){
        setViewState(getCurrentViewStateOrNew().apply {
            this.startTime = startTime
        })
    }


    fun setEndTime(endTime: String){
        setViewState(getCurrentViewStateOrNew().apply {
            this.endTime = endTime
        })
    }

    fun setCurrentSchedule(schedule: Schedule){
        setViewState(getCurrentViewStateOrNew().apply {
            day = schedule.day
            when(schedule){
                is Schedule.Open -> {
                    val time = schedule.hour.trim().split("-")
                    startTime = time[0]
                    endTime = time[1]
                    isOpen = true
                }
                is Schedule.Close -> {
                    startTime = schedule.hour
                    endTime = schedule.hour
                    isOpen = false
                }
            }

        })
    }

    private fun postSchedule(token:String, schedule: Schedule){
        viewModelScope.launch(IO){
            scheduleInteractor.changeSchedule.fetch(
                token,
                schedule
            ).collect { result ->
                onCollect(
                    result,
                    onLoading = {
                        setIsFetching(it)
                    },
                    executeOnSuccess = {
                        setUserInteraction(UIInteraction.GenericMessage(MessageType.StringMessage(it.data)))
                        setCurrentSchedule(schedule)
                    }
                )
            }
        }

    }

    override fun initNewViewState(): ScheduleViewState {
        return ScheduleViewState()
    }

    fun closeToko(token: String){
        val viewState = getCurrentViewStateOrNew()
        postSchedule(token, schedule =  Schedule.Close(viewState.day))
    }

    fun openToko(token: String) {
        val viewState = getCurrentViewStateOrNew()
        postSchedule(token, schedule =  Schedule.Open(
          viewState.day,
          "${viewState.startTime} - ${viewState.endTime}"
        ))
    }
}