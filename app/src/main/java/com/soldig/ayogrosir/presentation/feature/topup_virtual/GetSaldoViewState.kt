package com.soldig.ayogrosir.presentation.feature.topup_virtual

import com.soldig.ayogrosir.model.ResponseGetSaldo
import com.soldig.ayogrosir.utils.other.SingleEvent
import kotlinx.parcelize.IgnoredOnParcel

data class GetSaldoViewState(
    private var _pos: ResponseGetSaldo? = null,
    var isLoading: Boolean = false,
) {

    @IgnoredOnParcel
    var responGetSaldo : SingleEvent<ResponseGetSaldo>? = null
        set(value) {
            println("data topup $value")
            _pos = value?.peek()
            println("data respon $_pos")
            field = value
        }

}