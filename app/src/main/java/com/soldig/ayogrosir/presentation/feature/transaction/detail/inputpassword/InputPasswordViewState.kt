package com.soldig.ayogrosir.presentation.feature.transaction.detail.inputpassword

import com.soldig.ayogrosir.utils.other.SingleEvent

data class InputPasswordViewState(
    var inputtedPassword: String = "",
    var isInputPasswordSuccess: SingleEvent<Boolean>? = null,
    var isValidatingPassword: Boolean = false
)