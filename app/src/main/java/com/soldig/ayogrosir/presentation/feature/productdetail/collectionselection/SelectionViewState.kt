package com.soldig.ayogrosir.presentation.feature.productdetail.collectionselection


data class SelectionViewState(
    var selectedList : List<SelectionViewType> = listOf(),
    var isFetchingList : Boolean = false,
)