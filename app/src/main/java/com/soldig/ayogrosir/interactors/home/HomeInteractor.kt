package com.soldig.ayogrosir.interactors.home

import com.soldig.ayogrosir.interactors.common.GetAllProduct
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class HomeInteractor
@Inject constructor(
    val getAllProduct: GetAllProduct,
    val getPaymentMethods: GetPaymentMethods,
    val checkout: Checkout
){

}