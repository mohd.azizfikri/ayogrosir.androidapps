package com.soldig.ayogrosir.interactors.closeshop
import com.soldig.ayogrosir.datasource.network.abstraction.ReportNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.Report
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

@ViewModelScoped
class GetReport
@Inject constructor(
    private val reportNetworkDataSource: ReportNetworkDataSource
){
    fun fetch(
        token: String,
        startDate: Date?,
        endDate: Date?
    ) : Flow<Resource<Report>> = networkOnlyCall {
        reportNetworkDataSource.getCloseReport(token)
    }
}