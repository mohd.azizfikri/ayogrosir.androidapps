package com.soldig.ayogrosir.interactors.order

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class  OrderInteractor
@Inject constructor(
    val getAllOrderList: GetAllOrderList,
    val updateOrder: UpdateOrder
){
}