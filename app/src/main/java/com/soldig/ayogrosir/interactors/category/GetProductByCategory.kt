package com.soldig.ayogrosir.interactors.category

import com.soldig.ayogrosir.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.Product
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetProductByCategory @Inject constructor(
    private val productNetworkDataSource: ProductNetworkDataSource
){
    fun fetch(
        pos:POS,
        productId: String
    ) : Flow<Resource<List<Product>>> = networkOnlyCall {
        productNetworkDataSource.getAllProductByCategory(pos, productId)
    }
}