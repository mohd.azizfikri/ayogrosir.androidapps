package com.soldig.ayogrosir.interactors.virtual

import com.soldig.ayogrosir.datasource.network.impl.DigiFlazzNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.VirtualProductHistory
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetHistory @Inject constructor(
    private val digiFlazzNetworkDataSource: DigiFlazzNetworkDataSource
) {

    fun fetch(
        token: String
    ) : Flow<Resource<List<VirtualProductHistory>>> = networkOnlyCall {
        digiFlazzNetworkDataSource.getTransactionHistory(token)
    }

}