package com.soldig.ayogrosir.interactors.transaction

import com.soldig.ayogrosir.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.ayogrosir.interactors.*
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.TransactionHistory
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

@ViewModelScoped
class GetTransactionHistory
@Inject constructor(
    private val transactionNetworkDataSource : TransactionNetworkDataSource,
    private val transactionCacheDataSource: TransactionCacheDataSource
){
    private  val emptyList = "Cannot find any data"



    fun fetch(
        pos: POS,
        startDate: Date? = null,
        endDate: Date? = null
    ): Flow<Resource<List<TransactionHistory>>> = networkFirstCall(
        loadFromDB = {
            transactionCacheDataSource.getAllTransactionHistory(pos.phoneNumber)
        },
        fetch = {
            transactionNetworkDataSource.getAllTransactionHistory(pos.token , startDate, endDate)

        },
        errorSpesificHandler = { serverMessage ->
            if(serverMessage.message == emptyList){
                Resource.Success(listOf(), DataSource.Network)
            }else{
                Resource.Error(UIInteraction.GenericMessage(MessageType.StringMessage(serverMessage.message)))
            }

        }
    )



}