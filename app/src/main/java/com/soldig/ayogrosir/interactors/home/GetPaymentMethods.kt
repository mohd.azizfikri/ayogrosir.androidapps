package com.soldig.ayogrosir.interactors.home

import com.soldig.ayogrosir.datasource.cache.abstraction.PaymentCacheDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.PaymentNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkBoundResource
import com.soldig.ayogrosir.model.PaymentCategory
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetPaymentMethods
@Inject constructor(
    private val paymentNetworkDataSource: PaymentNetworkDataSource,
    private val paymentCacheDataSource: PaymentCacheDataSource
){

    fun fetch(
        token: String
    ) : Flow<Resource<List<PaymentCategory>>> = networkBoundResource(
        loadFromDB =  {
            paymentCacheDataSource.getAllPaymentCategory()
        },
        shouldFetch = {
            true
        },
        saveFetchResult = { fromApi, fromDb ->
            paymentCacheDataSource.insertPaymentCategories(fromApi)
        },
        fetch = {
            paymentNetworkDataSource.getAllPaymentMethod(token)
        }
    )

}