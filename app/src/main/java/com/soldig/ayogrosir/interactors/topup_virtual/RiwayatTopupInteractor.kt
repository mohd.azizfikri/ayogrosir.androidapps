package com.soldig.ayogrosir.interactors.topup_virtual

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class RiwayatTopupInteractor
@Inject constructor(
    val getRiwayatTopupUser: RiwayatTopupUser
){
}