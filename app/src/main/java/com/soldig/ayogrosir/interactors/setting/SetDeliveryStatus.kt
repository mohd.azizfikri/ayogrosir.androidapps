package com.soldig.ayogrosir.interactors.setting

import com.soldig.ayogrosir.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class SetDeliveryStatus
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource
){

    fun fetch(
        token: String,
        isDeliveryAvailable: Boolean
    ) : Flow<Resource<String>> = networkOnlyCall {
        userNetworkDataSource.setDeliveryStatus(token,isDeliveryAvailable)
    }

}