package com.soldig.ayogrosir.interactors.topup_virtual

import com.soldig.ayogrosir.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.ResponseGetSaldo
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetSaldoUser
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource
){
    fun fetch(
        token: String
    ) : Flow<Resource<ResponseGetSaldo>> = networkOnlyCall {
        userNetworkDataSource.getSaldo(token)
    }
}