package com.soldig.ayogrosir.interactors.order


import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.ayogrosir.interactors.DataSource
import com.soldig.ayogrosir.interactors.MessageType
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.TransactionHistory
import com.soldig.ayogrosir.model.TransactionStatus
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

@ViewModelScoped
class GetAllOrderList
@Inject constructor(
    private val transactionNetworkDataSource: TransactionNetworkDataSource
) {

    private  val emptyList = "Cannot find any data"
    fun fetch(
        token: String,
        startDate: Date? = null,
        endDate: Date? = null,
        status: TransactionStatus? = null
    ) : Flow<Resource<List<TransactionHistory>>> = networkOnlyCall(
        errorSpesificHandler = { serverMessage ->
            if(serverMessage.message == emptyList){
                Resource.Success(listOf(), DataSource.Network)
            }else{
                Resource.Error(UIInteraction.GenericMessage(MessageType.ResourceMessage(R.string.login_unkown_error)))
            }
        }
    ){
        transactionNetworkDataSource.getOrderOnlyTransaction(token,startDate,endDate,status)
    }

}