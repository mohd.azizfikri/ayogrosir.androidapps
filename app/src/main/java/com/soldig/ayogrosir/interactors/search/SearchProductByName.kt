package com.soldig.ayogrosir.interactors.search

import com.soldig.ayogrosir.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.cacheOnlyCall
import com.soldig.ayogrosir.model.Product
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class SearchProductByName
@Inject constructor(
    private val productCacheDataSource: ProductCacheDataSource
){

    fun fetch(
        nameKey: String,
        phoneNumber : String
    ): Flow<Resource<List<Product>>> = cacheOnlyCall {
        productCacheDataSource.getProductByName(nameKey,phoneNumber)
    }


}