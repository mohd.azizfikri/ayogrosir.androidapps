package com.soldig.ayogrosir.interactors.setting

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject


@ViewModelScoped
class SettingInteractor
@Inject constructor(
    val getCurrentSchedule: GetCurrentSchedule,
    val setDeliveryStatus: SetDeliveryStatus
){
}