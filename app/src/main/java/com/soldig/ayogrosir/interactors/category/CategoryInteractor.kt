package com.soldig.ayogrosir.interactors.category

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class CategoryInteractor
@Inject constructor(
    val getAllCategory: GetAllCategory,
    val getProductByCategory: GetProductByCategory
)