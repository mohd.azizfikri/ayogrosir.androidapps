package com.soldig.ayogrosir.interactors.virtual

import com.soldig.ayogrosir.datasource.network.impl.DigiFlazzNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.SelectedVirtualProduct
import com.soldig.ayogrosir.model.VirtualProductResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DoPayment @Inject constructor(
    private val digiFlazzNetworkDataSource: DigiFlazzNetworkDataSource
){

    fun fetch(
        token: String,
        selectedVirtualProduct: SelectedVirtualProduct
    ) : Flow<Resource<VirtualProductResult>> = networkOnlyCall {
        digiFlazzNetworkDataSource.payment(token, selectedVirtualProduct)
    }

}