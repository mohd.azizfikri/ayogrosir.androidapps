package com.soldig.ayogrosir.interactors.splash

import com.soldig.ayogrosir.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.ayogrosir.interactors.*
import com.soldig.ayogrosir.model.POS
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SplashSyncInteractor @Inject constructor(
    private val networkDataSource: ProductNetworkDataSource,
    private val cacheDataSource: ProductCacheDataSource,
    private val userNetworkDataSource: UserNetworkDataSource
) {

    fun fetch(
        pos: POS
    ) : Flow<Resource<Boolean>> = flow {
        emit(Resource.Loading())
         withContext(IO){
            val syncFCMTransaction = async { syncFCM(pos) }
            val syncProductTransaction = async { syncProduct(pos) }
            syncProductTransaction.await()
            syncFCMTransaction.await()
        }
        emit(Resource.Success(true, DataSource.Network))
    }

    private suspend fun syncFCM(
        pos: POS
    )  {
        try{
            userNetworkDataSource.updateFCM(pos.token)
        }catch (exception: ResourceError){
            exception.printStackTrace()
        }
    }

    private suspend fun syncProduct(
        pos: POS,
    )  {
        try {
            val lastTimeUpdateFromServer = safeApiCall {
                networkDataSource.getProductLastUpdate(pos.token)
            }
            val result = safeApiCall {
                networkDataSource.getAllProductList(pos)
            }
            safeCacheCall {
                cacheDataSource.insertAll(result)
                cacheDataSource.updateLastProductUpdate(pos.phoneNumber, lastTimeUpdateFromServer)
            }
        } catch (exception: ResourceError) {
            exception.printStackTrace()
        }
    }

    fun fetchFromDB(
        pos: POS
    ) : Flow<Resource<Boolean>> = flow {
        emit(Resource.Loading())

        try {
            safeCacheCall {
                cacheDataSource.getAll(pos.phoneNumber)
            }
        } catch (exception: ResourceError) {
            exception.printStackTrace()
        }

        emit(Resource.Success(true, DataSource.Network))
    }


}