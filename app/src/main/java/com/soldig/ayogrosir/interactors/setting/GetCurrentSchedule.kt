package com.soldig.ayogrosir.interactors.setting

import com.soldig.ayogrosir.datasource.network.abstraction.ScheduleNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.SettingModel
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetCurrentSchedule
@Inject constructor(
    private val scheduleNetworkDataSource: ScheduleNetworkDataSource
){


    fun fetch(
        token: String
    ) : Flow<Resource<SettingModel>> = networkOnlyCall{
        scheduleNetworkDataSource.getScheduleAndDeliveryStatus(token)
    }


}