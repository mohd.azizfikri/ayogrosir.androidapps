package com.soldig.ayogrosir.interactors.transaction

import com.soldig.ayogrosir.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkFirstCall
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.TransactionDetail
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetTransactionDetail
@Inject constructor(
    private val cacheDataSource: TransactionCacheDataSource,
    private val networkDataSource: TransactionNetworkDataSource
){

    fun fetch(
        pos: POS,
        id: String
    ) : Flow<Resource<TransactionDetail>> = networkFirstCall(
        fetch = {
            networkDataSource.getTransactionDetail(pos.token, id)
        },
        loadFromDB = {
            cacheDataSource.getTransactionDetail(id)
        }
    )

}