package com.soldig.ayogrosir.interactors.topup_virtual

import com.soldig.ayogrosir.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.ResponseRiwayatTopup
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class RiwayatTopupUser
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource
){
    fun fetch(
        token: String,
        tgl1: String,tgl2: String,status: String
    ) : Flow<Resource<ResponseRiwayatTopup>> = networkOnlyCall {
        userNetworkDataSource.getRiwayatSaldo(token, tgl1, tgl2, status)
    }
}