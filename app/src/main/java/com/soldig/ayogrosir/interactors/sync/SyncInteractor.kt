package com.soldig.ayogrosir.interactors.sync

import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SyncInteractor @Inject constructor(
    val sync: Sync
)