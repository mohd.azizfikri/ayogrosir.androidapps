package com.soldig.ayogrosir.interactors.sync

import com.soldig.ayogrosir.datasource.cache.abstraction.POSCacheDataSource
import com.soldig.ayogrosir.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.ayogrosir.interactors.safeApiCall
import com.soldig.ayogrosir.interactors.safeCacheCall
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Sync
@Inject constructor(
    private val posCacheDataSource: POSCacheDataSource,
    private val transactionCacheDataSource: TransactionCacheDataSource,
    private val transactionNetworkDataSource: TransactionNetworkDataSource
){


    suspend fun fetch(){
        withContext(IO){
            val posList = safeCacheCall { posCacheDataSource.getAllPos() }
            posList.forEach {
                val allPosTransaction = safeCacheCall { transactionCacheDataSource.getAllTransactionForSync(it.phoneNumber) }
                safeApiCall { transactionNetworkDataSource.checkoutSync(it.token, allPosTransaction) }
                safeCacheCall { transactionCacheDataSource.deleteAllPOSTransaction(it.phoneNumber) }
            }
        }
    }

}