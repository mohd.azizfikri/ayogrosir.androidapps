package com.soldig.ayogrosir.interactors.report

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class ReportInteractor
@Inject constructor(
    val getReport: GetReport
){
}