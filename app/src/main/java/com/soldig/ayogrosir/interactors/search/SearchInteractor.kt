package com.soldig.ayogrosir.interactors.search

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class SearchInteractor
@Inject constructor(
    val searchProductByName: SearchProductByName
){
}