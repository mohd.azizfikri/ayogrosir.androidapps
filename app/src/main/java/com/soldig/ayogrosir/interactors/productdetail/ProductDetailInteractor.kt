package com.soldig.ayogrosir.interactors.productdetail

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject


@ViewModelScoped
class ProductDetailInteractor
@Inject constructor(
    val updateProduct: UpdateProduct,
    val getAllAvailableProduct: GetAllAvailableProduct
){
}