package com.soldig.ayogrosir.interactors.virtual

import com.soldig.ayogrosir.datasource.network.impl.DigiFlazzNetworkDataSource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import javax.inject.Inject

class PostPaidCheck @Inject constructor(
    private val digiFlazzNetworkDataSource: DigiFlazzNetworkDataSource
) {

    fun fetch(
        token: String,
        skuCode: String,
        number: String
    ) = networkOnlyCall {
        digiFlazzNetworkDataSource.checkPostpaid(token, skuCode, number)
    }

}