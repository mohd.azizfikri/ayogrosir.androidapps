package com.soldig.ayogrosir.interactors.virtual

import javax.inject.Inject

class VirtualInteractor @Inject constructor(
    val getVirtualProductList: GetVirtualProductList,
    val postPaidCheck: PostPaidCheck,
    val doPayment: DoPayment,
    val getHistory: GetHistory
) {
}