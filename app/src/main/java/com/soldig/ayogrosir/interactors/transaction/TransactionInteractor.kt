package com.soldig.ayogrosir.interactors.transaction

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class TransactionInteractor
@Inject constructor(
    val getTransactionHistory: GetTransactionHistory,
    val getTransactionDetail : GetTransactionDetail,
    val cancelTransaction: CancelTransaction
)