package com.soldig.ayogrosir.interactors.topup_virtual

import com.soldig.ayogrosir.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.ResponseTopup
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

@ViewModelScoped
class TopupVirtualUser
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource,
) {
    suspend fun topUp(token : String, param: HashMap<String, @JvmSuppressWildcards RequestBody>, partFile: MultipartBody.Part?)
    : Flow<Resource<ResponseTopup>> = networkOnlyCall{
        userNetworkDataSource.topupVirtual(token, param, partFile)
    }
}