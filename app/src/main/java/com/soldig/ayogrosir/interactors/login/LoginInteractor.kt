package com.soldig.ayogrosir.interactors.login

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject


@ViewModelScoped
class LoginInteractor
@Inject constructor(
   val userLogin: UserLogin
)