package com.soldig.ayogrosir.interactors.home

import com.soldig.ayogrosir.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.ayogrosir.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkFirstCall
import com.soldig.ayogrosir.interactors.safeApiCall
import com.soldig.ayogrosir.model.*
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class Checkout
@Inject constructor(
    private val transactionNetworkDataSource: TransactionNetworkDataSource,
    private val transactionCacheDataSource: TransactionCacheDataSource,
    private val productNetworkDataSource: ProductNetworkDataSource,
    private val productCacheDataSource: ProductCacheDataSource
){
    fun fetch(
        pos: POS,
        transaction: Transaction
    ) : Flow<Resource<CheckoutReturn>> = networkFirstCall(
        loadFromDB = {
            val transactionResult = transactionCacheDataSource.checkout(pos.phoneNumber,transaction)
            CheckoutReturn(
                transactionResult = transactionResult,
                isCheckoutDoneInServer = false
            )
        },
        fetch = {
            getPOSProductFromTransaction(transaction)
            val transactionResult = transactionNetworkDataSource.checkout(pos.token,transaction)
            val lastTimeUpdateFromServerAfterUpdate = safeApiCall {
                productNetworkDataSource.getProductLastUpdate(pos.token)
            }
            productCacheDataSource.updateLastProductUpdate(pos.phoneNumber, lastTimeUpdateFromServerAfterUpdate)
            productCacheDataSource.insertAll(getPOSProductFromTransaction(transaction))
            CheckoutReturn(
                transactionResult = transactionResult,
                isCheckoutDoneInServer = true
            )
        }
    )

    private fun getPOSProductFromTransaction(transaction: Transaction) : List<Product.POSProduct> {
        return transaction.cart.orderList.map {
            val newProduct = it.value.product.posProduct
            newProduct
        }
    }
}