package com.soldig.ayogrosir.interactors

import androidx.annotation.StringRes
import com.soldig.ayogrosir.utils.other.UIInteraction

sealed class Resource<T>{
    data class Success<T>(val data: T,val dataSource: DataSource) : Resource<T>()
    class Loading<T>() : Resource<T>()
    data class Error<T>(val uiInteraction: UIInteraction) : Resource<T>()
}

enum class DataSource{
    Network,
    Cache,
    NetworkAndCache
}

sealed class MessageType{
    data class StringMessage(val message: String) : MessageType()
    data class ResourceMessage(@StringRes val messageStringRes: Int) : MessageType()
}

enum class ErrorType{
    FromNetwork,
    FromCache
}

class ResourceError(val errorType: MessageType, val dataSource: ErrorType) : Exception()
