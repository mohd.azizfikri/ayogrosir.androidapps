package com.soldig.ayogrosir.interactors.topup

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class TopupInteractor
@Inject constructor(
    private val setBeginingBalance: SetBeginingBalance
){
}