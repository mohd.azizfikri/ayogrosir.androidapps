package com.soldig.ayogrosir.interactors.transaction

import com.soldig.ayogrosir.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.TransactionStatus
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class CancelTransaction
@Inject constructor(
    private val transactionNetworkDataSource: TransactionNetworkDataSource
){

    fun fetch(
        token : String,
        id : String
    ) : Flow<Resource<String>> = networkOnlyCall {
        transactionNetworkDataSource.updateTransactionStatus(token, id, TransactionStatus.DIBATALKAN)
    }

}