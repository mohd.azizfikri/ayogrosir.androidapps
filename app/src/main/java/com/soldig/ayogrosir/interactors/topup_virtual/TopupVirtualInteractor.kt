package com.soldig.ayogrosir.interactors.topup_virtual

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class TopupVirtualInteractor
@Inject constructor(
    val topUp: TopupVirtualUser
)