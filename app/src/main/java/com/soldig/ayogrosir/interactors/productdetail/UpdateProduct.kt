package com.soldig.ayogrosir.interactors.productdetail

import com.soldig.ayogrosir.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.ayogrosir.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.ayogrosir.interactors.*
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.model.Product
import com.soldig.ayogrosir.utils.other.UIInteraction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ViewModelScoped
class UpdateProduct
@Inject constructor(
    private val networkDataSource: ProductNetworkDataSource,
    private val cacheDataSource: ProductCacheDataSource
){

    fun fetch(
        pos: POS,
        product: Product
    ) : Flow<Resource<String>> = flow {
        try {
            emit(Resource.Loading())
            val lastTimeUpdateFromCache = safeCacheCall {
                cacheDataSource.getLastProductUpdate(pos.phoneNumber)
            }
            var lastTimeUpdateFromServer =  safeApiCall {
                networkDataSource.getProductLastUpdate(pos.token)
            }
            if(lastTimeUpdateFromCache != lastTimeUpdateFromServer){
                val newestProductList = safeApiCall {
                    networkDataSource.getAllProductList(pos)
                }
                safeCacheCall {
                    cacheDataSource.insertAll(newestProductList)
                }
            }
            val result = safeApiCall {
                networkDataSource.updateProduct(pos.token, product)
            }
            lastTimeUpdateFromServer = safeApiCall {
                networkDataSource.getProductLastUpdate(pos.token)
            }
            safeCacheCall {
                cacheDataSource.upsertOne(result)
                cacheDataSource.updateLastProductUpdate(pos.phoneNumber, lastTimeUpdateFromServer)
            }
            emit(Resource.Success(result.id, DataSource.Network))
        } catch (exception: ResourceError) {
            emit(
                Resource.Error<String>(
                    uiInteraction = UIInteraction.GenericMessage(
                        exception.errorType
                    )
                )
            )
        }
    }.flowOn(Dispatchers.IO)


//    fun fetch(
//        token: String,
//        product: Product
//    ) : Flow<Resource<String>> = networkOnlyCall {
//        productNetworkDataSource.updateProduct(token, product)
//    }

}