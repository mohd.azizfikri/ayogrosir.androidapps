package com.soldig.ayogrosir.interactors.category

import com.soldig.ayogrosir.datasource.network.abstraction.CategoryNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.ProductCategory
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetAllCategory
@Inject constructor(
    private val categoryNetworkDataSource: CategoryNetworkDataSource
){
    fun fetch(
        token: String
    ) : Flow<Resource<List<ProductCategory>>> = networkOnlyCall {
        categoryNetworkDataSource.getAllCategory(token)
    }
}