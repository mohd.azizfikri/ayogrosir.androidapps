package com.soldig.ayogrosir.interactors.schedule

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class ScheduleInteractor
@Inject constructor(
    val changeSchedule: ChangeSchedule,
){
}