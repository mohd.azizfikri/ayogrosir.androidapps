package com.soldig.ayogrosir.interactors.schedule

import com.soldig.ayogrosir.datasource.network.abstraction.ScheduleNetworkDataSource
import com.soldig.ayogrosir.interactors.Resource
import com.soldig.ayogrosir.interactors.networkOnlyCall
import com.soldig.ayogrosir.model.Schedule
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


@ViewModelScoped
class ChangeSchedule
@Inject constructor(
    private val scheduleNetworkDataSource: ScheduleNetworkDataSource
){

    fun fetch(
        token: String,
        schedule: Schedule
    ) : Flow<Resource<String>> = networkOnlyCall{
        scheduleNetworkDataSource.postSchedule(token,schedule)
    }

}