package com.soldig.ayogrosir.worker

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.soldig.ayogrosir.interactors.sync.SyncInteractor
import javax.inject.Inject

class MyWorkerFactory
    @Inject constructor(
        private val syncInteractor: SyncInteractor
    ): WorkerFactory() {


    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {
        return when (workerClassName) {
            CheckoutWorker::class.java.name -> {
                CheckoutWorker(
                    appContext,
                    workerParameters,
                    syncInteractor = syncInteractor
                )
            }
            else -> null
        }
    }
}