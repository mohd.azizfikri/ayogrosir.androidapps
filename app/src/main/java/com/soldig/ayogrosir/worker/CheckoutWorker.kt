package com.soldig.ayogrosir.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.soldig.ayogrosir.interactors.sync.SyncInteractor
import javax.inject.Inject

class CheckoutWorker @Inject constructor(
    appContext : Context,
    workerParams: WorkerParameters,
    private val syncInteractor: SyncInteractor
) : CoroutineWorker(appContext, workerParams) {

    override suspend fun doWork(): Result {
        return try{
            syncInteractor.sync.fetch()
            println("Workmanager Success")
            Result.success()
        }catch (exception: Exception){
            println("Workmanage gagal karena :")
            exception.printStackTrace()
            Result.retry()
        }
    }
}
