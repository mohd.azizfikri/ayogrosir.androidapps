package com.soldig.ayogrosir.base

import android.content.Intent
import com.soldig.ayogrosir.model.POS
import com.soldig.ayogrosir.presentation.feature.login.LoginActivity
import com.soldig.ayogrosir.utils.ext.clearUser
import com.soldig.ayogrosir.utils.ext.getUserInSharedPref

abstract class BaseLoggedInActivity : BaseActivity(){

    val currentPOS : POS by lazy {
        getUserInSharedPref() ?: logout()
    }
    protected fun logout() : POS{
        clearUser()
        startActivity(Intent(this, LoginActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
        finish()
        return POS.produceFailedPOS()
    }
}