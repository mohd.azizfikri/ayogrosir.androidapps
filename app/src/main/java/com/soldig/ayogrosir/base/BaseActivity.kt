package com.soldig.ayogrosir.base

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.common.hideSystemUI
import com.soldig.ayogrosir.databinding.ToolbarBinding
import com.soldig.ayogrosir.utils.ext.hideKeyboardInActivity
import java.text.SimpleDateFormat
import java.util.*


abstract class BaseActivity : AppCompatActivity() {

    open fun shouldHideSystemUI() : Boolean {
        return true
    }
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus && shouldHideSystemUI()) hideSystemUI()
    }
    @SuppressLint("SimpleDateFormat")
    fun initTime(whiteColor: Boolean = false, toolbar : ToolbarBinding) {
        val calendar = Calendar.getInstance()
        val id = Locale("in", "ID")
        val simpleDateFormat = SimpleDateFormat("EEEE, dd MMMM yyyy", id)
        val dateTime = simpleDateFormat.format(calendar.time)
        with(toolbar){
            tvTime.text = dateTime
            if (whiteColor) {
                tvTime.setTextColor(Color.WHITE)
                tvAppName.setTextColor(Color.WHITE)
                tvClock.setTextColor(Color.WHITE)
            }
        }
    }
    fun initToolbar(title: String = "", toolbar: ToolbarBinding) {
        with(toolbar.mToolbar){
            isVisible = true
            setSupportActionBar(this)
            supportActionBar?.title = title
            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            setNavigationIcon(R.drawable.ic_group_16204)
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(shouldHideSystemUI()){
            when (item.itemId) {
                android.R.id.home -> {
                    finish()
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        hideKeyboardInActivity()
    }
    fun dialogDocument(title: String, onShow: (view: View, dialog: AlertDialog) -> Unit) {
        val dialog: AlertDialog
        val dialogBuilder = AlertDialog.Builder(this)
        val view: View = layoutInflater.inflate(R.layout.item_popup, null)
        //view.tvJudulDialogDefault.text = "$title"
        dialogBuilder.setView(view)
        dialog = dialogBuilder.create()
        //dialog.window?.setBackgroundDrawableResource(R.drawable.bg_rounded_shape_white)
        dialog.show()
        view.setOnClickListener {  }
        onShow(view, dialog)
    }
}