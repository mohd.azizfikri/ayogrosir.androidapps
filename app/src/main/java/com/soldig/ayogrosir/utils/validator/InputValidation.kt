package com.soldig.ayogrosir.utils.validator

sealed class InputValidation<T>(t  : T){

    data class Success<T>(val t : T) : InputValidation<T>(t)

    data class Error<T>(val t : T) : InputValidation<T>(t)

}

infix fun <T> InputValidation<T>.then(f : InputValidation<T>) : InputValidation<T> =
    when (this) {
        is InputValidation.Success -> f
        is InputValidation.Error -> InputValidation.Error(this.t)
    }



