package com.soldig.ayogrosir.utils.ext
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.android.material.snackbar.Snackbar


fun View.goVisible() {
    visibility = View.VISIBLE
}

fun View.goGone() {
    visibility = View.GONE
}

fun View.goInvisible() {
    visibility = View.INVISIBLE
}

fun View.toggleVisibility(isVisible: Boolean) = if(isVisible) goVisible() else goGone()

fun View.showLongSnackbar(message:String){
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}

fun View.showShortSnackbar(message:String){
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}

fun View.hideKeyboard() {
    val inputMethodManager = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}



