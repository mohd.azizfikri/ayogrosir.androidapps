package com.soldig.ayogrosir.utils


import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.soldig.ayogrosir.R
import java.util.*


class DatePickerFragment : DialogFragment(), OnDateSetListener {
    private lateinit var listener: OnDatePicker
    private var type = 1

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = activity as OnDatePicker
        } catch (e: ClassCastException) {
            Log.e("TAG", "onAttach: " + e.message)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = arguments?.getInt("type", 1) as Int
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current date as the default date in the picker
        val c: Calendar = Calendar.getInstance()
        val year: Int = c.get(Calendar.YEAR)
        val month: Int = c.get(Calendar.MONTH)
        val day: Int = c.get(Calendar.DAY_OF_MONTH)

        // Create a new instance of DatePickerDialog and return it
        return DatePickerDialog(requireContext(), R.style.AppTheme_DatePickerDialog, this, year, month, day)
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        val date = day.toString() + "/" + (month + 1).toString() + "/" + year.toString()
        listener.onDateSelect(date, type)
    }

    interface OnDatePicker {
        fun onDateSelect(date: String, type: Int)
    }

    companion object {
        fun newInstance(type: Int): DatePickerFragment {
            val f = DatePickerFragment()
            val args = Bundle()
            args.putInt("type", type)
            f.arguments = args
            return f
        }
    }
}