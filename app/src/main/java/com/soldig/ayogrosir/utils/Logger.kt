package com.soldig.ayogrosir.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Logger
@Inject constructor(
    @ApplicationContext val  context : Context
){


    private var fos : FileOutputStream? = null

    fun log(text: String){
        try {
            fos = context.openFileOutput("LOGGER", MODE_PRIVATE)
            fos?.write(("\n" + text).toByteArray())
        }catch (e : FileNotFoundException){

        } finally {
            fos?.close()
        }
    }

}