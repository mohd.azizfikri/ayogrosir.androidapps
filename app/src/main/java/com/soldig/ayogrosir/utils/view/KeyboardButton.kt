package com.soldig.ayogrosir.utils.view

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.databinding.KeyboardButtonBinding
import com.soldig.ayogrosir.utils.ext.toDips

class KeyboardButton (
    context: Context,
    attributeSet: AttributeSet? = null
) : FrameLayout(context, attributeSet) {

    private val binding : KeyboardButtonBinding =
        KeyboardButtonBinding.inflate(LayoutInflater.from(context), this, true)

    var textSize : Float
    var cardSize : Float
    var text: String = "1"

    fun getKeyboardText() : String = binding.text.text.toString()

    init{
        try{
            context.theme.obtainStyledAttributes(
                attributeSet,
                R.styleable.KeyboardButton,
                0,0
            ).apply {
                textSize = getDimensionPixelSize(R.styleable.KeyboardButton_keyboardTextSize, 24).toFloat()
                cardSize = getDimensionPixelSize(R.styleable.KeyboardButton_keyboardCardSize, 48).toFloat()
                text = getString(R.styleable.KeyboardButton_keyboardText) ?: text
                binding.text.text = text
                binding.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
                binding.root.layoutParams = binding.root.layoutParams.apply {
                    height = cardSize.toDips(context.resources)
                    width = cardSize.toDips(context.resources)
                }

            }
        }finally {

        }


    }
}