package com.soldig.ayogrosir.utils.validator

interface ValidationCallback<Input,Result>{
    fun validator(input : Input) : InputValidation<Result>
    fun onValidationDone(input: Input, isValid : Boolean)
}