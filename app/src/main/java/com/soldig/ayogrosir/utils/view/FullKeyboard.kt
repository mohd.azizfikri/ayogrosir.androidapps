package com.soldig.ayogrosir.utils.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import com.soldig.ayogrosir.databinding.FullKeyboardLayoutBinding

class FullKeyboard (
    context: Context,
    attributeSet: AttributeSet? = null
) : FrameLayout(context, attributeSet) , View.OnClickListener {

    private val binding = FullKeyboardLayoutBinding.inflate(LayoutInflater.from(context), this, true)


    private var editText : EditText?  = null



    init {
        binding.apply {
            val btnList  = listOf(btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0,btnQ,btnW,btnE,btnR,btnT,btnY,btnU,btnI,btnO,btnP,btnA,btnS,btnD
                ,btnF,btnG,btnH,btnJ,btnK,btnL,btnZ,btnX,btnC,btnV,btnB,btnN,btnM
            )
            btnList.forEach {
                it.setOnClickListener(this@FullKeyboard)
            }
            btnSpace.setOnClickListener{
                appendEditText(" ")
            }
            btnDelete.setOnClickListener {
                editText?.let{
                    if(it.text.isNotEmpty()){
                        if(it.selectionStart == it.selectionEnd){
                            it.text.delete(it.selectionStart - 1 , it.selectionStart)
                        }else{
                            it.text.delete(it.selectionStart , it.selectionEnd)
                        }
                    }
                }
            }
        }
    }


    fun setEditedEditTextByKeboard(editText: EditText){
        this.editText = editText
    }

    override fun onClick(view: View?) {
        view ?: return
        if(view is KeyboardButton){
            appendEditText(view.getKeyboardText())
        }
    }

    @SuppressLint("SetTextI18n")
    private fun appendEditText(string: String) {
        editText?.let{
            val startSelection = it.selectionStart
            val endSelection = it.selectionEnd
            val completeString = it.text.toString()
            val startString = completeString.substring(0 , startSelection)
            val endString = completeString.substring(endSelection , completeString.length)
            it.setText(startString + string + endString )
            it.setSelection(if(startSelection == endSelection) it.text.length else startSelection + 1)
        }
    }

}