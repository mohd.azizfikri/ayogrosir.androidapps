package com.soldig.ayogrosir.utils.ext

import android.app.Activity
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.soldig.ayogrosir.R
import com.soldig.ayogrosir.databinding.*
import com.soldig.ayogrosir.model.POS


fun Activity.getUserInSharedPref() : POS? {
    val pref = getSharedPreferences(getString(R.string.file_pref_key), Context.MODE_PRIVATE)
    val token =  pref.getString(getString(R.string.currenc_acc_pref_key_token), null)
    val name = pref.getString(getString(R.string.currenc_acc_pref_key_name), null)
    val phoneNumber = pref.getString(getString(R.string.currenc_acc_pref_key_number),null)
    val loginTime =   pref.getLong(getString(R.string.currenc_acc_pref_key_login_time),-1L)
    val address = pref.getString(getString(R.string.currenc_acc_pref_key_address), null)
    val profileImage = pref.getString(getString(R.string.currenc_acc_pref_key_profile_pic), null)
    return if(token != null && name != null && phoneNumber != null && loginTime != -1L && address != null &&  profileImage != null) {
        POS(
            name = name,
            token = token,
            phoneNumber = phoneNumber,
            loginTime = loginTime,
            alamat = address,
            profileImage = profileImage
        )
    }else{
        null
    }
}

fun Activity.hideKeyboard() {
    //Find the currently focused view, so we can grab the correct window token from it.
    //Find the currently focused view, so we can grab the correct window token from it.
    var currentView: View = currentFocus ?: View(this)
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    (getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(currentView.windowToken, 0)
}

fun Activity.setUser(pos: POS){

    val pref = getSharedPreferences(getString(R.string.file_pref_key), Context.MODE_PRIVATE)
    with(pref.edit()){
        putString(getString(R.string.currenc_acc_pref_key_token), pos.token)
        putString(getString(R.string.currenc_acc_pref_key_name), pos.name)
        putString(getString(R.string.currenc_acc_pref_key_number), pos.phoneNumber)
        putLong(getString(R.string.currenc_acc_pref_key_login_time), pos.loginTime)
        putString(getString(R.string.currenc_acc_pref_key_address),pos.alamat)
        putString(getString(R.string.currenc_acc_pref_key_profile_pic),pos.profileImage)
        apply()
    }
}

fun Activity.clearUser(){
    val pref = getSharedPreferences(getString(R.string.file_pref_key), Context.MODE_PRIVATE)
    with(pref.edit()){
        clear()
        apply()
    }
}

fun Context.launchConfirmationDialog(
    message: String,
    title: String = "",
    @StringRes positiveButtonText: Int = R.string.yes,
    @StringRes negativeButtonText: Int = R.string.no,
    onDismiss: () -> Unit = {},
    onPositiveButton : (materialDialog: MaterialDialog) -> Unit ,
    onNegativeButton: (materialDialog: MaterialDialog) -> Unit ) : MaterialDialog{
    return MaterialDialog(this)
        .apply {
            setOnDismissListener {
                onDismiss()
            }
        }
        .cornerRadius(12f)
        .show {
            val dialogBinding =  DialogConfirmationBinding.inflate(LayoutInflater.from(this.view.context),this.view,false)
            customView(view = dialogBinding.root)
            with(dialogBinding){
                tvTitle.toggleVisibility(title.isNotEmpty())
                tvTitle.text = title
                tvMessage.toggleVisibility(message.isNotEmpty())
                tvMessage.text = message
                btnYes.setOnClickListener { onPositiveButton(this@show) }
                btnNo.setOnClickListener { onNegativeButton(this@show) }
            }

        }

}

fun Context.launchInformationDialog(
    message: String, title: String ): MaterialDialog{
    return MaterialDialog(this)
        .cornerRadius(12f)
        .show {
            val dialogBinding =  DialogInformationBinding.inflate(LayoutInflater.from(this.view.context),this.view,false)
            customView(view = dialogBinding.root)
            dialogBinding.tvTitle.text =title;
            dialogBinding.tvMessage.text  =message;
            dialogBinding.btnYes.setOnClickListener{
                this.dismiss()
            }


        }

}

fun Context.launchInformationDialogNew(
    message: String, title: String, textButton: String ): MaterialDialog{
    return MaterialDialog(this)
        .cornerRadius(12f)
        .show {
            val dialogBinding =  DialogInformationBinding.inflate(LayoutInflater.from(this.view.context),this.view,false)
            customView(view = dialogBinding.root)
            dialogBinding.tvTitle.text =title;
            dialogBinding.tvMessage.text  =message;
            dialogBinding.btnYes.text = textButton
            dialogBinding.btnYes.setOnClickListener{
                this.dismiss()
            }


        }

}

fun Context.launchErrorDialog(
    message: String, title: String , errorMessage: String): MaterialDialog{
    return MaterialDialog(this)
        .cornerRadius(12f)
        .show {
            val dialogBinding =  DialogErrorBinding.inflate(LayoutInflater.from(this.view.context),this.view,false)
            customView(view = dialogBinding.root)
            dialogBinding.tvTitle.text =title
            dialogBinding.tvMessage.text  =message
            dialogBinding.tvError.text = errorMessage
            dialogBinding.btnYes.setOnClickListener{
                this.dismiss()
            }

        }

}


fun Context.launchDialogTopup(
    message: String, title: String ): MaterialDialog{
    return MaterialDialog(this)
        .cornerRadius(12f)
        .show {
            val dialogBinding =  ItemPopupBinding.inflate(LayoutInflater.from(this.view.context),this.view,false)
            customView(view = dialogBinding.root)
            dialogBinding.tvJudulDialog.text =title;
            dialogBinding.tvIsiDialog.text  =message;
            dialogBinding.btnBacktopup.setOnClickListener{
                this.dismiss()
            }


        }

}

fun Context.launchDialogTopupNew(): MaterialDialog{
    return MaterialDialog(this)
        .cornerRadius(12f)
        .show {
            val dialogBinding =  ItemTopupNewBinding.inflate(LayoutInflater.from(this.view.context),this.view,false)
            customView(view = dialogBinding.root)
//            dialogBinding.tvJudulDialog.text =title;
//            dialogBinding.tvIsiDialog.text  =message;
            dialogBinding.btnBacktopup.setOnClickListener{
                this.dismiss()
            }


        }

}



fun Activity.hideKeyboardInActivity() {
    val view: View? = this.currentFocus
    if (view != null) {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
