package com.soldig.ayogrosir.datasource.cache.model

import androidx.room.Embedded

data class TransactionWithProduct(
    @Embedded val transaction : TransactionEntity,
    @Embedded val product : ProductEntity,
    @Embedded val transactionProductCrossRef: TransactionProductCrossRef
)
