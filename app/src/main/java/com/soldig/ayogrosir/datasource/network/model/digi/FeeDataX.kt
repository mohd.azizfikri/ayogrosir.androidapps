package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class FeeDataX(
    @SerializedName("FeeApp")
    val feeApp: Double,
    @SerializedName("FeeShop")
    val feeShop: Double,
    @SerializedName("Id")
    val id: String
)