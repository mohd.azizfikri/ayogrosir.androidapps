package com.soldig.ayogrosir.datasource.network.model


import com.google.gson.annotations.SerializedName

data class TransactionDto(
    @SerializedName("Address")
    val address: String,
    @SerializedName("Amount")
    val amount: Double,
    @SerializedName("Change")
    val change: Double,
    @SerializedName("Created")
    val created: String,
    @SerializedName("DeliveryFee")
    val deliveryFee: Double,
    @SerializedName("Details")
    val details: List<OrderItemDto>,
    @SerializedName("Discount")
    val discount: Double,
    @SerializedName("Id")
    val id: String,
    @SerializedName("IsPaid")
    val isPaid: Boolean,
    @SerializedName("Latitude")
    val latitude: String,
    @SerializedName("Longitude")
    val longitude: String,
    @SerializedName("M_Customer_Id")
    val mCustomerId: String? = null,
    @SerializedName("M_Shop_Id")
    val mShopId: String,
    @SerializedName("Paid")
    val paid: Double,
    @SerializedName("PayWith")
    val payWith: String? ,
    @SerializedName("ProductType")
    val productType: Int,
    @SerializedName("Reason")
    val reason: String? = null,
    @SerializedName("StrStatus")
    val status: String? = null,
    @SerializedName("StrChange")
    val strChange: String,
    @SerializedName("StrPaid")
    val strPaid: String,
    @SerializedName("TotalAmount")
    val totalAmount: Double,
    @SerializedName("TotalItem")
    val totalItem: Int,
    @SerializedName("TransNo")
    val transNo: String,
    @SerializedName("TransStatus")
    val transStatus: Int,
    @SerializedName("TransType")
    val transType: Int,
    @SerializedName("CustomerName")
    val customerName: String,
    @SerializedName("Phone")
    val customerPhone: String
)