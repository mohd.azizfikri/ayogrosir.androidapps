package com.soldig.ayogrosir.datasource.cache.abstraction

import com.soldig.ayogrosir.model.Product

interface ProductCacheDataSource {


    suspend fun upsertOne(product: Product)

    suspend fun insertAll(product : List<Product>)

    suspend fun getAll(phoneNumber: String) : List<Product>

    suspend fun getProductByName(nameKey: String,phoneNumber: String) : List<Product>

    suspend fun updateLastProductUpdate(phoneNumber: String, lastProductUpdate: String)

    suspend fun getLastProductUpdate(phoneNumber: String) : String

}