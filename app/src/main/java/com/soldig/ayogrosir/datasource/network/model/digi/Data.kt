package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("customer_no")
    val customerNo: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("price")
    val price: Double,
    @SerializedName("rc")
    val rc: String,
    @SerializedName("ref_id")
    val refId: String,
    @SerializedName("status")
    val status: String,
)