package com.soldig.ayogrosir.datasource.network.mapper

import com.soldig.ayogrosir.datasource.DomainMapper
import com.soldig.ayogrosir.datasource.network.model.ProductDto
import com.soldig.ayogrosir.model.DiscountType
import com.soldig.ayogrosir.model.Product
import javax.inject.Inject


class ProductNetworkMapper @Inject constructor(): DomainMapper<ProductDto,Product> {
    override fun mapFromDomain(domain: Product): ProductDto {
        val id = when(domain){
            is Product.NotPOSProduct -> null
            is Product.POSProduct -> domain.id
        }
        return ProductDto(
            id = id,
            mProductId = domain.productId,
            mProductcategoryId = domain.categoryId,
            picture = domain.img,
            quantityInCart = domain.qty,
            name = domain.title,
            sellPrice = domain.sellPrice.toInt(),
            currentPrice = domain.sellAfterDiscountPrice.toInt(),
            discountType = discountTypeToId(domain.discountType),
            discountValue = domain.discountValue.toInt(),
            isActive = domain.isActive,
            isFeatured = domain.isPilihan,
            isOwned = domain.isOwned,
            purchasePrice = domain.purchasePrice.toInt(),
            order = domain.ordering,
            Created = "",
            CreatedBy = "",
            HasDiscount = false,
            Modified = "",
            ModifiedBy = "",
            StrCurrentPrice = "",
            StrSellPrice = ""
        )
    }

    override fun mapToDomain(t: ProductDto): Product {
        if(!t.id.equals("0")){
            return Product.POSProduct(
                id = t.id!!,
                img = t.picture,
                title =  t.name,
                qty = t.stock,
                sellPrice = t.sellPrice.toDouble(),
                sellAfterDiscountPrice = t.currentPrice.toDouble(),
                purchasePrice = t.purchasePrice.toDouble(),
                discountType = discountIdToType(t.discountType),
                discountValue = t.discountValue?.toDouble() ?: 0.0,
                categoryId = t.mProductcategoryId,
                isActive = t.isActive,
                isPilihan = t.isFeatured,
                isOwned = t.isOwned,
                ordering = t.order,
                productId = t.mProductId,
                posId = t.mShopId,
                packSize = t.pakcSize
            )
        }
        return Product.NotPOSProduct(
            img = t.picture,
            title =  t.name,
            qty = t.stock,
            sellPrice = t.sellPrice.toDouble(),
            sellAfterDiscountPrice = t.currentPrice.toDouble(),
            purchasePrice = t.purchasePrice.toDouble(),
            discountType = discountIdToType(t.discountType),
            discountValue = t.discountValue?.toDouble() ?: 0.0,
            categoryId = t.mProductcategoryId,
            isActive = t.isActive,
            isPilihan = t.isFeatured,
            isOwned = t.isOwned,
            ordering = t.order,
            productId = t.mProductId,
            posId = t.mShopId,
            packSize = t.pakcSize
        )

    }


    fun mapToDomainWithNewPOSID(t:ProductDto, phoneNumber: String) : Product{
        val domain = mapToDomain(t)
        println("Phone Number")
        domain.posId = phoneNumber
        return domain
    }

    private fun discountIdToType(discountId : Int?) : DiscountType{
        return when(discountId) {
            1 -> DiscountType.Percentage
            2 -> DiscountType.Nominal
            else -> DiscountType.Nothing
        }
    }

    fun discountTypeToId(discountType: DiscountType) : Int? {
        return when(discountType){
            DiscountType.Nothing -> null
            DiscountType.Percentage -> 1
            DiscountType.Nominal -> 2
        }
    }

}