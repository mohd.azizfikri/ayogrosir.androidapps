package com.soldig.ayogrosir.datasource.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "product")
data class ProductEntity(

    @PrimaryKey
    @ColumnInfo(name ="product_id")
    val productId: String,

    @ColumnInfo(name = "category_id")
    val categoryId: String,

    @ColumnInfo(name = "img")
    val img: String,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name ="pack_size")
    val packSize: String,
)