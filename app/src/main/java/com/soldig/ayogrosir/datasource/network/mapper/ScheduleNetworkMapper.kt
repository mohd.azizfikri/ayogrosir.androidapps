package com.soldig.ayogrosir.datasource.network.mapper

import com.soldig.ayogrosir.datasource.DomainMapper
import com.soldig.ayogrosir.model.Schedule
import com.soldig.ayogrosir.utils.ext.translateDate
import java.util.*
import javax.inject.Inject


class ScheduleNetworkMapper
@Inject constructor() : DomainMapper<Pair<String,String>, Schedule> {


    override fun mapFromDomain(domain: Schedule): Pair<String, String> {
        val indonesiaLocale = Locale("id", "ID")
        return when(domain){
            is Schedule.Close -> Pair(translateDate(indonesiaLocale, Locale.ENGLISH, domain.day),"Close")
            else ->Pair(translateDate(indonesiaLocale, Locale.ENGLISH, domain.day),domain.hour)
        }
    }

    override fun mapToDomain(t: Pair<String, String>): Schedule {
        val indonesiaLocale = Locale("id", "ID")
        val day = translateDate(Locale.ENGLISH, indonesiaLocale, t.first)
        return when (t.second) {
            "Close" -> Schedule.Close(day)
            else -> Schedule.Open(day,t.second)
        }
    }









}