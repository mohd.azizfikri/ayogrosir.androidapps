package com.soldig.ayogrosir.datasource.cache.model

import androidx.room.Embedded
import androidx.room.Relation

data class TransactionForSyncRelation(
    @Embedded val transaction : TransactionEntity,
    @Relation(
        parentColumn = "transaction_id",
        entityColumn = "transactionId"
    )
    val transactionProductCrossRef: List<TransactionProductCrossRef>
)
