package com.soldig.ayogrosir.datasource.network.abstraction

import com.soldig.ayogrosir.model.ProductCategory

interface CategoryNetworkDataSource {


    suspend fun getAllCategory(token: String) : List<ProductCategory>

}