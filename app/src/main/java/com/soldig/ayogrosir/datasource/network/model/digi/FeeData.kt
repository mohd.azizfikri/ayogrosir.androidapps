package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class FeeData(
    @SerializedName("FeeApp")
    val feeApp: Double = 0.0,
    @SerializedName("FeeShop")
    val feeShop: Double = 0.0,
    @SerializedName("FeeTotal")
    val feeTotal: Double = 0.0,
    @SerializedName("HargaProduk")
    val hargaProduk: Double = 0.0
)