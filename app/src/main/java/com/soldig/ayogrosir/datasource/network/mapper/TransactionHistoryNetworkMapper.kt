package com.soldig.ayogrosir.datasource.network.mapper

import com.soldig.ayogrosir.datasource.OneWayDomainMapper
import com.soldig.ayogrosir.datasource.network.model.TransactionDto
import com.soldig.ayogrosir.model.TransactionHistory
import com.soldig.ayogrosir.model.TransactionStatus
import javax.inject.Inject


class TransactionHistoryNetworkMapper
@Inject constructor(

) : OneWayDomainMapper<TransactionDto, TransactionHistory>{
    override fun mapToDomain(t: TransactionDto): TransactionHistory {
        return TransactionHistory(
            orderDate = t.created,
            orderNumber = t.transNo,
            totalItem = t.totalItem,
            totalPrice = t.totalAmount,
            id = t.id,
            status = TransactionStatus.getByValue(t.transStatus) ?: TransactionStatus.UNKNOWN
        )
    }

}