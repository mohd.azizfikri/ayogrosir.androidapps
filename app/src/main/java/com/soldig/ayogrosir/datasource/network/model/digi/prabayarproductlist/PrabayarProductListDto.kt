package com.soldig.ayogrosir.datasource.network.model.digi.prabayarproductlist


import com.google.gson.annotations.SerializedName

data class PrabayarProductListDto(
    @SerializedName("DFResult")
    val dFResult: List<DFResult>,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)