package com.soldig.ayogrosir.datasource.network.model


import com.google.gson.annotations.SerializedName

data class TransactionListForSyncDto(
    @SerializedName("Transactions")
    val transactions: List<TransactionForSyncDto>
)