package com.soldig.ayogrosir.datasource.network.impl

import com.soldig.ayogrosir.datasource.network.abstraction.CategoryNetworkDataSource
import com.soldig.ayogrosir.datasource.network.mapper.ProductCategoryNetworkMapper
import com.soldig.ayogrosir.datasource.network.services.CategoryService
import com.soldig.ayogrosir.model.ProductCategory
import javax.inject.Inject


class ProductCategoryNetworkDataSourceImpl
@Inject constructor(
    private val productCategoryNetworkMapper: ProductCategoryNetworkMapper,
    private val categoryService: CategoryService
) : CategoryNetworkDataSource{
    override suspend fun getAllCategory(token: String): List<ProductCategory> {
        return categoryService.getAllCategory(token).data.map { productCategoryNetworkMapper.mapToDomain(it) }
    }
}