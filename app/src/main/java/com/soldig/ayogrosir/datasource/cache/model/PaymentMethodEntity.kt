package com.soldig.ayogrosir.datasource.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "payment_method")
data class PaymentMethodEntity(

    @PrimaryKey
    val paymentMethodId: String,

    @ColumnInfo(name="name")
    val name: String,

    @ColumnInfo(name= "category_id")
    val categoryId: String

)