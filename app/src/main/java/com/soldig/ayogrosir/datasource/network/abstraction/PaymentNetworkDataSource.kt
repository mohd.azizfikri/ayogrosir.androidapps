package com.soldig.ayogrosir.datasource.network.abstraction

import com.soldig.ayogrosir.model.PaymentCategory

interface PaymentNetworkDataSource{


    suspend fun getAllPaymentMethod(token: String): List<PaymentCategory>

}