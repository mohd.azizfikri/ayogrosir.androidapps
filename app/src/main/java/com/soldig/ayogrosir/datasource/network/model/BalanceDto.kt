package com.soldig.ayogrosir.datasource.network.model

import com.google.gson.annotations.SerializedName


data class BalanceDto(
    @SerializedName("BeginningBalance")
    val balance: Double
)