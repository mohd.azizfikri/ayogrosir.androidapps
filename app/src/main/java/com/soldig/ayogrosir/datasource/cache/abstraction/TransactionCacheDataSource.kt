package com.soldig.ayogrosir.datasource.cache.abstraction

import com.soldig.ayogrosir.model.*

interface TransactionCacheDataSource {

    suspend fun checkout(phoneNumber:String, transaction: Transaction) : TransactionDetail

    suspend fun getTransactionDetail(transactionId: String) : TransactionDetail

    suspend fun getAllTransactionHistory(phoneNumber: String) : List<TransactionHistory>

    suspend fun getAllTransactionForSync(phoneNumber: String) : List<TransactionForSync>

    suspend fun deleteAllPOSTransaction(phoneNumber: String)

//    suspend fun updateTransactionStatus(token: String, id: String, transactionStatus: TransactionStatus) : String

}