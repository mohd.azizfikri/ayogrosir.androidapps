package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DigiflazzProduct(
    @SerializedName("brand")
    val brand: String,
    @SerializedName("buyer_product_status")
    val buyerProductStatus: Boolean,
    @SerializedName("buyer_sku_code")
    val buyerSkuCode: String,
    @SerializedName("category")
    val category: String,
    @SerializedName("desc")
    val desc: String,
    @SerializedName("end_cut_off")
    val endCutOff: String,
    @SerializedName("multi")
    val multi: Boolean,
    @SerializedName("price")
    val price: Double = 0.0,
    @SerializedName("product_name")
    val productName: String,
    @SerializedName("seller_name")
    val sellerName: String,
    @SerializedName("seller_product_status")
    val sellerProductStatus: Boolean,
    @SerializedName("start_cut_off")
    val startCutOff: String,
    @SerializedName("stock")
    val stock: Int,
    @SerializedName("type")
    val type: String,
    @SerializedName("unlimited_stock")
    val unlimitedStock: Boolean,
    @SerializedName("FeeShop")
    val warungFee: Double = 0.0,
    @SerializedName("FeeApp ")
    val adminFee: Double = 0.0,

)