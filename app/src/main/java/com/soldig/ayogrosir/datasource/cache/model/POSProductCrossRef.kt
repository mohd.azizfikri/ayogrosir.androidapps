package com.soldig.ayogrosir.datasource.cache.model

import androidx.room.*
import com.soldig.ayogrosir.model.DiscountType


@Entity(tableName = "pos_product",
    primaryKeys = ["pos_id", "productId"],
    foreignKeys = [
        ForeignKey(
            entity = POSEntity::class,
            parentColumns = ["phone_number"],
            childColumns = ["pos_id"],
            onUpdate = ForeignKey.NO_ACTION,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = ProductEntity::class,
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.NO_ACTION,
            parentColumns = ["product_id"],
            childColumns = ["productId"]
        )
    ],
    indices = [
        Index(value = ["pos_product_id"] , unique = true),
        Index(value= ["pos_id"] , unique = false),
        Index(value= ["productId"], unique = false)
    ]
)
data class POSProductCrossRef(

    @ColumnInfo(name = "pos_product_id")
    val posProductID: String,

    val productId: String,

    @ColumnInfo(name = "pos_id")
    val posId: String,

    @ColumnInfo(name = "qty")
    val qty: Int,

    @ColumnInfo(name = "purchase_price")
    val purchasePrice: Double,

    @ColumnInfo(name = "sell_price")
    val sellPrice: Double,

    @ColumnInfo(name = "sell_after_discount_price")
    val sellAfterDiscountPrice: Double,

    @ColumnInfo(name = "discount_type")
    val discountType: DiscountType,

    @ColumnInfo(name = "discount_value")
    val discountValue: Double,

    @ColumnInfo(name = "isactive")
    val isActive: Boolean,

    @ColumnInfo(name = "ispilihan")
    val isPilihan: Boolean,

    @ColumnInfo(name = "isowned")
    val isOwned: Boolean,

    @ColumnInfo(name = "ordering")
    val ordering: Int?
)