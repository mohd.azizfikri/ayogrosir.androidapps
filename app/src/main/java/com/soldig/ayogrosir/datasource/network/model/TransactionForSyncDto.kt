package com.soldig.ayogrosir.datasource.network.model


import com.google.gson.annotations.SerializedName

data class TransactionForSyncDto(
    @SerializedName("Items")
    val items: List<ProductTransactionForSyncDto>,
    @SerializedName("Noref")
    val noref: String,
    @SerializedName("Paid")
    val paid: Double,
    @SerializedName("PaymentId")
    val paymentId: String,
    @SerializedName("TransDate")
    val transDate: String
)