package com.soldig.ayogrosir.datasource.network.services

import com.soldig.ayogrosir.datasource.network.impl.ApiResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface ScheduleService {

    @GET("pos/shop/schedule")
    suspend fun getSchedule(
        @Header("Authorization") token: String,
    ) : ApiResponse<HashMap<String,String>>

    @POST("pos/shop/set_schedule")
    suspend fun postSchedule(
        @Header("Authorization") token: String,
        @Body scheduleMap : HashMap<String,String>
    ) : ApiResponse<HashMap<String,String>>
}