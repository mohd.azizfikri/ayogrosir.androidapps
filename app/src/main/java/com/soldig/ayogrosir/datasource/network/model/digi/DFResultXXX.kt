package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DFResultXXX(
    @SerializedName("data")
    val `data`: DataX
)