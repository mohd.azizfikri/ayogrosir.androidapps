package com.soldig.ayogrosir.datasource.network.mapper

import com.soldig.ayogrosir.datasource.OneWayMapper
import com.soldig.ayogrosir.datasource.network.model.ProductTransactionForSyncDto
import com.soldig.ayogrosir.datasource.network.model.TransactionForSyncDto
import com.soldig.ayogrosir.model.DiscountType
import com.soldig.ayogrosir.model.TransactionForSync
import javax.inject.Inject

class TransactionForSyncNetworkMapper
    @Inject constructor(
    ): OneWayMapper<TransactionForSync, TransactionForSyncDto> {
    override fun mapToDomain(t: TransactionForSync): TransactionForSyncDto {
        return TransactionForSyncDto(
            noref = t.transactionId,
            paid = t.paid,
            paymentId = t.paymentId,
            transDate = t.transactionDate,
            items = t.listProduct.map {
                ProductTransactionForSyncDto(
                    currentPrice = it.currentPrice,
                    discountValue = it.discountValue,
                    discountType = discountTypeToId(it.discountType),
                    quantity = it.quantity,
                    sellPrice = it.sellPrice,
                    shopProductId = it.shopProductId
                )
            }
        )
    }
    fun discountTypeToId(discountType: DiscountType) : Int? {
        return when(discountType){
            DiscountType.Nothing -> null
            DiscountType.Percentage -> 1
            DiscountType.Nominal -> 2
        }
    }
}