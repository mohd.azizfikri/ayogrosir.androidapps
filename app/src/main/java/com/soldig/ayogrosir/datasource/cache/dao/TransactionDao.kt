package com.soldig.ayogrosir.datasource.cache.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.soldig.ayogrosir.datasource.cache.model.TransactionEntity
import com.soldig.ayogrosir.datasource.cache.model.TransactionForSyncRelation
import com.soldig.ayogrosir.datasource.cache.model.TransactionWithProduct

@Dao
abstract class TransactionDao : BaseDao<TransactionEntity>(){


//    @Transaction
//    @Query(
//        """
//    SELECT * FROM transaction_entity
//    INNER JOIN transaction_product ON transaction_product.transactionId = transactionId
//    INNER JOIN transaction_entity ON transaction_entity.transactionId = transactionId
//    WHERE transactionId=:transactionId
//    """
//    )
//    abstract suspend fun getTransactionDetail(transactionId: String) : List<TransactionWithProduct>
//

    @Transaction
    @Query(
        """
        SELECT * FROM transaction_entity
        INNER JOIN transaction_product ON transaction_product.transactionId = transaction_entity.transaction_id
        INNER JOIN product ON product.product_id = transaction_product.productId
        WHERE transaction_entity.transaction_id=:transactionId
    """
    )
    abstract suspend fun getTransactionDetail(transactionId: String) : List<TransactionWithProduct>


//    @Transaction
//    @Query(
//        """
//        SELECT * FROM transaction_entity
//        INNER JOIN transaction_product ON transaction_product.transactionId = transaction_entity.transaction_id
//        INNER JOIN pos ON pos.phone_number = transaction_entity.posId
//        WHERE transaction_entity.posId =:phoneNumber
//    """
//    )
//    abstract suspend fun getTransactionDataForSync(phoneNumber: String) : List<TransactionForSyncRelation>


    @Transaction
    @Query("""SELECT * FROM transaction_entity WHERE transaction_entity.posId =:phoneNumber""")
    abstract suspend fun getTransactionDataForSync(phoneNumber: String) : List<TransactionForSyncRelation>

    @Transaction
    @Query("SELECT * FROM transaction_entity WHERE posId=:posId ")
    abstract suspend fun getTransactionByPOS(posId: String): List<TransactionEntity>

    @Transaction
    @Query("DELETE FROM transaction_entity WHERE posId=:posId")
    abstract suspend fun deleteAllTransactionByPOS(posId: String)








}