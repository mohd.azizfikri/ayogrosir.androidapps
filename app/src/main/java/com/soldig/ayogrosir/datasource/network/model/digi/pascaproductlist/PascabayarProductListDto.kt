package com.soldig.ayogrosir.datasource.network.model.digi.pascaproductlist


import com.google.gson.annotations.SerializedName

data class PascabayarProductListDto(
    @SerializedName("DFResult")
    val dFResult: List<DFResult>,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)