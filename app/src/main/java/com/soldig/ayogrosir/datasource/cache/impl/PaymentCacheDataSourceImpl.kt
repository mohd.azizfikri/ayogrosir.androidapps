package com.soldig.ayogrosir.datasource.cache.impl

import com.soldig.ayogrosir.datasource.cache.abstraction.PaymentCacheDataSource
import com.soldig.ayogrosir.datasource.cache.dao.PaymentDao
import com.soldig.ayogrosir.datasource.cache.mapper.PaymentCategoryEntityMapper
import com.soldig.ayogrosir.datasource.cache.mapper.PaymentMethodEntityMapper
import com.soldig.ayogrosir.model.PaymentCategory
import javax.inject.Inject

class PaymentCacheDataSourceImpl
@Inject constructor(
    private val paymentCategoryEntityMapper: PaymentCategoryEntityMapper,
    private val paymentMethodEntityMapper: PaymentMethodEntityMapper,
    private val paymentDao: PaymentDao
) : PaymentCacheDataSource{



    override suspend fun insertPaymentCategories(paymentCategories: List<PaymentCategory>) {
        paymentDao.insertPaymentCategories(paymentCategories.map { paymentCategoryEntityMapper.mapFromDomain(it) })
        paymentCategories.forEach { paymentCategory ->
            paymentDao.insertPaymentMethods(paymentCategory.paymentMethods.map { paymentMethodEntityMapper.mapFromDomain(it) })
        }
    }


    override suspend fun getAllPaymentCategory(): List<PaymentCategory> {
        return paymentDao.getAllPaymentCategoryAndMethod().map { it ->
            paymentCategoryEntityMapper.mapToDomain(it.paymentCategory).apply {
                paymentMethods = it.paymentMethods.map { paymentMethodEntityMapper.mapToDomain(it) }
            }
        }
    }
}