package com.soldig.ayogrosir.datasource.network.model


import com.google.gson.annotations.SerializedName

data class OrderItemDto(
    @SerializedName("ActualPrice")
    val actualPrice: Double,
    @SerializedName("Created")
    val created: Any,
    @SerializedName("CreatedBy")
    val createdBy: Any,
    @SerializedName("Discount")
    val discount: Double,
    @SerializedName("DiscountPercent")
    val discountPercent: Any,
    @SerializedName("DiscountType")
    val discountType: Int?,
    @SerializedName("Id")
    val id: String,
    @SerializedName("M_Shopproduct_Id")
    val mShopproductId: String,
    @SerializedName("Modified")
    val modified: Any,
    @SerializedName("ModifiedBy")
    val modifiedBy: Any,
    @SerializedName("Name")
    val name: String,
    @SerializedName("ProductId")
    val productId: String = "",
    @SerializedName("Price")
    val price: Double,
    @SerializedName("Quantity")
    val quantity: Int,
    @SerializedName("StrActualPrice")
    val strActualPrice: String,
    @SerializedName("StrDiscount")
    val strDiscount: String,
    @SerializedName("StrPrice")
    val strPrice: String,
    @SerializedName("T_Transaction_Id")
    val tTransactionId: Int
)