package com.soldig.ayogrosir.datasource.network.model


import com.google.gson.annotations.SerializedName

data class PaymentCategoryDto(
    @SerializedName("Created")
    val created: String = "",
    @SerializedName("CreatedBy")
    val createdBy: String = "",
    @SerializedName("Description")
    val description: String = "",
    @SerializedName("Id")
    val id: String,
    @SerializedName("Items")
    val items: List<PaymentMethodDto>,
    @SerializedName("Name")
    val name: String,
    @SerializedName("Type")
    val type: String
)