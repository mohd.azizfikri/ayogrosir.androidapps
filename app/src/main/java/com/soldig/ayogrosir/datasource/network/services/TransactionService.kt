package com.soldig.ayogrosir.datasource.network.services

import com.soldig.ayogrosir.datasource.network.impl.ApiResponse
import com.soldig.ayogrosir.datasource.network.model.TransactionDto
import com.soldig.ayogrosir.datasource.network.model.TransactionListForSyncDto
import retrofit2.http.*

interface TransactionService {

    @POST("pos/transaction/checkout")
    suspend fun checkout(
        @Header("Authorization") token: String,
        @Body body : HashMap<String, Any?>
    ) : ApiResponse<TransactionDto>

    @POST("pos/transaction/checkout_sync")
    suspend fun checkoutSync(
        @Header("Authorization") token: String,
        @Body transaction : TransactionListForSyncDto
    ) : ApiResponse<Nothing>

    @GET("pos/transaction/history")
    suspend fun transactionHistoryList(
        @Header("Authorization") token: String,
        @Query("StartDate") startDate : String? = null,
        @Query("EndDate") endDate: String? = null,
        @Query("Status") transactionStatus : Int? = null,
        @Query("TransactionFrom") transactionFrom : Int? = null
    ) : ApiResponse<List<TransactionDto>>

    // minato
    @GET("pos/transaction/history")
    suspend fun transactionFilterByDate(
        @Header("Authorization") token: String,
        @Query("StartDate") startDate: String? = null
    )

    @GET("pos/transaction/detail/{id}")
    suspend fun getTransactionDetail(
        @Header("Authorization") token: String,
        @Path("id") id: String,
    ) : ApiResponse<TransactionDto>

    @Headers("Content-Type: application/json")
    @POST("pos/transaction/update_status/{id}")
    suspend fun updateTransactionId(
        @Header("Authorization") token: String,
        @Path("id") id: String,
        @Body status : HashMap<String, Any>
    ) : ApiResponse<Any?>
}
