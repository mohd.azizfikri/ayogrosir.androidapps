package com.soldig.ayogrosir.datasource.network.mapper

import com.soldig.ayogrosir.datasource.OneWayDomainMapper
import com.soldig.ayogrosir.datasource.network.model.ProductCategoryDto
import com.soldig.ayogrosir.model.ProductCategory
import javax.inject.Inject


class ProductCategoryNetworkMapper
@Inject constructor(

) :OneWayDomainMapper<ProductCategoryDto, ProductCategory>{
    override fun mapToDomain(t: ProductCategoryDto): ProductCategory {
        return ProductCategory(
            name = t.name,
            id = t.id
        )
    }
}