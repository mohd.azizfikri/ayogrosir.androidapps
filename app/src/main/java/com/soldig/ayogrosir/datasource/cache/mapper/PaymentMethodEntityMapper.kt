package com.soldig.ayogrosir.datasource.cache.mapper

import com.soldig.ayogrosir.datasource.DomainMapper
import com.soldig.ayogrosir.datasource.cache.model.PaymentMethodEntity
import com.soldig.ayogrosir.model.PaymentMethod
import javax.inject.Inject

class PaymentMethodEntityMapper
@Inject constructor(

) : DomainMapper<PaymentMethodEntity,PaymentMethod>{
    override fun mapFromDomain(domain: PaymentMethod): PaymentMethodEntity {
        return PaymentMethodEntity(
            name = domain.name,
            paymentMethodId = domain.id,
            categoryId = domain.categoryId
        )
    }

    override fun mapToDomain(t: PaymentMethodEntity): PaymentMethod {
        return PaymentMethod(
            name = t.name,
            categoryId = t.categoryId,
            id = t.paymentMethodId
        )
    }
}