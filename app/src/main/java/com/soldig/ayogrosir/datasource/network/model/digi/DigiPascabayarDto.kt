package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DigiPascabayarDto(
    @SerializedName("DFResult")
    val dFResult: DFResultXXX,
    @SerializedName("FeeData")
    val feeData: FeeData,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)