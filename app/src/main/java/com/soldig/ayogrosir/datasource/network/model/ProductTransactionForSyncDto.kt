package com.soldig.ayogrosir.datasource.network.model


import com.google.gson.annotations.SerializedName

data class ProductTransactionForSyncDto(
    @SerializedName("CurrentPrice")
    val currentPrice: Double,
    @SerializedName("DiscountType")
    val discountType: Int?,
    @SerializedName("DiscountValue")
    val discountValue: Double,
    @SerializedName("Quantity")
    val quantity: Int,
    @SerializedName("SellPrice")
    val sellPrice: Double,
    @SerializedName("ShopProductId")
    val shopProductId: String
)