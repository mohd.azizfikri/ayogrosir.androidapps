package com.soldig.ayogrosir.datasource.network.services

import com.soldig.ayogrosir.datasource.network.model.digi.*
import com.soldig.ayogrosir.datasource.network.model.digi.cektagihan.DigiCheckTagihanDto
import com.soldig.ayogrosir.datasource.network.model.digi.pascaproductlist.PascabayarProductListDto
import com.soldig.ayogrosir.datasource.network.model.digi.prabayarproductlist.PrabayarProductListDto
import retrofit2.http.*

interface DigiFlazzService {

    @GET("df/price_list?mode=0")
    suspend fun getAllPrabayarList(
        @Header("Authorization") token: String,
    ) : PrabayarProductListDto

    @GET("df/price_list?mode=1")
    suspend fun getAllPascabayarList(
        @Header("Authorization") token: String,
    ) : PascabayarProductListDto

    @GET("df/cek_tagihan")
    suspend fun cekTagihan(
        @Header("Authorization") token: String,
        @Query("KodeTransaksi") transactionCode : String,
        @Query("NoPelanggan") userNumber: String
    ) : DigiCheckTagihanDto

    @FormUrlEncoded
    @POST("pay/topup_digiflazz")
    suspend fun topup(
        @Header("Authorization") token: String,
        @Field("KodeTransaksi") transactionCode : String,
        @Field("NoPelanggan") userNumber: String,
        @Field("NamaProduk") productName : String,
        @Field("HargaProduk") price: Double,
        @Field("KategoriProduk") productCategory: String,
    ) : DigiTopupResultDto

    @POST("df/history_search")
    suspend fun getTransactionHistory(
        @Header("Authorization") token: String,
    ) : DigiTransactionHistoryDto

    @FormUrlEncoded
    @POST("pay/bayar_tagihan_digiflazz")
    suspend fun bayarTagihan(
        @Header("Authorization") token: String,
        @Field("KodeTransaksi") transactionCode : String,
        @Field("NoPelanggan") userNumber: String,
        @Field("NamaProduk") productName : String,
        @Field("HargaProduk") price: Double,
        @Field("KategoriProduk") productCategory: String,
    ) : DigiPascabayarDto

}