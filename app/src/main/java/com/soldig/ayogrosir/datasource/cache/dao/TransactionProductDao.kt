package com.soldig.ayogrosir.datasource.cache.dao

import androidx.room.Dao
import com.soldig.ayogrosir.datasource.cache.model.TransactionProductCrossRef

@Dao
abstract class TransactionProductDao : BaseDao<TransactionProductCrossRef>(){
}