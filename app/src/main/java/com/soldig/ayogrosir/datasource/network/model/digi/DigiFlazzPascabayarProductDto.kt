package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DigiFlazzPascabayarProductDto(
    @SerializedName("admin")
    val admin: Int,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("buyer_product_status")
    val buyerProductStatus: Boolean,
    @SerializedName("buyer_sku_code")
    val buyerSkuCode: String,
    @SerializedName("category")
    val category: String,
    @SerializedName("commission")
    val commission: Int,
    @SerializedName("desc")
    val desc: String,
    @SerializedName("product_name")
    val productName: String,
    @SerializedName("seller_name")
    val sellerName: String,
    @SerializedName("seller_product_status")
    val sellerProductStatus: Boolean
)