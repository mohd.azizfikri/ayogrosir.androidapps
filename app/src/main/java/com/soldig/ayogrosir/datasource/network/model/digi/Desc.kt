package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class Desc(
    @SerializedName("detail")
    val detail: List<Detail> = listOf(),
    @SerializedName("lembar_tagihan")
    val lembarTagihan: Int
)