package com.soldig.ayogrosir.datasource.network.abstraction

import com.soldig.ayogrosir.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap

interface UserNetworkDataSource {

    suspend fun login(user: User) : POS

    suspend fun setStartingBalance(startTingBalance : Double, token: String) : Boolean

    suspend fun setDeliveryStatus(token: String, isDeliveryOpen: Boolean) : String

    suspend fun closePOS(token: String) : String

    suspend fun updateFCM(token: String) : String
    suspend fun topupVirtual(
        token: String,
        param: HashMap<String, @JvmSuppressWildcards RequestBody>,
        partFile: MultipartBody.Part?
    ): ResponseTopup

    suspend fun getRiwayatSaldo(token: String,tgl1: String,tgl2: String,status: String) : ResponseRiwayatTopup

    suspend fun getSaldo(token: String) : ResponseGetSaldo
}