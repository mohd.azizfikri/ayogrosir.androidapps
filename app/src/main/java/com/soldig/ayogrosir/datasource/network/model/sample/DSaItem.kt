package com.soldig.ayogrosir.datasource.network.model.sample

import com.google.gson.annotations.SerializedName

data class DSaItem(
    @SerializedName("Created")
    val Created: String,
    @SerializedName("CreatedBy")
    val CreatedBy: String,
    @SerializedName("CurrentPrice")
    val currentPrice: Int = 0,
    @SerializedName("DiscountType")
    val discountType: Int? = null,
    @SerializedName("DiscountValue")
    val discountValue: Int? = null,
    @SerializedName("HasDiscount")
    val HasDiscount: Boolean,
    @SerializedName("Id")
    val id: String? = null,
    @SerializedName("IsActive")
    val isActive: Boolean = false,
    @SerializedName("IsAlreadyInCart")
    val isAlreadyInCart: Boolean = false,
    @SerializedName("IsFeatured")
    val isFeatured: Boolean = false,
    @SerializedName("IsOwned")
    val isOwned: Boolean = false,
    @SerializedName("M_Product_Id")
    val mProductId: String,
    @SerializedName("M_Productcategory_Id")
    val mProductcategoryId: String,
    @SerializedName("M_Shop_Id")
    val mShopId: String = "",
    @SerializedName("Modified")
    val Modified: String,
    @SerializedName("ModifiedBy")
    val ModifiedBy: String,
    @SerializedName("Name")
    val name: String = "",
    @SerializedName("Ordering")
    val order: Int?,
    @SerializedName("PackSize")
    val pakcSize: String = "",
    @SerializedName("Picture")
    val picture: String = "",
    @SerializedName("PurchasePrice")
    val purchasePrice: Int = 0,
    @SerializedName("QuantityInCart")
    val quantityInCart: Int = 0,
    @SerializedName("SellPrice")
    val sellPrice: Int = 0,
    @SerializedName("Stock")
    val stock: Int = 0,
    @SerializedName("StrCurrentPrice")
    val StrCurrentPrice: String,
    @SerializedName("StrSellPrice")
    val StrSellPrice: String
)