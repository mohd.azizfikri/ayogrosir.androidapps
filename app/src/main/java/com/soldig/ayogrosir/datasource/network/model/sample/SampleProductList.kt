package com.soldig.ayogrosir.datasource.network.model.sample

import com.soldig.ayogrosir.datasource.network.model.ProductDto

data class SampleProductList(
    val Data: ProductDto,
    val Message: String,
    val Response: Response
)