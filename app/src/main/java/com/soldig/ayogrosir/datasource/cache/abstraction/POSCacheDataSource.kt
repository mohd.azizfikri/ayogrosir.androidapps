package com.soldig.ayogrosir.datasource.cache.abstraction

import com.soldig.ayogrosir.model.POS

interface POSCacheDataSource {

    suspend fun insertPOS(pos: POS)

    suspend fun getAllPos() : List<POS>


}