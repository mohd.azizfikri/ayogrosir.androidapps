package com.soldig.ayogrosir.datasource.cache.mapper

import com.soldig.ayogrosir.datasource.DomainMapper
import com.soldig.ayogrosir.datasource.cache.model.PaymentCategoryEntity
import com.soldig.ayogrosir.model.PaymentCategory
import javax.inject.Inject

class PaymentCategoryEntityMapper
@Inject constructor() : DomainMapper<PaymentCategoryEntity, PaymentCategory>{
    override fun mapFromDomain(domain: PaymentCategory): PaymentCategoryEntity {
        return PaymentCategoryEntity(
            paymentCategoryId = domain.id,
            name = domain.name,
            paymentType = domain.paymentType
        )
    }

    override fun mapToDomain(t: PaymentCategoryEntity): PaymentCategory {
        return PaymentCategory(
            id= t.paymentCategoryId,
            name = t.name,
            paymentType = t.paymentType
        )
    }
}