package com.soldig.ayogrosir.datasource.network.services

import com.soldig.ayogrosir.datasource.network.impl.ApiResponse
import com.soldig.ayogrosir.datasource.network.model.PaymentCategoryDto
import retrofit2.http.GET
import retrofit2.http.Header

interface PaymentService {

    @GET("paymentcategory/list")
    suspend fun getAllPaymentMethode(
        @Header("Authorization") token: String
    ) : ApiResponse<List<PaymentCategoryDto>>
}