package com.soldig.ayogrosir.datasource.network.model.digi.cektagihan


import com.google.gson.annotations.SerializedName

data class DigiCheckTagihanDto(
    @SerializedName("DFResult")
    val dFResult: DFResult,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)