package com.soldig.ayogrosir.datasource.cache.dao

import androidx.room.Dao
import androidx.room.Query
import com.soldig.ayogrosir.datasource.cache.model.ProductEntity

@Dao
abstract class ProductDao : BaseDao<ProductEntity>() {



    @Query("SELECT * FROM Product")
    abstract suspend fun getAll() : List<ProductEntity>


    @Query(" SELECT * FROM product WHERE title LIKE :key")
   abstract suspend fun getProductByName(key: String) : List<ProductEntity>


    @Query("SELECT * FROM product WHERE product_id=:id")
    abstract suspend fun getProductById(id: String) : ProductEntity




}