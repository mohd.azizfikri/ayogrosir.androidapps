package com.soldig.ayogrosir.datasource.network.services

import com.soldig.ayogrosir.datasource.network.impl.ApiResponse
import com.soldig.ayogrosir.datasource.network.model.ProductCategoryDto
import retrofit2.http.GET
import retrofit2.http.Header


interface CategoryService {

    @GET("product_category/list")
    suspend fun getAllCategory(
        @Header("Authorization") token: String
    ) : ApiResponse<List<ProductCategoryDto>>

}