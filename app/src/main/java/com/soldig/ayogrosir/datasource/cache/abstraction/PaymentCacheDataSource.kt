package com.soldig.ayogrosir.datasource.cache.abstraction

import com.soldig.ayogrosir.model.PaymentCategory

interface PaymentCacheDataSource {


    suspend fun insertPaymentCategories(paymentCategories: List<PaymentCategory>)

    suspend fun getAllPaymentCategory() : List<PaymentCategory>

}