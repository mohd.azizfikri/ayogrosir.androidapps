package com.soldig.ayogrosir.datasource.cache.dao

import androidx.room.*
import com.soldig.ayogrosir.datasource.cache.model.PaymentCategoryEntity
import com.soldig.ayogrosir.datasource.cache.model.PaymentCategoryWithMethodsRelation
import com.soldig.ayogrosir.datasource.cache.model.PaymentMethodEntity

@Dao
interface PaymentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPaymentCategory(paymentCategory: PaymentCategoryEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPaymentCategories(paymentCategoryList : List<PaymentCategoryEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPaymentMethods(paymentMethodList : List<PaymentMethodEntity>)

    @Transaction
    @Query("SELECT * FROM payment_category")
    suspend fun getAllPaymentCategoryAndMethod() : List<PaymentCategoryWithMethodsRelation>

}