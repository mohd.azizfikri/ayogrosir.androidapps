package com.soldig.ayogrosir.datasource.network.model


import com.google.gson.annotations.SerializedName

data class ItemsSold(
    @SerializedName("M_Shopproduct_Id")
    val mShopproductId: String,
    @SerializedName("Name")
    val name: String,
    @SerializedName("Quantity")
    val quantity: Int
)