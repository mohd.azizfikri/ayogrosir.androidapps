package com.soldig.ayogrosir.datasource.network.mapper

import com.soldig.ayogrosir.datasource.DomainMapper
import com.soldig.ayogrosir.datasource.network.model.PaymentMethodDto
import com.soldig.ayogrosir.model.PaymentMethod
import javax.inject.Inject

class PaymentMethodNetworkMapper
@Inject constructor() : DomainMapper<PaymentMethodDto, PaymentMethod> {
    override fun mapFromDomain(domain: PaymentMethod): PaymentMethodDto {
        return PaymentMethodDto(
            id = domain.id,
            name = domain.name,
            mPaymentcategoryId = domain.categoryId
        )
    }

    override fun mapToDomain(t: PaymentMethodDto): PaymentMethod {
        return PaymentMethod(
            id = t.id,
            name = t.name,
            categoryId = t.mPaymentcategoryId
        )
    }
}