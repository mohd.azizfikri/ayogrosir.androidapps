package com.soldig.ayogrosir.datasource.network.mapper

import com.soldig.ayogrosir.datasource.DomainMapper
import com.soldig.ayogrosir.datasource.network.model.UserDto
import com.soldig.ayogrosir.model.User
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class UserNetworkMapper
@Inject constructor(): DomainMapper<UserDto, User>{
    override fun mapFromDomain(domain: User): UserDto {
        return UserDto(
            phoneNumber = domain.phoneNumber,
            pin = domain.pin
        )
    }

    override fun mapToDomain(t: UserDto): User {
        return User(
            phoneNumber = t.phoneNumber,
            pin = t.pin
        )
    }
}