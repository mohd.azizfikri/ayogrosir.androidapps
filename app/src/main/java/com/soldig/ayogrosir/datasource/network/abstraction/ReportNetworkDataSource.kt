package com.soldig.ayogrosir.datasource.network.abstraction

import com.soldig.ayogrosir.model.Report
import java.util.*

interface ReportNetworkDataSource {

    suspend fun getReport(token: String, startDate: Date?, endDate: Date? = null) : Report

    suspend fun getCloseReport(token: String) : Report

}