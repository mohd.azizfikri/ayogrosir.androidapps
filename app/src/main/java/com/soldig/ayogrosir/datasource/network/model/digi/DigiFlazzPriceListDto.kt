package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DigiFlazzBasicResponse<Data>(
    @SerializedName("DFResult")
    val dFResult: DFResult<Data>,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)