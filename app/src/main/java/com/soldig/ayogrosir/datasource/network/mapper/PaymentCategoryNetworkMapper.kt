package com.soldig.ayogrosir.datasource.network.mapper

import com.soldig.ayogrosir.datasource.DomainMapper
import com.soldig.ayogrosir.datasource.network.model.PaymentCategoryDto
import com.soldig.ayogrosir.model.PaymentCategory
import com.soldig.ayogrosir.model.PaymentCategoryType
import com.soldig.ayogrosir.model.PaymentMethod
import javax.inject.Inject


class PaymentCategoryNetworkMapper
@Inject constructor(
    private val paymentMethodNetworkMapper: PaymentMethodNetworkMapper
) : DomainMapper<PaymentCategoryDto, PaymentCategory> {
    override fun mapFromDomain(domain: PaymentCategory): PaymentCategoryDto {
        return PaymentCategoryDto(
            id = domain.id,
            name = domain.name,
            type = paymentCategoryTypeToID(domain.paymentType),
            items = domain.paymentMethods.map { paymentMethod: PaymentMethod -> paymentMethodNetworkMapper.mapFromDomain(paymentMethod) }
        )
    }

    override fun mapToDomain(t: PaymentCategoryDto): PaymentCategory {
        return PaymentCategory(
            id = t.id,
            name = t.name,
            paymentType = paymentCategoryIdToType(t.type),
            paymentMethods = t.items.map { paymentMethodNetworkMapper.mapToDomain(it) }
        )
    }

    private fun paymentCategoryTypeToID(type: PaymentCategoryType) : String{
        return when(type){
            PaymentCategoryType.CASH -> "1"
            else -> "2"
        }
    }

    private fun paymentCategoryIdToType(id : String) : PaymentCategoryType {
        return when(id){
            "1" -> PaymentCategoryType.CASH
            else -> PaymentCategoryType.NOT_CASH
        }
    }
}