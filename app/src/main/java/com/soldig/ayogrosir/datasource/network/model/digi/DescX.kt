package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DescX(
    @SerializedName("detail")
    val detail: List<DetailX>,
    @SerializedName("lembar_tagihan")
    val lembarTagihan: Int
)