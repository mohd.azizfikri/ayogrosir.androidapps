package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DigiPascaBayarTransactionResult(
    @SerializedName("buyer_sku_code")
    val buyerSkuCode: String,
    @SerializedName("customer_no")
    val customerNo: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("rc")
    val rc: String,
    @SerializedName("ref_id")
    val refId: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("price")
    val price : Double? = null,
)