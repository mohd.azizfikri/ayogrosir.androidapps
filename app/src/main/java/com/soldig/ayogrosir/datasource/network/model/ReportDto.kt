package com.soldig.ayogrosir.datasource.network.model


import com.google.gson.annotations.SerializedName

data class ReportDto(
    @SerializedName("CanceledAmount")
    val canceledAmount: Double,
    @SerializedName("CanceledBill")
    val canceledBill: Int,
    @SerializedName("CanceledDeliveryAmount")
    val canceledDeliveryAmount: Double,
    @SerializedName("CanceledProduct")
    val canceledProduct: Int,
    @SerializedName("DeliveryAmount")
    val deliveryAmount: Double,
    @SerializedName("DiscountAmount")
    val discountAmount: Double,
    @SerializedName("ItemsCanceled")
    val itemsCanceled: List<ItemsCanceled>,
    @SerializedName("ItemsSold")
    val itemsSold: List<ItemsSold>,
    @SerializedName("SoldAmount")
    val soldAmount: Double,
    @SerializedName("SoldProduct")
    val soldProduct: Int,
    @SerializedName("TotalAmount")
    val totalAmount: Double,
    @SerializedName("TotalBill")
    val totalBill: Int,
    @SerializedName("TotalCashTransaction")
    val totalCashTransaction: Double,
    @SerializedName("TotalNonCashPayment")
    val totalNonCashPayment: Double,
    @SerializedName("TotalNonCashTransaction")
    val totalNonCashTransaction: Double,
    @SerializedName("TotalShopBeginningBalance")
    val totalShopBeginningBalance: Double,
    @SerializedName("TotalWholeCash")
    val totalWholeCash: Double,
    @SerializedName("TotalWallet")
    val reportWallet : List<ReportWalletDto>
)