package com.soldig.ayogrosir.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DescXX(
    @SerializedName("detail")
    val detail: List<DetailXX>,
    @SerializedName("lembar_tagihan")
    val lembarTagihan: Int
)