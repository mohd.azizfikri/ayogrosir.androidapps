package com.soldig.ayogrosir.datasource.network.model.digi.cektagihan


import com.google.gson.annotations.SerializedName

data class Desc(
    @SerializedName("lembar_tagihan")
    val lembarTagihan: Int
)