package com.soldig.ayogrosir.datasource.network.abstraction

import com.soldig.ayogrosir.model.Schedule
import com.soldig.ayogrosir.model.SettingModel


interface ScheduleNetworkDataSource {

    suspend fun getScheduleAndDeliveryStatus(token:String) : SettingModel

    suspend fun postSchedule(token:String, schedule: Schedule) : String

}