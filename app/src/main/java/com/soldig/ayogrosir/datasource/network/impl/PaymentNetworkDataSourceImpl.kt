package com.soldig.ayogrosir.datasource.network.impl

import com.soldig.ayogrosir.datasource.network.abstraction.PaymentNetworkDataSource
import com.soldig.ayogrosir.datasource.network.mapper.PaymentCategoryNetworkMapper
import com.soldig.ayogrosir.datasource.network.services.PaymentService
import com.soldig.ayogrosir.model.PaymentCategory
import javax.inject.Inject

class PaymentNetworkDataSourceImpl
@Inject constructor(
    private val paymentService: PaymentService,
    private val paymentCategoryNetworkMapper: PaymentCategoryNetworkMapper
): PaymentNetworkDataSource{


    override suspend fun getAllPaymentMethod(token: String): List<PaymentCategory> {
        return paymentService.getAllPaymentMethode(token).data.map {
            paymentCategoryNetworkMapper.mapToDomain(it) }
    }
}