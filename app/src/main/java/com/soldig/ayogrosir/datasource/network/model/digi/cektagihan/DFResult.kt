package com.soldig.ayogrosir.datasource.network.model.digi.cektagihan


import com.google.gson.annotations.SerializedName

data class DFResult(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("FeeData")
    val feeData: FeeData
)