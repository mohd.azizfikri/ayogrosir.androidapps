package com.soldig.ayogrosir.datasource.cache.mapper

import android.annotation.SuppressLint
import com.soldig.ayogrosir.datasource.OneWayDomainMapper
import com.soldig.ayogrosir.datasource.cache.model.TransactionForSyncRelation
import com.soldig.ayogrosir.model.ProductTransactionForSync
import com.soldig.ayogrosir.model.TransactionForSync
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TransactionForSyncEntityMapper
    @Inject constructor(

    ): OneWayDomainMapper<TransactionForSyncRelation , TransactionForSync> {
    override fun mapToDomain(t: TransactionForSyncRelation): TransactionForSync {
        val transaction = t.transaction
        val productList = t.transactionProductCrossRef
        return TransactionForSync(
            transactionId = transaction.id,
            paymentId = transaction.paymentId,
            paid = transaction.totalPaid,
            transactionDate = dateToStringForm(Date(transaction.transactionDate)),
            listProduct = productList.map {
                ProductTransactionForSync(
                    shopProductId = it.posProductId,
                    discountType = it.discountType,
                    sellPrice = it.priceBeforeDiscount,
                    discountValue = it.discount,
                    currentPrice = it.priceAfterDiscount,
                    quantity = it.qty
                )
            }
        )
    }

    @SuppressLint("SimpleDateFormat")
    private fun dateToStringForm(date: Date): String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return simpleDateFormat.format(date)
    }
}
